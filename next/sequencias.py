import frappe

def getSecuencia(tipo_documento,establecimiento,punto_emision):
    existe = frappe.db.exists("sequencias_facturacion",{"tipo_documento":tipo_documento,"establecimiento":establecimiento,"punto_emision":punto_emision}) 
    if existe:
        docseq = frappe.get_doc("sequencias_facturacion",{"tipo_documento":tipo_documento,"establecimiento":establecimiento,"punto_emision":punto_emision} )
        docseq.establecimiento = establecimiento
        docseq.punto_emision = punto_emision
        docseq.secuencia = str(int(docseq.secuencia) + 1).zfill(9)
        docseq.save()
        frappe.db.commit()
        return docseq.secuencia 
        
    else:
        seq  = frappe.new_doc("sequencias_facturacion")
        seq.tipo_documento = tipo_documento
        seq.establecimiento = establecimiento
        seq.punto_emision = punto_emision
        seq.secuencia = "1".zfill(9)
        seq.insert()
        frappe.db.commit()
        return seq.secuencia