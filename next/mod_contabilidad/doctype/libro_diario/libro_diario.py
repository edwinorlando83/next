# Copyright (c) 2023, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class libro_diario(Document):
	def validate(self):
		suma_debe=0
		suma_haber=0
		periodo = frappe.get_doc("Periodo", self.periodo)
		if periodo.per_estacerrado == 1:
			frappe.throw( "El periodo esta cerrado, no se puede agregar movimientos ")
		array=[]
		for row in self.libro_diario_detalle:
			array.append(row.plandecuentas)
			suma_debe += row.debito
			suma_haber += row.credito
		self.total_debito = suma_debe
		self.total_credito = suma_haber
		if( tiene_elementos_repetidos(array)):
			frappe.throw( "Existen cuentas repetidas")
		if( self.total_debito  !=self.total_credito ):
			frappe.throw( "La suma del debe y haber no cuadra")
		if( self.total_debito == 0 and  self.total_credito == 0 ):
			frappe.throw( "La suma del debe y haber deben ser mayores a cero")



def tiene_elementos_repetidos(arr):
    """
    Verifica si hay elementos repetidos en un array.
    Devuelve True si hay elementos repetidos, False si no.
    """
    # Utilizamos un conjunto (set) para verificar duplicados
    conjunto_elementos = set()
    
    for elemento in arr:
        # Si el elemento ya está en el conjunto, hay un duplicado
        if elemento in conjunto_elementos:
            return True
        # Agregamos el elemento al conjunto
        conjunto_elementos.add(elemento)
    
    # No se encontraron duplicados
    return False
