// Copyright (c) 2023, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('libro_diario', {
	  refresh: function(frm) {
		filtro(frm);
	  }
});

function filtro(frm){
	cur_frm.set_query(  'plandecuentas', 'libro_diario_detalle', function () {
		return {
			filters: [
				['movimiento', '=', 1]

			]
		}
	});

}
