# Copyright (c) 2023, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class Periodo(Document):
	def validate(self):
		if self.per_fech_cie :
			self.per_estacerrado = 1
		else:
			self.per_estacerrado = 0

