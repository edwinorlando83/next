# Copyright (c) 2023, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import re 
class plandecuentas(Document):
	def validate(self):
		self.nombre = self.cuenta + ' ' + self.descripcion 
		# asignar padre
		self.cuenta_real = remover_punto_final(self.cuenta )
		 
		array = self.cuenta_real.split(".")
		self.nivel = len(array)
		
		if self.movimiento == 1:
			self.is_group = 0
		else:
			self.is_group = 1
			
		 
		if self.nivel == 1:
			self.parent_plandecuentas = None
		else:
			posant =  concatenar_primeras_n_posiciones (array,self.nivel - 1)	 
			 
			sql = """select  name  from tabplandecuentas where cuenta_real='{0}'  """.format(posant)
			
			objres = frappe.db.sql(sql)
			if not objres:
				frappe.throw("Defina el padre primero: " + posant )
			else:				 
				self.parent_plandecuentas = objres[0][0]
			 

def remover_punto_final(cadena):
    # Caso base: si la cadena está vacía, retorna la cadena vacía
    if len(cadena) == 0:
        return cadena

    # Si el último carácter es un punto, llamamos recursivamente sin ese punto
    if cadena[-1] == '.':
        return remover_punto_final(cadena[:-1])
    else:
        # Si el último carácter no es un punto, retornamos la cadena tal cual
        return cadena


def concatenar_primeras_n_posiciones(arr, n):
    if n < 0:
        print("El valor de n debe ser no negativo.")
        return None
    elif n >= len(arr):
        return '.'.join(arr)
    else:
        return '.'.join(arr[:n]) 

#bench --site nextweb exectute  next.mod_contabilidad.doctype.plandecuentas.plandecuentas.updateplancuentas
def updateplancuentas():
	sql = "select name, cuenta  from tabplandecuentas order by cuenta"
	objres = frappe.db.sql(sql , as_dict=True)
	for row in objres:
		pl = frappe.get_doc("plandecuentas", row.name)
		pl.cuenta = pl.cuenta + '.'
		pl.save()
		frappe.db.commit()
	
