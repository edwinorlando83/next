frappe.query_reports["Reporte de Kardex"] = {
	"filters": [
		{
			"fieldname":"inproducto",
			"label": __("Producto"),
			"fieldtype": "Link",
            "options": "Productos",
            "reqd":1
		}
	]
}