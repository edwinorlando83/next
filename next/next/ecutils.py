from __future__ import unicode_literals
import xml.etree.cElementTree as ET
import frappe, os
import random
import barcode
import tempfile
from datetime import datetime
#def pin_dv_gen(digs):
#    l = len(digs)
#    d=0
#    for i in range(1,l+1):
#        d = d + int(digs[-i])*(i+1)
#        dv=d*10 % 11
#    if 10==dv: return '0'
#    return str( dv )

def pin_dv_gen(digs):
    baseMultiplicador = 7
    multiplicador = 2
    verificador = 0
    total = 0
    for i in  reversed(digs):
        aux = int(i) * multiplicador
        multiplicador = multiplicador + 1
        if multiplicador > baseMultiplicador:
            multiplicador = 2
        total+=aux

    if total < 0 :
        verificador = 0
    else:
        m = total % 11

        r = 11 - m
        if r == 11:
            verificador = 0
        else:
            verificador = r

    if verificador == 10 :
            verificador = 1

    return str(verificador)


def pin_dv_verify(digs):
    dig = pin_dv_gen(digs[0:-1])
    return dig == digs[-1]

def generate_pin_with_dv():
    pin = "".join(random.sample('0123456789', 8))
    return pin + pin_dv_gen(pin)

def generarClaveAcesso(fecha,tipoComprobante,ruc,ambiente,serie,secuencial):
   
    arrayFecha = fecha.split('-')
    dia = arrayFecha[2]
    mes = arrayFecha[1]
    anio = arrayFecha[0]
    claveAcceso = dia + mes + anio
    pin = "".join(random.sample('0123456789', 8))
    claveAcceso48 = (claveAcceso + tipoComprobante + ruc +
    ambiente + serie + secuencial + pin + '1')
    claveAcceso = claveAcceso48 + pin_dv_gen(claveAcceso48)
    return claveAcceso

def genenarXmlFacturas(clave_acceso):
    
    #ruta = os.path.dirname(__file__)
    #ruta = frappe.local.site
    ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    directorio = ruta + "/xmls"
    rutafile = directorio + "/" + clave_acceso + ".xml"

    if not os.path.exists(directorio):
        os.makedirs(directorio)

    ruc = clave_acceso[10:-26]

    factura = ET.Element('factura')
    factura.set("id", "comprobante")
    factura.set("version", "1.1.0")
    infoTributaria = ET.SubElement(factura, "infoTributaria")
    ambiente = ET.SubElement(infoTributaria, "ambiente")
    ambiente.text = clave_acceso[23:-25]
    tipoEmision = ET.SubElement(infoTributaria, "tipoEmision")
    tipoEmision.text = clave_acceso[47:-1]
    razonSocial = ET.SubElement(infoTributaria, "razonSocial")
    razonSocial.text = getRazonSocial(ruc)
    nombreComercial = ET.SubElement(infoTributaria, "nombreComercial")
    nombreComercial.text = getNombreComercial(ruc)
    _ruc = ET.SubElement(infoTributaria, "ruc")
    _ruc.text = ruc
    claveAcceso = ET.SubElement(infoTributaria, "claveAcceso")
    claveAcceso.text = clave_acceso
    codDoc = ET.SubElement(infoTributaria, "codDoc")
    codDoc.text = clave_acceso[8:-39]
    estab = ET.SubElement(infoTributaria, "estab")
    estab.text = clave_acceso[24:-22]
    ptoEmi = ET.SubElement(infoTributaria, "ptoEmi")
    ptoEmi.text = clave_acceso[27:-19]
    secuencial = ET.SubElement(infoTributaria, "secuencial")
    secuencial.text = clave_acceso[30:-10]
    dirMatriz = ET.SubElement(infoTributaria, "dirMatriz")
    dirMatriz.text = getdirMatriz(ruc)
    rimpe = getregimen(ruc)
    if rimpe:
        vacio = len(rimpe.strip()) 
        if vacio > 0:
            contribuyenteRimpe = ET.SubElement(infoTributaria, "contribuyenteRimpe")
            contribuyenteRimpe.text = getregimen(ruc)

    infoFactura = ET.SubElement(factura, "infoFactura")
    fechaEmision = ET.SubElement(infoFactura, "fechaEmision")
    femi = (clave_acceso[0:-47] + '/' +
    clave_acceso[2:-45] + '/' + clave_acceso[4:-41])
    fechaEmision.text = femi

    dirEstablecimiento = ET.SubElement(infoFactura, "dirEstablecimiento")
     
    dirEstablecimiento.text = getdirEstablecimiento(clave_acceso[24:-22])
    
    objespecial = getEspecial(ruc)
    if not objespecial:
        contribuyenteEspecial = ET.SubElement(infoFactura,
             "contribuyenteEspecial")
        contribuyenteEspecial.text = objespecial[0][0]

    obligadoContabilidad = ET.SubElement(infoFactura, "obligadoContabilidad")
    obligadoContabilidad.text = getContabilidad(ruc)



    #guiaRemision = ET.SubElement(factura, "guiaRemision")
    #guiaRemision.text = ""
    datosComprador = getComprador(clave_acceso)
    tipoIdenComprador = datosComprador[0][0]
    rucComprador = datosComprador[0][1]
    nombreComprador = datosComprador[0][2]
    direcciondelComprador = datosComprador[0][4] 
    tipoI1,tipoI2= tipoIdenComprador.split('-')

    tipoIdentificacionComprador = ET.SubElement(infoFactura,"tipoIdentificacionComprador")
    tipoIdentificacionComprador.text = tipoI2

    razonSocialComprador = ET.SubElement(infoFactura, "razonSocialComprador")
    razonSocialComprador.text = nombreComprador

    identificacionComprador = ET.SubElement(infoFactura, "identificacionComprador")
    identificacionComprador.text = rucComprador


    direccionComprador = ET.SubElement(infoFactura, "direccionComprador")
    direccionComprador.text = direcciondelComprador

    #totaldescuento,totalsinimpuestos,importetotal,formapago,iva_valor
    datosSubyDesc = getSubtotalyDesc(clave_acceso)

    totalSinImpuesto = ET.SubElement(infoFactura, "totalSinImpuestos")
    totalSinImpuesto.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    totalDescuento = ET.SubElement(infoFactura, "totalDescuento")
    totalDescuento.text = '{0:.2f}'.format(datosSubyDesc[0][0])

    totalConImpuestos = ET.SubElement(infoFactura,"totalConImpuestos")
    totalImpuesto = ET.SubElement(totalConImpuestos,"totalImpuesto")

    codsri = getCodigosImpuestoSri(clave_acceso)
    codigo = ET.SubElement(totalImpuesto, "codigo")
    codigo.text ="2"

    codigoPorcentaje = ET.SubElement(totalImpuesto, "codigoPorcentaje")
    codigoPorcentaje.text = codsri[1]

    descuentoAdicional = ET.SubElement(totalImpuesto, "descuentoAdicional")
    descuentoAdicional.text = '{0:.2f}'.format(datosSubyDesc[0][0])

    baseImponible = ET.SubElement(totalImpuesto, "baseImponible")
    baseImponible.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    valor = ET.SubElement(totalImpuesto, "valor")
    valor.text = '{0:.2f}'.format(datosSubyDesc[0][4])

    propina = ET.SubElement(infoFactura, "propina")
    propina.text = "0.00"

    importeTotal = ET.SubElement(infoFactura, "importeTotal")
    importeTotal.text = '{0:.2f}'.format(datosSubyDesc[0][2])

    moneda = ET.SubElement(infoFactura, "moneda")
    moneda.text = "DOLAR"

    formapago = str(datosSubyDesc[0][3])
    codfp,txt = formapago.split('-')
    pagos  = ET.SubElement(infoFactura, "pagos")
    pago = ET.SubElement(pagos, "pago")
    formaPago = ET.SubElement(pago, "formaPago")
    formaPago.text = codfp
    total = ET.SubElement(pago, "total")
    total.text =  '{0:.2f}'.format(datosSubyDesc[0][2])

    detalles = ET.SubElement(factura, "detalles")
   
    productos = getProductos(clave_acceso)
    for p in productos:
        detalle = ET.SubElement(detalles, "detalle")
        codigoPrincipal = ET.SubElement(detalle, "codigoPrincipal")
        codigoPrincipal.text = p[5]

        #codigoAuxiliar = ET.SubElement(detalle, "codigoAuxiliar")
        #codigoAuxiliar.text = p[5]

        descripcion = ET.SubElement(detalle, "descripcion")
        descripcion.text = p[1]

        cantidad = ET.SubElement(detalle, "cantidad")
        cantidad.text = '{0:.2f}'.format(p[3])

        precioUnitario = ET.SubElement(detalle, "precioUnitario")
        precioUnitario.text = '{0:.2f}'.format(p[2])

        descuento = ET.SubElement(detalle, "descuento")
        descuento.text = '{0:.2f}'.format(p[4])

        precioTotalSinImpuesto = ET.SubElement(detalle, "precioTotalSinImpuesto")

        precioTotalSinImpuesto.text =   '{0:.2f}'.format(float(p[6]))

        impuestos_prod =  ET.SubElement(detalle, "impuestos")
        impuesto_prod =  ET.SubElement(impuestos_prod, "impuesto")
        codigo_prod =  ET.SubElement(impuesto_prod, "codigo")
        codigo_prod.text = "2"

        codigoPorcentaje_prod =  ET.SubElement(impuesto_prod, "codigoPorcentaje")
        codigoPorcentaje_prod.text = p[8]

        tarifa_prod =  ET.SubElement(impuesto_prod, "tarifa")
        tarifa_prod.text = str(p[9])
        

        baseImponible_prod =  ET.SubElement(impuesto_prod, "baseImponible")
        baseImponible_prod.text =  '{0:.2f}'.format(p[6])

        valor_prod =  ET.SubElement(impuesto_prod, "valor")
        valor_prod.text =  '{0:.2f}'.format(p[7])

    infosAdd = getinfosAdd(clave_acceso)
    infoAdicional =  ET.SubElement(factura, "infoAdicional")
    for info in infosAdd:
        campoAdicional =  ET.SubElement(infoAdicional, "campoAdicional")
        campoAdicional.text = info[1]
        campoAdicional.set('nombre', info[0])

    tree = ET.ElementTree(factura)
    tree.write(rutafile, xml_declaration=True, encoding='utf-8', method="xml")
    #frappe.throw("fin   "+rutafile)     
    os.system("""/home/frappe/frappe-bench/callfac.sh {0} {1} """.format(frappe.get_site_config().db_name , ruc) )
    return rutafile

def genenarXmlNC(clave_acceso,docnc):
    #ruta = os.path.dirname(__file__)
    #ruta = frappe.local.site
    ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    directorio = ruta + "/xmls"
    rutafile = directorio + "/" + clave_acceso + ".xml"

    if not os.path.exists(directorio):
        os.makedirs(directorio)

    ruc = clave_acceso[10:-26]

    factura = ET.Element('notaCredito')
    factura.set("id", "comprobante")
    factura.set("version", "1.1.0")
    infoTributaria = ET.SubElement(factura, "infoTributaria")
    ambiente = ET.SubElement(infoTributaria, "ambiente")
    ambiente.text = clave_acceso[23:-25]
    tipoEmision = ET.SubElement(infoTributaria, "tipoEmision")
    tipoEmision.text = clave_acceso[47:-1]
    razonSocial = ET.SubElement(infoTributaria, "razonSocial")
    razonSocial.text = getRazonSocial(ruc)
    nombreComercial = ET.SubElement(infoTributaria, "nombreComercial")
    nombreComercial.text = getNombreComercial(ruc)
    _ruc = ET.SubElement(infoTributaria, "ruc")
    _ruc.text = ruc
    claveAcceso = ET.SubElement(infoTributaria, "claveAcceso")
    claveAcceso.text = clave_acceso
    codDoc = ET.SubElement(infoTributaria, "codDoc")
    codDoc.text = clave_acceso[8:-39]
    estab = ET.SubElement(infoTributaria, "estab")
    estab.text = clave_acceso[24:-22]
    ptoEmi = ET.SubElement(infoTributaria, "ptoEmi")
    ptoEmi.text = clave_acceso[27:-19]
    secuencial = ET.SubElement(infoTributaria, "secuencial")
    secuencial.text = clave_acceso[30:-10]
    dirMatriz = ET.SubElement(infoTributaria, "dirMatriz")
    dirMatriz.text = getdirMatriz(ruc)

    rimpe = getregimen(ruc)
    if rimpe:
        vacio = len(rimpe.strip()) 
        if vacio > 0:
            contribuyenteRimpe = ET.SubElement(infoTributaria, "contribuyenteRimpe")
            contribuyenteRimpe.text = getregimen(ruc)


    infoFactura = ET.SubElement(factura, "infoNotaCredito")
    fechaEmision = ET.SubElement(infoFactura, "fechaEmision")
    femi = (clave_acceso[0:-47] + '/' +
    clave_acceso[2:-45] + '/' + clave_acceso[4:-41])
    fechaEmision.text = femi

    dirEstablecimiento = ET.SubElement(infoFactura, "dirEstablecimiento")
    dirEstablecimiento.text = getdirEstablecimiento(clave_acceso[24:-22])

    objespecial = getEspecial(ruc)
    if not objespecial:
        contribuyenteEspecial = ET.SubElement(infoFactura,
             "contribuyenteEspecial")
        contribuyenteEspecial.text = objespecial[0][0]
 

    #guiaRemision = ET.SubElement(factura, "guiaRemision")
    #guiaRemision.text = ""
    datosComprador = getComprador_NC(clave_acceso)
    tipoIdenComprador = datosComprador[0][0]
    rucComprador = datosComprador[0][1]
    nombreComprador = datosComprador[0][2]
    direcciondelComprador = datosComprador[0][4] 
    tipoI1,tipoI2= tipoIdenComprador.split('-')

    tipoIdentificacionComprador = ET.SubElement(infoFactura,"tipoIdentificacionComprador")
    tipoIdentificacionComprador.text = tipoI2

    razonSocialComprador = ET.SubElement(infoFactura, "razonSocialComprador")
    razonSocialComprador.text = nombreComprador

    identificacionComprador = ET.SubElement(infoFactura, "identificacionComprador")
    identificacionComprador.text = rucComprador


    #direccionComprador = ET.SubElement(infoFactura, "direccionComprador")
    #direccionComprador.text = direcciondelComprador

    #totaldescuento,totalsinimpuestos,importetotal,formapago,iva_valor
    datosSubyDesc = getSubtotalyDesc_NC(clave_acceso)

    obligadoContabilidad = ET.SubElement(infoFactura, "obligadoContabilidad")
    obligadoContabilidad.text = getContabilidad(ruc)
    
    codDocModificado = ET.SubElement(infoFactura, "codDocModificado")
    codDocModificado.text = "01"

    numDocModificado = ET.SubElement(infoFactura, "numDocModificado")
    numDocModificado.text = docnc.numdocmodificado

    fechaEmisionDocSustento = ET.SubElement(infoFactura, "fechaEmisionDocSustento")
    fechaEmisionDocSustento.text =   docnc.fechaemisiondocsustento



    totalSinImpuesto = ET.SubElement(infoFactura, "totalSinImpuestos")
    totalSinImpuesto.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    valorModificacion = ET.SubElement(infoFactura, "valorModificacion")
    valorModificacion.text = '{0:.2f}'.format(datosSubyDesc[0][2])

    totalConImpuestos = ET.SubElement(infoFactura,"totalConImpuestos")
    totalImpuesto = ET.SubElement(totalConImpuestos,"totalImpuesto")

    codigo = ET.SubElement(totalImpuesto, "codigo")
    codigo.text = "2"

    codigoPorcentaje = ET.SubElement(totalImpuesto, "codigoPorcentaje")
    codigoPorcentaje.text = '0'#getCodigoImpuestoSri(clave_acceso)

    
    baseImponible = ET.SubElement(totalImpuesto, "baseImponible")
    baseImponible.text = '{0:.2f}'.format(datosSubyDesc[0][1])

    valor = ET.SubElement(totalImpuesto, "valor")
    valor.text = '{0:.2f}'.format(datosSubyDesc[0][4])

    

 

    motivo = ET.SubElement(infoFactura, "motivo")
    motivo.text = "DOLAR"

    #formapago = str(datosSubyDesc[0][3])
    #codfp,txt = formapago.split('-')
    #pagos  = ET.SubElement(infoFactura, "pagos")
    #pago = ET.SubElement(pagos, "pago")
    #formaPago = ET.SubElement(pago, "formaPago")
    #formaPago.text = codfp
    #total = ET.SubElement(pago, "total")
    #total.text =  '{0:.2f}'.format(datosSubyDesc[0][2])

    detalles = ET.SubElement(factura, "detalles")
   
    productos = getProductos_NC(clave_acceso)
    for p in productos:
        detalle = ET.SubElement(detalles, "detalle")
        codigoInterno = ET.SubElement(detalle, "codigoInterno")
        codigoInterno.text = p[5]

        #codigoAuxiliar = ET.SubElement(detalle, "codigoAuxiliar")
        #codigoAuxiliar.text = p[5]

        descripcion = ET.SubElement(detalle, "descripcion")
        descripcion.text = p[1]

        cantidad = ET.SubElement(detalle, "cantidad")
        cantidad.text = '{0:.2f}'.format(p[3])

        precioUnitario = ET.SubElement(detalle, "precioUnitario")
        precioUnitario.text = '{0:.2f}'.format(p[2])

        descuento = ET.SubElement(detalle, "descuento")
        descuento.text = '{0:.2f}'.format(p[4])

        precioTotalSinImpuesto = ET.SubElement(detalle, "precioTotalSinImpuesto")

        precioTotalSinImpuesto.text =   '{0:.2f}'.format(float(p[6]))

        impuestos_prod =  ET.SubElement(detalle, "impuestos")
        impuesto_prod =  ET.SubElement(impuestos_prod, "impuesto")
        codigo_prod =  ET.SubElement(impuesto_prod, "codigo")
        codigo_prod.text = "2"

        codigoPorcentaje_prod =  ET.SubElement(impuesto_prod, "codigoPorcentaje")
        codigoPorcentaje_prod.text = str(p[8])

        tarifa_prod =  ET.SubElement(impuesto_prod, "tarifa")
        tarifa_prod.text =  str(p[9])

        baseImponible_prod =  ET.SubElement(impuesto_prod, "baseImponible")
        baseImponible_prod.text =  '{0:.2f}'.format(p[6])

        valor_prod =  ET.SubElement(impuesto_prod, "valor")
        valor_prod.text =  '{0:.2f}'.format(p[7])

    infosAdd = getinfosAdd_NC(clave_acceso)
    infoAdicional =  ET.SubElement(factura, "infoAdicional")
   
    for info in infosAdd:
        campoAdicional =  ET.SubElement(infoAdicional, "campoAdicional")
        campoAdicional.text = info[1]
        campoAdicional.set('nombre', info[0])

    tree = ET.ElementTree(factura)
    tree.write(rutafile, xml_declaration=True, encoding='utf-8', method="xml")
    #frappe.throw("fin   "+rutafile)
    os.system("""/home/frappe/frappe-bench/callfac.sh {0} {1} """.format(frappe.get_site_config().db_name , ruc) )
    return rutafile

def genenarXmlRetencion(clave_acceso,doc):     
    ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    directorio = ruta + "/xmls"
    rutafile = directorio + "/" + clave_acceso + ".xml"

    if not os.path.exists(directorio):
        os.makedirs(directorio)
   
    ruc = clave_acceso[10:-26]
    
    comprobanteRetencion = ET.Element('comprobanteRetencion')
    comprobanteRetencion.set("id", "comprobante")
    comprobanteRetencion.set("version", "1.0.0")
    infoTributaria = ET.SubElement(comprobanteRetencion, "infoTributaria")
    ambiente = ET.SubElement(infoTributaria, "ambiente")
    ambiente.text = clave_acceso[23:-25]
    tipoEmision = ET.SubElement(infoTributaria, "tipoEmision")
    tipoEmision.text = clave_acceso[47:-1]
    razonSocial = ET.SubElement(infoTributaria, "razonSocial")
    razonSocial.text = getRazonSocial(ruc)
    nombreComercial = ET.SubElement(infoTributaria, "nombreComercial")
    
    nombrecomer = getNombreComercial(ruc)
    if  nombrecomer  != "" and nombrecomer  is not None :
        nombreComercial.text = nombrecomer
    
    _ruc = ET.SubElement(infoTributaria, "ruc")
    _ruc.text = ruc
    
    claveAcceso = ET.SubElement(infoTributaria, "claveAcceso")
    claveAcceso.text = clave_acceso
    codDoc = ET.SubElement(infoTributaria, "codDoc")
    codDoc.text = clave_acceso[8:-39]
    estab = ET.SubElement(infoTributaria, "estab")
    estab.text = clave_acceso[24:-22]
    ptoEmi = ET.SubElement(infoTributaria, "ptoEmi")
    ptoEmi.text = clave_acceso[27:-19]
    secuencial = ET.SubElement(infoTributaria, "secuencial")
    secuencial.text = clave_acceso[30:-10]
    dirMatriz = ET.SubElement(infoTributaria, "dirMatriz")
    dirMatriz.text = getdirMatriz(ruc)


    infoCompRetencion = ET.SubElement(comprobanteRetencion, "infoCompRetencion")
    fechaEmision = ET.SubElement(infoCompRetencion, "fechaEmision")
    femi = (clave_acceso[0:-47] + '/' + clave_acceso[2:-45] + '/' + clave_acceso[4:-41])
    fechaEmision.text = str(femi)
    
    dirEstablecimiento = ET.SubElement(infoCompRetencion, "dirEstablecimiento") 
    dirEstablecimiento.text =  getdirEstablecimiento(clave_acceso[24:-22])


    objespecial =getEspecial(ruc)
    if  objespecial != "" and objespecial is not None :
        contribuyenteEspecial = ET.SubElement(infoCompRetencion,"contribuyenteEspecial")
        contribuyenteEspecial.text = objespecial


    obligadoContabilidad = ET.SubElement(infoCompRetencion, "obligadoContabilidad")
    obligadoContabilidad.text =  getContabilidad(ruc)

    

    proveedor = frappe.get_doc('proveedores',doc.proveedores)
    tipoIden = '04'

    tipoIdentificacionSujetoRetenido = ET.SubElement(infoCompRetencion,"tipoIdentificacionSujetoRetenido")
    tipoIdentificacionSujetoRetenido.text = tipoIden

    razonSocialSujetoRetenido = ET.SubElement(infoCompRetencion, "razonSocialSujetoRetenido")
    razonSocialSujetoRetenido.text = proveedor.nombres

    identificacionSujetoRetenido = ET.SubElement(infoCompRetencion, "identificacionSujetoRetenido")
    identificacionSujetoRetenido.text = proveedor.ruc

    periodoFiscal = ET.SubElement(infoCompRetencion, "periodoFiscal")
    femi = (clave_acceso[2:-45] + '/' + clave_acceso[4:-41])
    periodoFiscal.text = str(femi)

    

    impuestos = ET.SubElement(comprobanteRetencion, "impuestos")
    arrayFecha =  datetime.strptime(str(doc.fecha_del_documento_a_modificar), '%Y-%m-%d')
    dia = arrayFecha.strftime("%d")
    mes = arrayFecha.strftime("%m")
    anio = arrayFecha.strftime("%Y")
    fecha = dia +"/"+ mes+"/" + anio

    #comprobanteRetencion 
    for p in doc.comprobante_retencion_detalle:
        detalle = ET.SubElement(impuestos, "impuesto")
        codigo = ET.SubElement(detalle, "codigo")
        if p.tipo== "IVA":
            num="2"
        else:
            num="1"
        codigo.text = num

        codigoRetencion = ET.SubElement(detalle, "codigoRetencion")
        codigoRetencion.text = p.codigoretencion

    
        baseImponible =  ET.SubElement(detalle, "baseImponible")
        baseImponible.text =  '{0:.2f}'.format(p.baseimponible)
        porcentajeRetener =  ET.SubElement(detalle, "porcentajeRetener")
        porcentajeRetener.text =  '{0:.2f}'.format(p.porcentajeretener)
        valorRetenido =  ET.SubElement(detalle, "valorRetenido")
        valorRetenido.text =  '{0:.2f}'.format(p.valorretenido)
        
        codDocSustento =  ET.SubElement(detalle, "codDocSustento")
        codDocSustento.text =  "01"
        numDocSustento =  ET.SubElement(detalle, "numDocSustento")
        numDocSustento.text =  doc.numdocsustento
        fechaEmisionDocSustento =  ET.SubElement(detalle, "fechaEmisionDocSustento")
        fechaEmisionDocSustento.text =  fecha

    infoAdicional =  ET.SubElement(comprobanteRetencion, "infoAdicional")
    for i in doc.informacion_adicional:
        campoAdicional =  ET.SubElement(infoAdicional, "campoAdicional")
        campoAdicional.text = i.descripcion
        campoAdicional.set('nombre',i.nombre)
    tree = ET.ElementTree(comprobanteRetencion)
    tree.write(rutafile, xml_declaration=True, encoding='UTF-8', method="xml")
    #frappe.throw("fin   "+rutafile)
    return rutafile


def getRazonSocial(ruc):
    obj_rz = frappe.db.sql("""SELECT razon_social FROM   tabEmisor""" )
    if not obj_rz:
        frappe.throw("No se ha establecido la Razon Social ")
    sri_rz = obj_rz[0][0]
    return sri_rz


def getregimen(ruc):
    obj_rz = frappe.db.sql("""SELECT regimen FROM   tabEmisor""" )
    if not obj_rz:
        sri_rz = ""
    sri_rz = obj_rz[0][0]
    return sri_rz


def getNombreComercial(ruc):
    obj_nc = frappe.db.sql("""SELECT nombre_comercial FROM   tabEmisor""")
    if not obj_nc:
        #frappe.throw("No se ha establecido la Razon Social")
        sri_nc=getRazonSocial(ruc)
    sri_nc = obj_nc[0][0]
    return sri_nc

def getdirMatriz(ruc):
    obj_rm = frappe.db.sql("""SELECT direccion_establecimiento FROM   tabEmisor""")
    if not obj_rm:
        frappe.throw("No se ha establecido la dirección Matriz")
    sri_rm = obj_rm[0][0]
    return sri_rm

def getdirEstablecimiento(estab):
    sqldirest = "SELECT direccion  FROM   tabAlmacenes where establecimiento='{0}'".format(estab)
    
    obj_direst = frappe.db.sql(sqldirest )
    if not obj_direst:
        frappe.throw("No se ha establecido la dirección del Almacén o establecimiento ==> " + estab )
    sri_direst = obj_direst[0][0]
    return sri_direst

def getContabilidad(ruc):
    obj_conta = frappe.db.sql("""SELECT obligado   from tabEmisor  """ )
    sri_conta = obj_conta[0][0]
    return sri_conta

def getEspecial(ruc):
    obj_especial = frappe.db.sql("""SELECT  contribuyente  from tabEmisor  """ )
    return obj_especial

def getComprador(clave_acceso):
    sql = """select  tipo_identificacion, ruc,nombres,correo,direccion 
    from tabClientes  
    where name =(SELECT cliente FROM  `tabFactura en Ventas` where clavedeacceso='{0}') """.format(clave_acceso)
    obj_comprador = frappe.db.sql(sql)
    return obj_comprador

def getComprador_NC(clave_acceso):
    sql = """select  tipo_identificacion, ruc,nombres,correo,direccion 
    from tabClientes  
    where name =(SELECT cliente FROM  tabnotasCredito  where clavedeacceso='{0}') """.format(clave_acceso)
    obj_comprador = frappe.db.sql(sql)
    return obj_comprador


def getSubtotalyDesc(clave_acceso):
    obj_select = frappe.db.sql("""SELECT totaldescuento,totalsinimpuestos + subtotal0 as totalsinimpuestos,importetotal,formapago,iva_total FROM  `tabFactura en Ventas` where clavedeacceso =%s""",clave_acceso)
    return obj_select

def getSubtotalyDesc_NC(clave_acceso):
    obj_select = frappe.db.sql("""SELECT totaldescuento,totalsinimpuestos + subtotal0 as totalsinimpuestos ,importetotal,' ' as formapago,iva_total FROM  tabnotasCredito where clavedeacceso =%s""",clave_acceso)
    return obj_select
 

def getCodigosImpuestoSri(clave_acceso):
    sql = """ select DISTINCT  i.tarifa , i.sri_iva 
        from tabdetalles_facventas  d
        inner join   `tabFactura en Ventas` v on ( d.parent = v.name)
        inner join tabProductos p on ( p.name = d.producto)
        inner join `tabImpuestos Venta` i on ( i.name  = p.impuesto  )
        where v.clavedeacceso ='{0}' """.format(clave_acceso)
    obj_select = frappe.db.sql(sql)
    return obj_select[0] 

 

 

def getProductos(clave_acceso):
    sql ="""   select  totallinea,
         p.nombre,
        preciounitario,
        dv.cantidad,
        descuento,
        IFNULL(p.codigo, p.name)  codigo,
        preciototalsinimpuesto,
        iva_valor  ,
        i.sri_iva ,
        i.tarifa 
        from tabdetalles_facventas dv  inner join tabProductos  p on ( dv.producto = p.name )
        inner join `tabImpuestos Venta` i on ( i.name  = p.impuesto  )
         where dv.parent in ( select name from `tabFactura en Ventas`  where clavedeacceso ='{0}')   """.format(clave_acceso)
    
    productos = frappe.db.sql(sql)
    return productos


def getProductos_NC(clave_acceso):
    sql ="""   select 
         totallinea,
         p.nombre,
        preciounitario,
        dv.cantidad,
        descuento,
        IFNULL(p.codigo, p.name)  codigo,
        preciototalsinimpuesto,
        iva_valor  ,
        i.sri_iva ,
        i.tarifa 
        from tabdetalles_notascredito dv  inner join tabProductos  p on ( dv.producto = p.name )
        inner join `tabImpuestos Venta` i on ( i.name  = p.impuesto  )
         where dv.parent in ( select name from tabnotasCredito  where clavedeacceso ='{0}')   """.format(clave_acceso)
    
    productos = frappe.db.sql(sql)
    return productos

def getinfosAdd(clave_acceso):
    productos = frappe.db.sql(""" SELECT  nombre, valor FROM   tabinfoAdicional where   parent = ( select name from `tabFactura en Ventas`  where clavedeacceso =%s) """,(clave_acceso))
    return productos

def getinfosAdd_NC(clave_acceso):
    productos = frappe.db.sql(""" SELECT  nombre, valor FROM   tabinfoAdicional where   parent = ( select name from tabnotasCredito  where clavedeacceso =%s) """,(clave_acceso))
    return productos

def envio_correo(clavedeacceso):
    cod = clavedeacceso[8:-39]
    if cod == "01":
        envio_correo_factura(clavedeacceso)
    if cod == "04":
        envio_correo_nc(clavedeacceso)

def envio_correo_nc(clavedeacceso):
    correos =[]
    fattachments = []
    notacredito = frappe.get_doc("notasCredito", {"clavedeacceso":str(clavedeacceso)})
    pdfnc =frappe.attach_print('notasCredito', notacredito.name, print_format='RIDE-NC',  file_name= notacredito.num_nota)
    fattachments.append(pdfnc)  
  
    datosComprador = frappe.get_doc('Clientes',notacredito.cliente)
    correos.append(datosComprador.correo or "sin@correo.com") 
    if datosComprador.correo != "sin@correo.com" :
        get_subject =  "NOTA DE CRÉDITO: {0}.".format( notacredito.num_nota)
        get_content = "Estimado(a) "+ datosComprador.nombres + "  se remite la nota de crédito electrónica: "+ notacredito.num_nota + 'Se adjunta documento PDF.' 
        if frappe.db.exists("Email Template", 'NOTACREDITO'):
            email_template = frappe.get_doc("Email Template", 'NOTACREDITO')
            emisor = frappe.get_doc("Emisor", notacredito.emisor )        
            context = {"doc": notacredito , "emisor":emisor}
            get_subject  = frappe.render_template(email_template.subject, context)
            get_content = frappe.render_template(email_template.response, context)
        frappe.sendmail( reference_doctype='notasCredito',reference_name= notacredito.name,	recipients= correos,subject=get_subject, message=get_content,attachments=fattachments,delayed=False)
        print ("enviado " +str(datosComprador.correo ))

def envio_correo_factura(clavedeacceso):
    correos =[]
    fattachments = []
    factura = frappe.get_doc("Factura en Ventas", {"clavedeacceso":str(clavedeacceso)})
    pdfFactura =frappe.attach_print('Factura en Ventas', factura.name, print_format='RIDE-FACTURA',  file_name= factura.num_fac)
    fattachments.append(pdfFactura)  
  
    datosComprador = frappe.get_doc('Clientes',factura.cliente)
    correos.append(datosComprador.correo or "sin@correo.com") 
    if datosComprador.correo != "sin@correo.com" :
        get_subject =  "FACTURA: {0}.".format( factura.name)
        get_content = "Estimado(a) "+ datosComprador.nombres + "  se remite la factura electronica: "+ factura.num_fac + 'Se adjunta documento PDF.' 
        if frappe.db.exists("Email Template", 'FACTURA'):
            email_template = frappe.get_doc("Email Template", 'FACTURA')
            emisor = frappe.get_doc("Emisor", factura.emisor )        
            context = {"doc": factura , "emisor":emisor , "cliente":datosComprador }
            get_subject  = frappe.render_template(email_template.subject, context)
            get_content = frappe.render_template(email_template.response, context)
        test = frappe.sendmail( reference_doctype='Factura en Ventas',reference_name= factura.name,	recipients= correos,subject=get_subject, message=get_content,attachments=fattachments,delayed=False)
        print ("enviado " +str(datosComprador.correo ))


def get_svg_from_text_code128(value):
        #example : code128
        code = barcode.get('code128', value)
        options={
        'module_width':0.3,
            'module_height':13,
            'font_size':12,
            'text_distance':4,
            'quiet_zone':5
        }    

        #create the temp file
        f = tempfile.NamedTemporaryFile()
        namer=f.name
        f.close()
        code.save(namer,options)

        #read the file    
        contents=''
        with open(namer+'.svg') as ff:
            contents=ff.read()
            ff.close()
            
        #remove the temp file
        os.remove(namer+'.svg')
            
        #get the svg tab only part
        pos=contents.find('<svg ')
        final_content=''
        after_svg_tag=''
        if pos>=0:
            after_svg_tag=contents[pos+len('<svg '):]
            final_content='<svg data-barcode-value="'+value+'" '+after_svg_tag	    

        return final_content

def generarXMLGuiaRemision(doc,clave_acceso):
 
    ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    directorio = ruta + "/xmls"
    rutafile = directorio + "/" + clave_acceso + ".xml"

    if not os.path.exists(directorio):
        os.makedirs(directorio)

  
    obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)[0][0]
    emisor =   frappe.get_doc("Emisor",obj_ruc)

    ruc = clave_acceso[10:-26]
    datosDestinatario = frappe.get_doc('Clientes',doc.cliente)
    guiaRemision = ET.Element('guiaRemision')
    guiaRemision.set("id", "comprobante")
    guiaRemision.set("version", "1.1.0")
    infoTributaria = ET.SubElement(guiaRemision, "infoTributaria")
    ambiente = ET.SubElement(infoTributaria, "ambiente")
    ambiente.text = clave_acceso[23:-25]
    tipoEmision = ET.SubElement(infoTributaria, "tipoEmision")
    tipoEmision.text = clave_acceso[47:-1]
    razonSocial = ET.SubElement(infoTributaria, "razonSocial")
    razonSocial.text = emisor.razon_social
    nombreComercial = ET.SubElement(infoTributaria, "nombreComercial")
    
    nombrecomer = emisor.nombre_comercial
    if  nombrecomer  != "" and nombrecomer  is not None :
        nombreComercial.text = nombrecomer
    else:
        nombreComercial.text = emisor.razon_social
    _ruc = ET.SubElement(infoTributaria, "ruc")
    _ruc.text = ruc

    claveAcceso = ET.SubElement(infoTributaria, "claveAcceso")
    claveAcceso.text = clave_acceso
    codDoc = ET.SubElement(infoTributaria, "codDoc")
    codDoc.text = clave_acceso[8:-39]
    estab = ET.SubElement(infoTributaria, "estab")
    estab.text = clave_acceso[24:-22]
    ptoEmi = ET.SubElement(infoTributaria, "ptoEmi")
    ptoEmi.text = clave_acceso[27:-19]
    secuencial = ET.SubElement(infoTributaria, "secuencial")
    secuencial.text = clave_acceso[30:-10]
    dirMatriz = ET.SubElement(infoTributaria, "dirMatriz")
    dirMatriz.text = emisor.direccion_establecimiento

    #------infoGuiaRemision
    infoGuiaRemision = ET.SubElement(guiaRemision, "infoGuiaRemision")

    dirEstablecimiento = ET.SubElement(infoGuiaRemision, "dirEstablecimiento") 
    dirEstablecimiento.text =  emisor.direccion_establecimiento

    dirPartida = ET.SubElement(infoGuiaRemision, "dirPartida") 
    dirPartida.text =  doc.dirpartida

    datosTransportista = frappe.get_doc('transportistas',doc.transportistas)
    tipoIdenTransportista = datosTransportista.tipoidentificaciontransportista

    razonSocialTransportista =ET.SubElement(infoGuiaRemision, "razonSocialTransportista") 
    razonSocialTransportista.text =  datosTransportista.nombres

    tipoIdentificacionTransportista = ET.SubElement(infoGuiaRemision,"tipoIdentificacionTransportista")
    tipoIdentificacionTransportista.text = tipoIdenTransportista[0:2]

    rucTransportista =ET.SubElement(infoGuiaRemision, "rucTransportista") 
    rucTransportista.text =  datosTransportista.identificaciontransportista
    
    #rise =ET.SubElement(infoGuiaRemision, "rise") 
    #rise.text =  "Contribuyente Regimen Simplificado RISE"
   
    obligadoContabilidad = ET.SubElement(infoGuiaRemision, "obligadoContabilidad")
    obligadoContabilidad.text = emisor.obligado   

    objespecial = emisor.contribuyente
    if  objespecial != "" and objespecial is not None :
        contribuyenteEspecial = ET.SubElement(infoGuiaRemision,"contribuyenteEspecial")
        contribuyenteEspecial.text = objespecial

    fechainitransporte = ET.SubElement(infoGuiaRemision, "fechaIniTransporte")
    arrayFecha =  datetime.strptime(str(doc.fechainitransporte), '%Y-%m-%d')
    dia = arrayFecha.strftime("%d")
    mes = arrayFecha.strftime("%m")
    anio = arrayFecha.strftime("%Y")
    fecha = dia +"/"+ mes+"/" + anio
    fechainitransporte.text = fecha

    fechafintransporte = ET.SubElement(infoGuiaRemision, "fechaFinTransporte")
    arrayFecha =  datetime.strptime(str(doc.fechafintransporte), '%Y-%m-%d')
    dia = arrayFecha.strftime("%d")
    mes = arrayFecha.strftime("%m")
    anio = arrayFecha.strftime("%Y")
    fecha = dia +"/"+ mes+"/" + anio
    fechafintransporte.text = fecha

    placa = ET.SubElement(infoGuiaRemision, "placa")
    placa.text = doc.placa       

    ##################################################

    destinatarios = ET.SubElement(guiaRemision, "destinatarios")

    destinatario = ET.SubElement(destinatarios, "destinatario")

    identificacionDestinatario = ET.SubElement(destinatario,"identificacionDestinatario")
    identificacionDestinatario.text = datosDestinatario.ruc

    razonSocialDestinatario = ET.SubElement(destinatario, "razonSocialDestinatario")
    razonSocialDestinatario.text = datosDestinatario.nombres

    dirDestinatario = ET.SubElement(destinatario, "dirDestinatario")
    dirDestinatario.text = datosDestinatario.direccion

    motivoTraslado = ET.SubElement(destinatario, "motivoTraslado")
    motivoTraslado.text = doc.motivotraslado

    if doc.documento_aduanero != "" and doc.documento_aduanero  is not None :
        docAduaneroUnico = ET.SubElement(destinatario, "docAduaneroUnico")
        docAduaneroUnico.text = doc.documento_aduanero

    if doc.cod_estab_destinoopcional != "" and doc.cod_estab_destinoopcional  is not None :
        codEstabDestino = ET.SubElement(destinatario, "codEstabDestino")
        codEstabDestino.text = doc.cod_estab_destinoopcional

    ruta = ET.SubElement(destinatario, "ruta")
    ruta.text = doc.ruta

    if doc.numero_de_autorizacion != "" and doc.numero_de_autorizacion  is not None :
        codDocSustento = ET.SubElement(destinatario, "codDocSustento")
        num_autorizacion=doc.numero_de_autorizacion
        codDocSustento.text =  num_autorizacion[8:-39]

    if doc.numdocsustento != "" and doc.numdocsustento  is not None :       
        numDocSustento =  ET.SubElement(destinatario, "numDocSustento")
        numDocSustento.text =  doc.numdocsustento

    if doc.numero_de_autorizacion != "" and doc.numero_de_autorizacion  is not None :
        numAutDocSustento = ET.SubElement(destinatario, "numAutDocSustento")
        numAutDocSustento.text =  doc.numero_de_autorizacion

    if doc.fecha_de_emision != "" and doc.fecha_de_emision  is not None :
        arrayFecha =  datetime.strptime(str(doc.fecha_de_emision), '%Y-%m-%d')
        dia = arrayFecha.strftime("%d")
        mes = arrayFecha.strftime("%m")
        anio = arrayFecha.strftime("%Y")
        fecha = dia +"/"+ mes+"/" + anio
        fechaEmisionDocSustento =  ET.SubElement(destinatario, "fechaEmisionDocSustento")
        fechaEmisionDocSustento.text =  fecha

    ##################################################

    detalles = ET.SubElement(destinatario, "detalles")  
    for p in doc.detalle_remision:
        detalle = ET.SubElement(detalles, "detalle")
        codigoInterno = ET.SubElement(detalle, "codigoInterno")
        codigoInterno.text = p.producto

        producto = frappe.get_doc("Productos",p.producto)

        descripcion = ET.SubElement(detalle, "descripcion")
        descripcion.text = producto.nombre

        cantidad = ET.SubElement(detalle, "cantidad")
        cantidad.text = '{0:.2f}'.format(p.cantidad)


##################################################
    infoAdicional =  ET.SubElement(guiaRemision, "infoAdicional")
    for i in doc.infoadicional:
        campoAdicional =  ET.SubElement(infoAdicional, "campoAdicional")
        campoAdicional.text = i.valor
        campoAdicional.set('nombre',i.nombre)
    
     
    tree = ET.ElementTree(guiaRemision)
    tree.write(rutafile, xml_declaration=True, encoding='UTF-8', method="xml")
    os.system("""/home/frappe/frappe-bench/callfac.sh {0} {1} """.format(frappe.get_site_config().db_name , ruc) )
    return rutafile