# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, os
from frappe.model.document import Document

class Emisor(Document):
    def validate(self):
        ruta = os.path.abspath(frappe.get_site_path("public", "files"))
        directorio = ruta + "/xmls/"
        self.rutaxml = directorio
        self.rutafiles = ruta


def setemisor():
    sql = """ select parent from tabemisor_usuarios where usuario='{0}' """.format(frappe.session.user)
    emisor = frappe.db.sql(sql )
    if emisor:
        #frappe.throw(str(emisor[0][0]))
        objemisor = frappe.get_doc("Emsior",emisor)
        frappe.defaults.set_user_default("emisor", emisor[0][0])
        frappe.defaults.set_user_default("iniciales", objemisor.iniciales)
       