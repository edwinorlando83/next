# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class dpa(Document):
	pass

@frappe.whitelist()
def get_provincias():
	sql = """ select distinct dpa_nprovincia  from tabdpa  order by 1  """
	return frappe.db.sql(sql, as_list = True)

@frappe.whitelist()
def get_cantones(provincia):
	sql = """  select distinct dpa_ncanton  
		from tabdpa  
		where dpa_nprovincia ='{0}' 
		order by 1   """.format(provincia)
	return frappe.db.sql(sql, as_list=True)


@frappe.whitelist()
def get_parroquia(provincia,canton):
	sql = """  select   dpa_nparroquia 
  from tabdpa 
  where dpa_nprovincia ='{0}'  and dpa_ncanton ='{1}' order by 1   """.format(provincia,canton)
	return frappe.db.sql(sql, as_list = True)