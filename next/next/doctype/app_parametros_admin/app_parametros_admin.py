# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document

class app_parametros_admin(Document):
	
	@frappe.whitelist()
	def export_tra(self):
		lst = frappe.get_all("Translation",fields=["source_text","translated_text"])
		with open("/home/frappe/frappe-bench/apps/next/datatraduccion.json", "w") as outfile:
			json.dump(lst, outfile)
		return "Exportado Correctamnete"
	
	@frappe.whitelist()
	def import_tra(self):
		
		with open('/home/frappe/frappe-bench/apps/next/datatraduccion.json', 'r') as openfile:
			json_object = json.load(openfile)
			for rw in json_object:	 
				tra = frappe.new_doc("Translation")
				tra.source_text = rw["source_text"]
				tra.translated_text = rw["translated_text"]
				tra.insert()
		return "Importado Correctamente"
	
	@frappe.whitelist()
	def borrar_pruebas(self):
		sql = """ update tabnotasCredito set docstatus=0 where sri_ambiente  <>  '2' or sri_ambiente is null """
		frappe.db.sql(sql)
		lstfac = frappe.db.get_list('notasCredito',{ "sri_ambiente":'1' })
		for fac in lstfac:			 
			docfac= frappe.get_doc("notasCredito",fac.name)
			docfac.delete()		

		
		sql = """ update `tabFactura en Ventas`  set docstatus=0 where sri_ambiente  <>  '2' or sri_ambiente is null """
		frappe.db.sql(sql)
		lstfac = frappe.db.get_list('Factura en Ventas',{ "sri_ambiente":'1'})
		 

		for fac in lstfac:
			frappe.db.delete("Kardex", {"factura_venta":fac.name}) 
			docfac= frappe.get_doc("Factura en Ventas",fac.name)
			docfac.delete()
		
		return "listo"

		