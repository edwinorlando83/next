// Copyright (c) 2022, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('app_parametros_admin', {
	// refresh: function(frm) {

	// }
	export_tra(frm) {
		frappe.call({
			doc: frm.doc,
			method: "export_tra",			
			callback: r => {      
		         alert(r.message);
			}
		  });
	},
	import_tra(frm) {
		frappe.call({
			doc: frm.doc,
			method: "import_tra",			
			callback: r => {      
		         alert(r.message);
			}
		  });
	},

	borrar_pruebas(frm) {
		frappe.call({
			doc: frm.doc,
			method: "borrar_pruebas",			
			callback: r => {      
		         alert(r.message);
			}
		  });
	}
});
