import frappe
import json

def after_install ():
    config_app()

#def boot_session():
#    frappe.defaults.set_user_default("unidad", "U")

def config_app():
    frappe.db.set_value('System Settings', None, 'allow_login_using_user_name', 1)
    frappe.db.set_value('Website Settings', None, 'app_name', 'Lexie')
    frappe.db.set_value('Website Settings', None, 'disable_signup', 1)
    sql = "update tabCurrency set enabled=0 where name <> 'USD' "
    frappe.db.sql(sql)

    impu = frappe.new_doc("Impuestos Venta")
    impu.nombre = "No Objeto de Impuesto"
    impu.tarifa = 0
    impu.sri_iva = "6"
    impu.insert()

    impu = frappe.new_doc("Impuestos Venta")
    impu.nombre = "IVA 0%"
    impu.tarifa = 0.0
    impu.sri_iva = "0"
    impu.insert()

    impu = frappe.new_doc("Impuestos Venta")
    impu.nombre = "IVA 14%"
    impu.tarifa = 14
    impu.sri_iva = "3"
    impu.insert()
    
    impu = frappe.new_doc("Impuestos Venta")
    impu.nombre = "IVA 12%"
    impu.tarifa = 12
    impu.sri_iva = "2"
    impu.insert()

    unid = frappe.new_doc("Unidad de Medida")
    unid.nombre = "Unidad"
    unid.codigo = "U"
    unid.insert()

    lp = frappe.new_doc("Lista de Precios")
    lp.nombre = "VENTAS" 
    lp.insert()


    caja = frappe.new_doc("lista_cajas")
    caja.nombre = "CAJA 1"
    caja.insert()

    pf =  frappe.new_doc("productos_familia") 
    pf.familia = "INVENTARIOS"
    pf.tipo = "BIENES"
    pf.insert()

    pf =  frappe.new_doc("productos_familia") 
    pf.familia = "SERVICIOS"
    pf.tipo = "SERVICIOS"
    pf.insert()

    pf =  frappe.new_doc("productos_familia") 
    pf.familia = "GASTOS GENERALES"
    pf.tipo = "SERVICIOS"
    pf.insert()

    alm =  frappe.new_doc("Almacenes") 
    alm.nombre = "PRINCIPAL"
    alm.direccion  = "n/a"
    alm.insert()



    roles=['CONTABILIDAD','EMPRESA - ADMINISTRADOR','Accounts User','Translator']
    rol=frappe.new_doc('Role Profile')     
    rol.role_profile="AdminEmpresa"
    rol.save()
    ix=1
    for r in roles:
            hr=frappe.new_doc('Has Role')
            hr.parent= rol.name
            hr.role=r
            hr.parentfield='roles'
            hr.parenttype='Role Profile'
            hr.idx=ix
            hr.save()
            ix=ix+1
    
    modprof = frappe.new_doc("Module Profile")
    modprof.module_profile_name = "ModEmpresa"
    modulosNoOcupa=["Workflow","Integrations","Custom","Contabilidad","Website","jaap","Core","Social","movil","Event Streaming"]
    for md in modulosNoOcupa:
        modprof.append("block_modules", {"module": md} )
    modprof.insert()

     


 