# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from next.utilidades.utils import *
logger = frappe.logger("next", allow_site=True, file_count=50)

class bom_orden(Document):
	def on_update(self):
		if self.docstatus == 1:
			for item in self.bom_orden_detalle:
				insert_kardex( "PRODUCCIÓN", self.ordp_almacen,item.bodp_producto,item.bodp_precio, item.bodp_cantidad,item.name )
	

	@frappe.whitelist()
	def getAtributos(self,ordp_producto ):
		sql =""" select DISTINCT atributo_productos  from tabproductos_lista_variantes  
 			where parent ='{0}' """.format( ordp_producto)
		return frappe.db.sql(sql,as_dict=False)
	
	@frappe.whitelist()
	def getValoresAtributos(self,atributo):
		sql ="""  select atri_valor  from tabatributo_valor where parent ='{0}'   order by 1 """.format(atributo)
		return frappe.db.sql(sql,as_list=1)
	
	@frappe.whitelist()
	def getSucursales(self,cliente_name):
		sql =""" SELECT  clsu_nombre, clsu_provincia , clsu_canton, clsu_parroquia, clsu_direccion,clsu_telefono 
  from tabcliente_sucursales 
	where  parent = '{0}' """.format(cliente_name)
		return frappe.db.sql(sql,as_dict=True)

	@frappe.whitelist()
	def guardarProductos(self, ordp_producto,ordp_atributos,ordp_at1,ordp_at2,ordp_at3,ordp_at4,ordp_at5,ordp_at6,ordp_at7,precio):
		valores = self.getValoresAtributos(ordp_atributos) 
		logger.info("valores - {0}".format(str(valores)))
		frappe.logger().debug(valores)
		 
		self.agregar(ordp_at1,valores[0][0],ordp_producto,ordp_atributos, precio)
		self.agregar(ordp_at2,valores[1][0],ordp_producto,ordp_atributos, precio)
		self.agregar(ordp_at3,valores[2][0],ordp_producto,ordp_atributos, precio)
		self.agregar(ordp_at4,valores[3][0],ordp_producto,ordp_atributos, precio)
		self.agregar(ordp_at5,valores[4][0],ordp_producto,ordp_atributos, precio)
		if len(valores) > 5:
			self.agregar(ordp_at6,valores[5][0],ordp_producto,ordp_atributos, precio)
		
		if len(valores) > 6:
			self.agregar(ordp_at7,valores[6][0],ordp_producto,ordp_atributos, precio)
		 

	@frappe.whitelist()
	def generarfactura(self):
		return generar_factura(self)

	@frappe.whitelist()
	def generarNc(self):
		return generar_Nc(self)		 

	def agregar(self,ordp_atx,talla,ordp_producto,ordp_atributos, precio):
		if int(ordp_atx) > 0 :
			existe = existeVariante(ordp_producto,ordp_atributos ,talla )
			if existe > 0:
				name_prod = getProductovariante(ordp_producto,ordp_atributos ,talla)
			else:
				name_prod = crearProducto(ordp_producto,ordp_atributos,talla)			
			
			self.append("bom_orden_detalle", {"bodp_producto":name_prod, "bodp_cantidad":ordp_atx,"bodp_precio":precio})			
			self.save()

def crearProducto(ordp_producto,ordp_atributos,atri_valor):


	maestro = frappe.get_doc("Productos", ordp_producto)                
	newprod = frappe.copy_doc(maestro)
	newprod.atributo_productos = []
	newprod.nombre = maestro.nombre + " "+ atri_valor
	newprod.variante_de   = maestro.name
	newprod.posee_variantes = False
	newprod.atributo_valor = get_atributo_valor(ordp_atributos,atri_valor)
	newprod.productos_precios = []
	setPrecios(newprod,maestro,ordp_atributos)
	newprod.save()
	return newprod.name

def get_atributo_valor(serie, talla):
	sql = """ select  name  from tabatributo_valor  
	where  parent = '{0}' and atri_valor ='{1}' """.format(serie,talla)
	return frappe.db.sql(sql)[0][0]

def existeVariante(producto_padre, serie, numero):
	sql = """  select    count(*)   from 
	tabProductos p
	inner join tabatributo_valor av on (p.atributo_valor = av.name)
	where variante_de ='{0}' and  av.parent ='{1}' and atri_valor = '{2}' """.format(producto_padre, serie, numero)
	 
	return frappe.db.sql(sql)[0][0]

def getProductovariante(producto_padre, serie, numero):
	sql = """  select    p.name  from 
	tabProductos p
	inner join tabatributo_valor av on (p.atributo_valor = av.name)
	where variante_de ='{0}' and  av.parent ='{1}' and atri_valor = '{2}' """.format(producto_padre, serie, numero)
	return frappe.db.sql(sql)[0][0]

def getPrecios(producto_padre,serie):
	sql = """select  lista_precios , precio  
	from tabproductos_lista_variantes 
	where parent = '{0}'   and atributo_productos = '{1}' """.format(producto_padre,serie)
	return frappe.db.sql(sql, as_dict = True)

def setPrecios(newprod, producto_padre,serie):
	for row in getPrecios(producto_padre,serie):
		newprod.append("productos_precios",{"lista_precios":row.lista_precios, "precio_siniva":row.precio, "precio":row.precio})


def generar_factura(doc):
	existesol = frappe.db.exists( "Solicitud", {"bom_orden":doc.name})
	if existesol:
		return {"existe":"SI", "name":existesol , "doc":"Solicitud", "msg":"Esta Orden de producción ya tienen una Nota de Venta / Otros Egresos"} 
	
	existefac = frappe.db.exists( "Factura en Ventas", {"bom_orden":doc.name})
	if existefac:
		return {"existe":"SI", "name":existefac , "doc":"Factura en Ventas", "msg":"Esta Orden de producción ya tiene una FACTURA"} 

	fac = frappe.new_doc("Factura en Ventas")
	fac.estab = doc.ordp_almacen
	fac.eslectronica = usa_facelec() 
	fac.cliente = doc.ordp_cliente
	fac.listaprecios =   doc.listaprecios
	fac.bajarstock = 1
	fac.bom_orden = doc.name
	fac.saldo = 0
	itotal=0

	for pro in doc.bom_orden_detalle:
		producto = frappe.get_doc("Productos", pro.bodp_producto)
		itotal = itotal+ pro.bodp_precio * pro.bodp_cantidad
		impven = frappe.get_doc("Impuestos Venta",producto.impuesto)
		fac.append('detalles_facventas', {
                    "producto":  pro.bodp_producto,
					 "nproducto":  producto.nombre,
                    "codigoprincipal":producto.codigo ,
                    "codigoauxiliar":producto.codigo ,
                    "descripcion": producto.nombre,
                    "cantidad":pro.bodp_cantidad ,
                    "preciounitario":pro.bodp_precio ,
                    "descuento": 0,
					"sri_iva": impven.sri_iva,
                    "preciototalsinimpuesto":  pro.bodp_precio * pro.bodp_cantidad,
                    "tarifa_iva": 0,
                    "iva_valor": 0,
                    "totallinea":pro.bodp_precio * pro.bodp_cantidad  
                })
	fac.subtotal0 =  itotal
	fac.iva_total  =  0.0
	fac.totalsinimpuestos  = 0.0
	fac.totaldescuento  =  0.0
	fac.importetotal =  itotal
	cli = frappe.get_doc("Clientes",doc.ordp_cliente)
	correo = cli.correo
	if not correo:
		correo = 'sin@correo.com'
    		
	fac.append('infoadicional', {"nombre":"correo" , "valor": correo})
	
	fac.insert()
	return {"existe":"NO","doc":"Factura en Ventas", "name":fac.name , "msg":"Se ha generado una Factura"} 
	 


def generar_Nc(doc):
	existesol = frappe.db.exists( "Solicitud", {"bom_orden":doc.name})
	if existesol:
		return {"existe":"SI", "doc":"Solicitud", "name":existesol , "msg":"Esta Orden de producción ya tienen una Nota de Venta / Otros Egresos"} 
	
	existefac = frappe.db.exists( "Factura en Ventas", {"bom_orden":doc.name})
	if existefac:
		return {"existe":"SI", "doc":"Factura en Ventas", "name":existefac , "msg":"Esta Orden de producción ya tiene una FACTURA"} 


	fac = frappe.new_doc("Solicitud")
	fac.estab = doc.ordp_almacen
	fac.tipo = "OTROS" 
	fac.cliente = doc.ordp_cliente
	fac.listaprecios =  doc.listaprecios
	fac.bajarstock = 1
	fac.bom_orden = doc.name
	fac.saldo = 0
	itotal=0

	for pro in doc.bom_orden_detalle:
		producto = frappe.get_doc("Productos", pro.bodp_producto)
		itotal = itotal+ pro.bodp_precio * pro.bodp_cantidad
		impven = frappe.get_doc("Impuestos Venta",producto.impuesto)
		fac.append('detalle_solicitudcred', {
                    "producto":  pro.bodp_producto,
                    "codigoprincipal":producto.codigo ,
                    "codigoauxiliar":producto.codigo ,
                    "descripcion": producto.nombre,
                    "cantidad":pro.bodp_cantidad ,
                    "preciounitario":pro.bodp_precio ,
                    "descuento": 0,
					"sri_iva": impven.sri_iva,
                    "preciototalsinimpuesto":  pro.bodp_precio * pro.bodp_cantidad,
                    "tarifa_iva": 0,
                    "iva_valor": 0,
                    "totallinea":pro.bodp_precio * pro.bodp_cantidad  
                })
	fac.subtotal0 =  itotal
	fac.iva_total  =  0.0
	fac.totalsinimpuestos  = 0.0
	fac.totaldescuento  =  0.0
	fac.importetotal =  itotal
	 
	
	fac.insert()
	return {"existe":"NO","doc":"Solicitud", "name":fac.name , "msg":"Se ha generado una Nota de Venta u Otros Egresos"} 