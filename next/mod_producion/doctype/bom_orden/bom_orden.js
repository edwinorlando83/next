// Copyright (c) 2022, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('bom_orden', {
	refresh: function (frm) {
		frm.set_query("ordp_producto", function (doc, cdt, cdn) {
			return {
				filters: { posee_variantes: 1 }
			}
		});

		if( cur_frm.doc.ordp_cliente ){
			getSucursales();
		}

		cur_frm.add_custom_button(__('Generar Nota Venta'), function () {
			generarNv();
		  }).css({'background-color':'#0B50FC','font-weight': 'bold' , 'color':'white'});
		cur_frm.add_custom_button(__('Generar Factura'), function () {
			generarfactura();
		  }).css({'background-color':'#FCC60B','font-weight': 'bold' , 'color':'black'});


		return new Promise((resolve) => {
			return frappe.db.get_single_value("configuraciones_generales","cnfg_inv_tallas")
				.then(value => {
					cur_frm.set_df_property("btn_agregar", "hidden", value==0);
				});
		});

	
	} ,
	btn_agregar: function (frm) {
		showDlg();
	},
	ordp_cliente: function (frm) {
		getSucursales();
	},
 
});
function generarNv () {
	if (cur_frm.doc.docstatus==0){
	    frappe.msgprint(__("Primero valide el pedido")); return;
		 
	}
	frappe.confirm('¿Estas seguro que deseas continuar, se va ha generar una Nota de venta u Otros Egresos?',
	  () => {
		frappe.call({
		  method: 'generarNc',
		  doc: cur_frm.doc,
		  freeze: true,
		  callback: (r) => {  
			if (r.message.existe == 'SI')
			   frappe.msgprint(r.message.msg);

			frappe.set_route('Form', r.message.doc , r.message.name)
		  }
		})
	  }, () => {
		// action to perform if No is selected
	  })
  }
function generarfactura () {
	if (cur_frm.doc.docstatus==0){
	    frappe.msgprint(__("Primero valide el pedido")); return;
		 
	}
	frappe.confirm('¿Estas seguro que deseas continuar, se va ha generar una factura?',
	  () => {
		frappe.call({
		  method: 'generarfactura',
		  doc: cur_frm.doc,
		  freeze: true,
		  callback: (r) => {  
			if (r.message.existe == 'SI')
			
			   frappe.msgprint(r.message.msg);


			frappe.set_route('Form', r.message.doc  , r.message.name)
		  }
		})
	  }, () => {
		// action to perform if No is selected
	  })
  }
function setEtiquetas() {
	frappe.call({
		doc: cur_frm.doc,
		freeze: true,
		freeze_message: "Buscando...",
		method: "getAtributos",
		args: {
			ordp_producto: cur_frm.doc.ordp_producto,
		},
		callback: (r) => {
			cur_frm.set_df_property("ordp_atributos", "options", r.message);
		},
	});


}

function guardarPedido() {
	if (cur_frm.doc.__unsaved = 1) {
		cur_frm.save();
	}
}



function showDlg() {
	guardarPedido();
	let d = new frappe.ui.Dialog({
		title: 'Enter details',
		fields: [
			{
				"fieldname": "ordp_producto",
				"fieldtype": "Link",
				"label": "Producto",
				"options": "Productos",
				"width": "150",
				"get_query": function () {
					return {
						filters: { posee_variantes: 1 }
					}
				},
				onchange: function(e) {	

					frappe.call({
						doc: cur_frm.doc,
						freeze: true,
						freeze_message: "Buscando...",
						method: "getAtributos",
						args: {
							ordp_producto: cur_dialog.fields_dict.ordp_producto.value  ,
						},
						callback: (r) => {
							d.fields_dict.ordp_atributos.df.options =  r.message;
							d.fields_dict.ordp_atributos.refresh();
						 
						},
					});

				}
			},

			{
				"fieldname": "col_bre_prod",
				"fieldtype": "Column Break"
			},
			{
				"fieldname": "ordp_atributos",
				"fieldtype": "Select",
				"label": "Variantes",			 
				onchange: function(e) {	 
 
					 setTallas(cur_dialog.fields_dict.ordp_atributos.value, d);
				}
			},
			{
				"fieldname": "section_break_12",
				"fieldtype": "Section Break",
			 
			},
			{
				"default": "0",
				"fieldname": "ordp_at1",
				"fieldtype": "Int"
			},
			{
				"fieldname": "column_break_14",
				"fieldtype": "Column Break"
			},
			{
				"default": "0",
				"fieldname": "ordp_at2",
				"fieldtype": "Int"
			},
			{
				"fieldname": "column_break_14",
				"fieldtype": "Column Break"
			},
			{
				"default": "0",
				"fieldname": "ordp_at3",
				"fieldtype": "Int"
			},

			{
				"fieldname": "column_break_16",
				"fieldtype": "Column Break"
			},

			{
				"default": "0",
				"fieldname": "ordp_at4",
				"fieldtype": "Int"
			},
			{
				"fieldname": "column_break_20",
				"fieldtype": "Column Break"
			},
			{
				"default": "0",
				"fieldname": "ordp_at5",
				"fieldtype": "Int"
			},
			{
				"fieldname": "column_break_22",
				"fieldtype": "Column Break"
			},
			{
				"fieldname": "ordp_at6",
				"fieldtype": "Int",
				"default": "0",
			},
			{
				"fieldname": "column_break_24",
				"fieldtype": "Column Break"
			},
			{
				"fieldname": "ordp_at7",
				"fieldtype": "Int",
				"default": "0",
			}
			,
			{
				"fieldname": "column_break_255",
				"fieldtype": "Column Break"
			},
			{
				"fieldname": "precio",
				"label": "Precio",
				"fieldtype": "Currency",
				"default": "0",
			}
		],
		primary_action_label: 'Agregar',
		primary_action(values) {
			console.log(values);
			guardarProductos();
			d.hide();
		}
	});
	
	d.show();
	//d.$wrapper.find('.modal-dialog').css("width", "900px");
	d.$wrapper.find('.modal-dialog').css("max-width", "70%").css("width", "70%");
}


function setTallas(ordp_atributos,d) {
	 
	frappe.call({
		doc: cur_frm.doc,
		freeze: true,
		freeze_message: "Buscando...",
		args: {
			atributo: ordp_atributos,
		},
		method: "getValoresAtributos",
		callback: (r) => {
			let tallas = r.message;
			console.log(tallas);
			borrar(d);
			d.fields_dict.ordp_at1.set_label(tallas[0]);
			d.fields_dict.ordp_at2.set_label(tallas[1]);
			d.fields_dict.ordp_at3.set_label(tallas[2]);
			d.fields_dict.ordp_at4.set_label(tallas[3]);
			d.fields_dict.ordp_at5.set_label(tallas[4]);
			d.fields_dict.ordp_at6.set_label(tallas[5]);
			d.fields_dict.ordp_at7.set_label(tallas[6]);
		 
			d.fields_dict.ordp_at1.refresh();
		 
		},
	});

}

function borrar(cur_frm) {
	cur_frm.fields_dict.ordp_at1.set_label("");
	cur_frm.fields_dict.ordp_at2.set_label("");
	cur_frm.fields_dict.ordp_at3.set_label("");
	cur_frm.fields_dict.ordp_at4.set_label(" ");
	cur_frm.fields_dict.ordp_at5.set_label(" ");
	cur_frm.fields_dict.ordp_at6.set_label(" ");
	cur_frm.fields_dict.ordp_at7.set_label(" ");
	//cur_frm.fields_dict.ordp_at8.set_label(" ");

}
function guardarProductos(){
 
	frappe.call({
		doc: cur_frm.doc,
		freeze: true,
		freeze_message: "Guardando...",
		args: {
			ordp_producto:cur_dialog.fields_dict.ordp_producto.value ,
			ordp_atributos:cur_dialog.fields_dict.ordp_atributos.value,
			ordp_at1:cur_dialog.fields_dict.ordp_at1.value ,
			ordp_at2:cur_dialog.fields_dict.ordp_at2.value ,
			ordp_at3:cur_dialog.fields_dict.ordp_at3.value ,
			ordp_at4:cur_dialog.fields_dict.ordp_at4.value ,
			ordp_at5:cur_dialog.fields_dict.ordp_at5.value ,
			ordp_at6:cur_dialog.fields_dict.ordp_at6.value ,
			ordp_at7:cur_dialog.fields_dict.ordp_at7.value ,
			precio :cur_dialog.fields_dict.precio.value

		},
		method: "guardarProductos",
		callback: (r) => {
			 cur_frm.refresh();
			 cur_frm.refresh_field("bom_orden_detalle");
		},
	});

}

function getSucursales(){
	frappe.call({
		doc: cur_frm.doc,
		freeze: true,
		freeze_message: "Buscando...",
		method: "getSucursales",
		args: {
			cliente_name: cur_frm.doc.ordp_cliente  ,
		},
		callback: (r) => {
	 
			let array  = []
			r.message.forEach(function (item) {
				array.push(item.clsu_nombre);
			  }); 

		 
			  cur_frm.set_df_property("entregar_en", "options", array);
			  cur_frm.refresh_field("entregar_en")
		},
	});
	
}
