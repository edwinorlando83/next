from __future__ import unicode_literals
import frappe 
import datetime
 
def getcodigo(doc):
    if not doc.emisor:			 
        doc.emisor = frappe.defaults.get_user_default('emisor')     
    emisor = frappe.get_doc("Emisor",doc.emisor) 
    doc.codigo_pk =  emisor.iniciales + "_" + doc.nombre.replace(" ","_")

def getEmisor():
    lstemisor = frappe.db.get_list("Emisor" , fields=['name' ])
    if len(lstemisor) == 1:
        return lstemisor[0].name
    else:
        frappe.throw("No se ha configurado los datos de la Empresa")

def setEmisor(doc):
    if not doc.emisor:    
        doc.emisor = getEmisor()
         

@frappe.whitelist()
def usa_facelec():
    emisor_name =  getEmisor()
    emisor = frappe.get_doc("Emisor",emisor_name)
    return emisor.usa_facelec

@frappe.whitelist()
def usa_genera_secuencial():
    emisor_name =  getEmisor()
    emisor = frappe.get_doc("Emisor",emisor_name)
    siguiente = -1
    if emisor.es_secuencial_manual :
        siguiente = emisor.secuencial_manual  + 1 
        emisor.secuencial_manual = siguiente
        emisor.save()
        
    return siguiente 


def insert_kardex(tipo,almacen,producto,preciounitario,cantidad,doc_name):
    kardex = frappe.new_doc("Kardex")
    kardex.almacen = almacen
    kardex.fecha = datetime.datetime.now()
    kardex.detalle = tipo 
    kardex.producto = producto 
    kardex.valoru = preciounitario
    kardex.cantidad = cantidad
    kardex.total = preciounitario * cantidad
    if tipo == "COMPRAS":
        kardex.factura_compra = doc_name    
    if tipo == "SOLICITUD DE CREDITO":
        kardex.solicitud = doc_name
        kardex.cantidad = cantidad * -1
                 
    if tipo == "Inventario Incial":
        kardex.inventarioinicial = doc_name 
    if tipo == "VENTA":
        kardex.factura_venta = doc_name 
        kardex.cantidad = cantidad * -1
    

    kardex.insert()
    kardex.docstatus = 1
    kardex.save()






        