# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from next.utilidades.utils import *
class Proveedores(Document):
	def before_insert(self):
    		setEmisor(self)
