frappe.listview_settings['comprobante_retencion'] = {
    hide_name_column: true,
    add_fields: ["codigo"],
    // colwidths: {"name": 6},
    get_indicator: function (doc) {
        var codigo= doc.codigo;
        var len= codigo.length;
        var estab = codigo.substring(len-17,len-14);
        console.log(estab)
	    var ptoemi=codigo.substring(len-13,len-10);
	    var secuencial=doc.codigo.substring(len-9,len-1);
        console.log(ptoemi)
        return [estab+ptoemi+secuencial, "blue"];
    },
    onload: function (listview) {
        let lstColumns = $(".list-row-col");
        for (let cValue of lstColumns) {
            if (cValue.innerText == 'Estado') {
                cValue.innerText = '# Documento';
                break;
            }
        }
        setTimeout(() => {
            $(".msg-box.no-border").find("p").first().text("No existen registros")
        }, 250);
    }
}