// Copyright (c) 2022, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('comprobante_retencion', {
	refresh:function(frm){
		
		if (frm.is_new() != 1) {
            if (frm.doc.claveacceso != undefined) {
                frm.add_custom_button(__("Ver RIDE"), function () {
                    var format = 'RIDE_RETENCION'
                    
                    var objWindowOpenResult = window.open(
                        frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
                            + "doctype=" + encodeURIComponent("comprobante_retencion")
                            + "&name=" + encodeURIComponent(frm.doc.name)
                            + "&format=" + encodeURIComponent(format)
                            + "&no_letterhead=0"));
                });
            }
        } 

	 
		

		
		frm.set_query('numdocsustento' , function () {
			return {
			  filters: [
				['proveedor', '=', cur_frm.doc.proveedores],
				['comprobante_retencion', '=', undefined],
			  ]
			}
		  })

	},
	validate: function(frm) {
		if ((frm.doc.numdocsustento).length != 15){
			frappe.throw("Campo Factura a modificar incorrecto, longitud de 15 caracteres")
		}
		var detalle=frm.doc.comprobante_retencion_detalle
		if(detalle.length <=2){
			if (detalle.length ==2)
				if(detalle[0].tipo ==detalle[1].tipo)
					frappe.throw("Detalle de impuestos incorecto, el tipo de impuesto se encuentra repetido")
		}else{
			frappe.throw("Detalle de impuestos incorecto, máximo 2 registros")
		}
	},
	proveedores:function(frm){
		if (frm.doc.proveedores != null && frm.doc.proveedores != ''){
			frappe.call({
				method: "frappe.client.get",
				args: {
					doctype: "Proveedores",
					name: frm.doc.proveedores,
				},
				callback(r) {
					if(r.message) {
						var proveedor = r.message;
						if (proveedor.correo != undefined && proveedor.correo !=""){
							updateGridData(proveedor.correo);
						}else{
							console.log("Proveedor no tiene Email")
						}
					}
				}
			});
		}
		
	}
});


frappe.ui.form.on('comprobante_retencion_detalle', {
	retencion_iva: function (frm, cdt, cdn) {

		const doc = frappe.get_doc(cdt, cdn);


		if (doc !== undefined && doc !== null) {


			let sretencion_iva = doc.retencion_iva.replace("%", "");
			let retencion_iva = parseFloat(sretencion_iva);
			frappe.model.set_value(cdt, cdn, "porcentajeretener", retencion_iva);

			frappe.db.get_doc('retencion_iva', doc.retencion_iva).then((doc) => {

				frappe.model.set_value(cdt, cdn, "codigoretencion", doc.ri_codigo);

			}).catch((err) => {
				console.log(err); // eslint-disable-line
			});
		}

	},
	retencion_renta: function (frm, cdt, cdn) {
		const doc = frappe.get_doc(cdt, cdn);
		if (doc !== undefined && doc !== null) {

			frappe.db.get_doc('retencion_renta', doc.retencion_renta).then((doc) => {


				if (isNumber(doc.porcentaje))
					frappe.model.set_value(cdt, cdn, "porcentajeretener", doc.porcentaje);
				else
					frappe.model.set_value(cdt, cdn, "porcentajeretener", 0);

				frappe.model.set_value(cdt, cdn, "codigoretencion", doc.codigoanexo);

			}).catch((err) => {
				console.log(err); // eslint-disable-line
			});
		}

	},
	porcentajeretener: function (frm, cdt, cdn) {
		calcularbase(frm, cdt, cdn);
	},
	baseimponible: function (frm, cdt, cdn) {
		calcularbase(frm, cdt, cdn);
	},
	valorretenido:function(frm, cdt, cdn){
		calculatotal(frm, cdt, cdn)
	},
	comprobante_retencion_detalle_remove:function (frm, cdt, cdn) {
		calculatotal(frm, cdt, cdn)
	}
});
function calcularbase(frm, cdt, cdn) {
	const doc = frappe.get_doc(cdt, cdn);
	var valorretenido = (doc.baseimponible * doc.porcentajeretener) / 100;
	valorretenido =Math.round((valorretenido + Number.EPSILON) * 100) / 100;
	frappe.model.set_value(cdt, cdn, "valorretenido", valorretenido);
	calculatotal(frm, cdt, cdn)
}

function isNumber(value) {
	try {
		parseFloat(value);

		return true;
	} catch (err) {
		console.log(err);
		return false;
	}
}

function calculatotal(frm, cdt, cdn){
	var d = locals[cdt][cdn];
	var total = 0;
	cur_frm.doc.comprobante_retencion_detalle.forEach(function(d) { total += d.valorretenido; });
	cur_frm.doc.total=total;
	cur_frm.refresh_fields("total");
}

function updateGridData(email) {
	cur_frm.clear_table("informacion_adicional");
	var new_email = cur_frm.add_child("informacion_adicional");
	frappe.model.set_value(new_email.doctype, new_email.name, "nombre", "Email");
	frappe.model.set_value(new_email.doctype, new_email.name, "valor", email);
	cur_frm.refresh_field("informacion_adicional");
}
