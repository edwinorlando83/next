# Copyright (c) 2022, orlando and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document 
import next.next.ecutils as ec
from next.utilidades.utils import *
import random

class comprobante_retencion(Document):

	def before_insert (doc):
		if not doc.ptoemi  :
				doc.ptoemi = getPuntoEmision(doc.numdocsustento)  
			 
		setEmisor(doc)

	def on_update(self):
		if self.docstatus==1:                
			clavedeacceso =  generarnumfac (self)
			ec.genenarXmlRetencion(clavedeacceso,self)  

 
		
def getPuntoEmision( numdocsustento):
        puntoemi = "001"
        if numdocsustento:
            puntoemi =  frappe.db.sql(""" SELECT estab  FROM `tabFactura de Compras` where name=%s""",numdocsustento)[0][0]
        return puntoemi



def generarnumfac(self):
        random.seed()
        obj_ambiente = frappe.db.sql("""select tipo_ambiente from tabEmisor """)
        if not obj_ambiente:
            frappe.throw("EL Ambiente no se ha establecido en el Emisor")

        sri_ambiente = obj_ambiente[0][0]

        obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)

        if not obj_ruc:
            frappe.throw("EL ruc no se ha establecido en el Emisor")
        sri_ruc = obj_ruc[0][0]

        if len(sri_ruc ) < 13 :
            frappe.throw("El ruc para la firma electrónica no tiene 13 dígitos, Por favor diríjase al módulo de facturación,en la sección SRI y emisor para configurarlo correctamente")


        obj_estab = frappe.db.sql("""SELECT establecimiento FROM  tabAlmacenes where name = %s """,self.estab)

        if not obj_estab:
            frappe.throw("EL establecimiento no se ha establecido en el Almacen")
        sri_establecimiento = obj_estab[0][0]


        if str( sri_ambiente ).strip() == 'Pruebas':
            sri_ambiente = "1"
        else:
            sri_ambiente = "2"

        tipoComprobante='01' 
  
        numerofacr = getSecuencial_RET(sri_ambiente,sri_ruc)  
    

        #doc.secuencial= numerofac
        numerofac = str(numerofacr).zfill(9)
      
        numfac_completo =  sri_establecimiento + self.ptoemi +'-'+numerofac
        serie =  sri_establecimiento + self.ptoemi
      
        #doc.num_fac = numfac_completo
        
         
        #doc.sri_ambiente = sri_ambiente


        claveacceso = ec.generarClaveAcesso( self.fechaemision,tipoComprobante,sri_ruc,sri_ambiente,serie,numerofac)
        #doc.barcode_ca = ec.get_svg_from_text_code128(claveacceso)
        #doc.clavedeacceso=claveacceso

        sqlu = """ update tabcomprobante_retencion 
		set  claveacceso = '{0}' ,
		barcode_ca='{1}' ,
		sri_ambiente='{2}',
		sri_establecimiento='{3}' , 
		secuencial ={4} , num_fac='{5}' , estadosri='GENERADO'
				where name = '{6}' """.format(claveacceso, ec.get_svg_from_text_code128(claveacceso), 
        sri_ambiente,sri_establecimiento,numerofac,numfac_completo,self.name     )    

        frappe.db.sql(sqlu)
        #logger.info(f"{user} accessed counter_app.update with value={value}")
        actualizar_secuencia_fac(sri_ruc,sri_ambiente)
        return  claveacceso

def actualizar_secuencia_fac(ruc,ambiente):
    emisor = frappe.get_doc("Emisor", ruc)
    if ambiente == '1':
        emisor.iniret_pruebas = emisor.iniret_pruebas + 1
    else:
        emisor.iniret = emisor.iniret+ 1
    emisor.save()


def getSecuencial_RET(ambiente,ruc):
    emisor = frappe.get_doc("Emisor", ruc)
    sec  = 0
    if ambiente == '1':
        sec = emisor.iniret_pruebas
    else:
        sec = emisor.iniret
    if sec== 0:
        sec = 1
    return sec 
 