# -*- coding: utf-8 -*-
# Copyright (c) 2020, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
import frappe
from next.utilidades.utils import *

class FacturaComprasGastos(Document):
    def before_insert(self):
        setEmisor(self)

        
@frappe.whitelist()     
def get_cargos( incargo):
        if incargo :
                cargos = frappe.db.sql(""" SELECT  tarifa,c.nombre,c.name FROM  tabCargos c inner join  `tabImpuestos Venta` i on ( c.impuesto = i.name ) where activo=1  and c.name =%s""",incargo)
                arr = [cargos[0][0],cargos[0][1]]
                return arr
