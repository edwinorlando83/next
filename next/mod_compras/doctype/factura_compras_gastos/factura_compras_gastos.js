// Copyright (c) 2020, orlando and contributors
// For license information, please see license.txt
var frmCompras;
frappe.ui.form.on("Factura Compras Gastos", {
  refresh: function(frm) {
    frmCompras = frm;
  },
  num_fac: function(frm) {
	validadNumerofactura(frm.doc.num_fac);
  
  },

  cliente: function(frm) {
   
  },
  validate: function(frm) {
	validadNumerofactura(frm.doc.num_fac);
 
  }
});
frappe.ui.form.on("cargos_compras", {
  cantidad: function(frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn);
    calculaPrecioTotalxLinea(doc, cdt, cdn);
  },
  descuento: function(frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn);
    calculaPrecioTotalxLinea(doc, cdt, cdn);
  },
  preciounitario: function(frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn);
    calculaPrecioTotalxLinea(doc, cdt, cdn);
  },

  cargos: function(frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn);

    getPrecioEiva(frm, doc.cargos, cdt, cdn);
  }
});

function calculaPrecioTotalxLinea(doc, cdt, cdn) {
  var cantidad = doc.cantidad;
  var preciounitario = doc.preciounitario;
  var descuento = doc.descuento;
  var tarifa_iva = doc.tarifa_iva;
 
  var iva_valor = ((preciounitario - descuento) * cantidad * tarifa_iva) / 100;
  iva_valor =  roundNumber(iva_valor,2);
  var preciototalsinimpuesto = (preciounitario - descuento) * cantidad;
  var totallinea = preciototalsinimpuesto + iva_valor;

  frappe.model.set_value(cdt, cdn, "totallinea", totallinea);
  frappe.model.set_value(cdt, cdn, "iva_valor", iva_valor);
  frappe.model.set_value(
    cdt,
    cdn,
    "preciototalsinimpuesto",
    preciototalsinimpuesto
  );

  var totalDescuentos = 0;
  var totalSinImpuestos = 0;
  var ImporteTotal = 0;
  var iva_total = 0;
  $.each(frmCompras.doc["detalles_faccompras"], function(i, row) {
    totalDescuentos += row.descuento;
    totalSinImpuestos += row.preciototalsinimpuesto;
    ImporteTotal += row.totallinea;
    iva_total += iva_valor;



  });

  frmCompras.doc.totaldescuento = totalDescuentos;
  frmCompras.doc.totalsinimpuestos = totalSinImpuestos;
  frmCompras.doc.iva_total = iva_total;
  frmCompras.doc.importetotal =  ImporteTotal ;
  frmCompras.doc.saldo =    ImporteTotal ;
  frmCompras.refresh_fields();
}

function getPrecioEiva(frm, inproducto, cdt, cdn) {
 if( inproducto !== undefined) { 
  frappe.call({
    method:
      "next.next.doctype.factura_compras_gastos.factura_compras_gastos.get_cargos",
    args: {
      incargo: inproducto
    },
    callback: r => {
    
      console.log(r.message);
      frappe.model.set_value(cdt, cdn, "tarifa_iva", r.message[0]);
    }
  });
}
}
function validadNumerofactura(num_fac){
	var res = validarNumeroFactura(num_fac);
    if (res != "OK") frappe.throw(res);
}
