# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime
from next.utilidades.utils import *
from zeep import Client
import xml.etree.ElementTree as ET
class FacturadeCompras(Document):
   
    def before_insert(self):
        setEmisor(self)
        #sql = """ select  ifnull(max(sec_interno),0) + 1 from  `tabFactura de Compras`  where emisor ='{0}' """.format(self.emisor)
        #intcodigo_pk = str(frappe.db.sql(sql)[0][0])
        #self.sec_interno = int(intcodigo_pk)
        #self.codigo_pk = 'COMP-'+ intcodigo_pk.zfill(6)

    def on_update(self): 
            RegistarIngresoProductos(self)
            if self.pagoefectivo == 1:
                if self.docstatus== 1:
                    registarPago(self)
                    self.saldo=0.0

     

 
@frappe.whitelist()
def getPrecioEiva( inproducto  ):    

        prod = frappe.get_doc("Productos",inproducto)
        
        precio_sin_iva =  0.0
               
        cantidad = 0  
        
        impven = frappe.get_doc("Impuestos Venta",prod.impuesto)
        ivaporcentaje = impven.tarifa 

        arr = [ precio_sin_iva , ivaporcentaje, cantidad  ,impven.sri_iva  ]
        return arr

def RegistarIngresoProductos(doc):
    if doc.docstatus==1 and doc.incrementarstock == 1:
        for pro in doc.detalles_faccompras:
            insert_kardex( "COMPRAS", doc.estab,pro.producto,pro.preciounitario, pro.cantidad,doc.name )

def registarPago(doc):
        rc = frappe.new_doc("Registro de Pagos")
        rc.fecha = datetime.datetime.now()
        rc.descripcion ="Pago en EFECTIVO de la factura: " + doc.num_fac
        rc.proveedores = doc.proveedor
        rc.docstatus = 1
       
        rc.append("detalle_pagos", {"tipo":"EFECTIVO" ,"cantidad":doc.importetotal  })       
        rc.append("detallepago_fac_compras", {"factura":doc.name ,"valor_apagar":doc.importetotal  })

        #frappe.throw(str(doc.importetotal) +str(doc.name) )
        rc.insert()
        doc.registrocobro = rc.name


#bench --site nextweb execute next.mod_compras.doctype.factura_de_compras.factura_de_compras.getDatosFactura
def getDatosFactura():
    client = Client('https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl')
    result = client.service.autorizacionComprobante(claveAccesoComprobante='1708202301010362888900120060040000942111234567810' )
    autorizacion = result.autorizaciones.autorizacion[0]
    #print(str( autorizacion.estado))
    #print(str( autorizacion.numeroAutorizacion ))
    #print(str( autorizacion.fechaAutorizacion ))
    #print(str( autorizacion.ambiente ))
    #print(str( autorizacion.comprobante))
 
    root = ET.fromstring(autorizacion.comprobante)
    ambiente = root.find('infoTributaria/ambiente').text
    tipoEmision = root.find('infoTributaria/tipoEmision').text
    razonSocial = root.find('infoTributaria/razonSocial').text

    # Print the values
    print("Ambiente:", ambiente)
    print("Tipo de Emision:", tipoEmision)
    print("Razon Social:", razonSocial)
    

