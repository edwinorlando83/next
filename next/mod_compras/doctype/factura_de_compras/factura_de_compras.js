// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt
var frmCompras;
var iva_procentaje = 12;
frappe.ui.form.on("Factura de Compras", {
  refresh: function (frm) {
    frmCompras = frm;

    frm.set_query("factura_aplica", function () {
      return {

        filters: [
          ['tipo_comprobante', '=', '01 - Factura'],
          ['proveedor', '=', cur_frm.doc.proveedor],

        ]
      };
    });


    if (!frm.is_new()) {


   
      cur_frm.add_custom_button(__('Importar Factura Electrónica'), function () {
   
      }).css({ 'background-color': '#FCC60B', 'font-weight': 'bold', 'color': 'black' });
  
      
    }



  },
  num_fac: function (frm) {
    // validadNumerofactura(frm.doc.num_fac);
  },
  validate: function (frm) {
    // validadNumerofactura(frm.doc.num_fac);
  },

  cliente: function (frm) {
    //	getDatosCliente(frm);
  },
  totaldescuento: function (frm) {
    frm.doc.iva_total = ( frm.doc.totalsinimpuestos - frm.doc.totaldescuento) * 0.12 ;
    frm.doc.importetotal =  ( frm.doc.totalsinimpuestos - frm.doc.totaldescuento) +  frm.doc.iva_total + frm.doc.noobjetoiva +  frm.doc.subtotal0 ;
    frm.refresh_field('iva_total');
  }
  ,
  importetotal: function (frm) {
    frm.doc.saldo = frm.doc.importetotal;
    frm.refresh_field('saldo');
  }
});
frappe.ui.form.on("detalles_faccompras", {
  cantidad: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },
  preciounitario: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  desporcentaje: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  producto: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      getPrecioEiva(frm, doc, cdt, cdn)
      // stock_por_producto(frm, doc, cdt, cdn)
    }
  },
  detalles_facventas_remove: (frm) => {

    calcular_valores()
  },
  tipoiva: (frm) => {

    calcular_valores()
  },
});


function calcular_valores() {
  var totalDescuentos = 0;
  var subtotal_iva12 = 0;
  var subtotal_iva0 = 0;
  var subtotal_nosujiva = 0;

  var iva_total = 0;

  $.each(cur_frm.doc.detalles_faccompras, function (i, row) {
    // calcula descuento
    let valordescuento = 0;

    if (row.desporcentaje > 0)
      valordescuento = (row.preciounitario * row.desporcentaje) / 100;

    row.descuento = valordescuento;

    let precio_calc = roundNumber((row.preciounitario - valordescuento) * row.cantidad, 2);
    //IVA 12%
    //IVA 0%
    //NO OBJETO DE IVA
    if (row.tipoiva == "NO OBJETO DE IVA") //No Objeto de Impuesto
    {
      subtotal_nosujiva += precio_calc;
      row.iva_valor = 0;
      row.sri_iva = "6"
    }
   
  if (row.tipoiva == "IVA 0%") //IVA 0%
      {
        subtotal_iva0 += precio_calc;
        row.iva_valor = 0;
        row.sri_iva = "0"
      }

  if (row.tipoiva == "IVA 12%") 
      {
        row.tarifa_iva = 12;
        row.sri_iva = "2"
        let iva_valor = (precio_calc * row.tarifa_iva) / 100;
        row.iva_valor = roundNumber(iva_valor, 2);
        subtotal_iva12 += precio_calc;

      }
  
  if (row.tipoiva == "IVA 15%") 
      {
        row.tarifa_iva = 15;
        row.sri_iva = "4"
        let iva_valor = (precio_calc * row.tarifa_iva) / 100;
        row.iva_valor = roundNumber(iva_valor, 2);
        subtotal_iva12 += precio_calc;

      }

      console.log(precio_calc);
    row.preciototalsinimpuesto = precio_calc;
    row.totallinea = roundNumber(precio_calc + row.iva_valor, 2);

    totalDescuentos += row.descuento;
    iva_total += row.iva_valor;
  })


  cur_frm.doc.totaldescuento = totalDescuentos;
  cur_frm.doc.totalsinimpuestos = subtotal_iva12;
  cur_frm.doc.subtotal0 = subtotal_iva0;
  cur_frm.doc.noobjetoiva = subtotal_nosujiva;
  cur_frm.doc.iva_total = iva_total;
  cur_frm.doc.importetotal = roundNumber(subtotal_iva12 + subtotal_iva0 + subtotal_nosujiva + iva_total, 2);

  cur_frm.doc.saldo = cur_frm.doc.importetotal;

  cur_frm.refresh_fields();
}

function getPrecioEiva(frm, doc, cdt, cdn) {

  if (doc.producto !== undefined && doc.producto !== null) {
    frappe.call({
      method: 'next.mod_compras.doctype.factura_de_compras.factura_de_compras.getPrecioEiva',
      freeze: true,
      args: {
        inproducto: doc.producto
      },
      callback: r => {
        console.log(r.message)
        frappe.model.set_value(cdt, cdn, 'preciounitario', r.message[0])
        // frappe.model.set_value(cdt, cdn, 'tarifa_iva', r.message[1])
        // frappe.model.set_value(cdt, cdn, 'sri_iva', r.message[3])
        cur_frm.refresh_fields()
        calcular_valores()
      }
    })
  }
}

function validadNumerofactura(num_fac) {
  var res = validarNumeroFactura(num_fac);
  if (res != "OK") frappe.throw(res);
}
