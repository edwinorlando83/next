import frappe
import datetime
import json

@frappe.whitelist()
def get_saldo_otros_egresos(cliente):
    sql = """ select d.solicitud,s.fechaemision  ,d.name, d.cuota_numero , d.fecha,d.cuota_valor ,d.saldo ,
 case when (d.fecha <= CURRENT_DATE() ) then 'SI' else 'NO' end atrasado
 ,s.numero_solicitud ,
 TIMESTAMPDIFF(DAY, d.fecha , CURRENT_DATE() ) AS dias_atrazo 
 from tabcliente_deudas  d
 left join tabSolicitud  s on ( s.name =  d.solicitud )
    where d.saldo > 0  and  d.cliente ='{0}'  
    order by  d.fecha  """.format(cliente)
    return frappe.db.sql(sql,as_dict=True)

@frappe.whitelist()
def getSaldoCLiente(cliente):
    sql1 =""" select   IFNULL(  sum(saldo),0)   from tabcliente_deudas  where saldo > 0 and cliente ='{0}' """.format(cliente)
    sql2 = """ select IFNULL(  sum(saldo),0)  from `tabFactura en Ventas`   where saldo > 0 and cliente ='{0}' """.format(cliente)
    res1 = frappe.db.sql(sql1)
    res2 = frappe.db.sql(sql2)
    objcliente = frappe.get_doc("Clientes",cliente)
    saldo =0     
    saldo += float(res1[0][0])  
    saldo += float(res2[0][0])
    return { "cliente":objcliente, "saldo":saldo, "saldo_facturas":res2[0][0], "saldo_cuotas":res1[0][0]}


@frappe.whitelist()
def abonar(cliente,valores,num_recivo):        
        txt=''
        if num_recivo:
            txt = " #recivo: {0}".format(num_recivo)

        rc = frappe.new_doc("Registro de Cobros")
        rc.tipo = "CUOTAS"
        rc.numero_recivo = num_recivo
        rc.fecha = datetime.datetime.now()
        rc.descripcion ="PAGO DE CUOTAS "  + txt 
        rc.clientes = cliente        
        total = 0
        jsonvalores = json.loads(valores)
        for rw in jsonvalores:           
            rc.append("detalle_cuotas_cobros", {"cliente_deudas":rw["name"] ,"valor_pagado":float(rw["valor"] ) })
            total += float(rw["valor"])
            
        rc.append("detalle_pagos", {"tipo":"EFECTIVO" ,"cantidad":float(total)  })

 
     
        rc.insert()
        rc.docstatus = 1
        rc.save()
        return rc.name
        


