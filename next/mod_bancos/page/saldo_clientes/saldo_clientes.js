var me;
var entidad;
let lstCuotas;
let lstCutasapagar;
frappe.pages['saldo_clientes'].on_page_load = function (wrapper) {
	me = this;
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Pago de Cuotas',
		single_column: true
	});

	page.main.html(frappe.render_template("saldo_clientes", {}));

	entidad = frappe.ui.form.make_control({
		parent: page.main.find(".entidad"),
		df: {
			fieldtype: "Link",
			options: "Clientes",
			fieldname: "clientes",
			reqd: 1,
			change: function () {

				if (entidad.get_value()) {
					show_entidad_info(entidad.get_value());
					getsaldo(entidad.get_value());
				}
			}
		},
		only_input: true,
	});
	entidad.refresh();
}
function getsaldo(inentidad) {

	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.getSaldoCLiente",
		args: {
			cliente: inentidad
		},
		freeze: true,
		callback: function (r) {

			if (r.message == undefined) {
				data = ''

			}
			else {

				drawDataSaldo(r.message);
			}

		}
	});
}
function show_entidad_info(inentidad) {
	lstCuotas = [];
	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.get_saldo_otros_egresos",
		args: {
			cliente: inentidad
		},
		freeze: true,
		callback: function (r) {

			if (r.message == undefined) {
				data = ''
				//drawError()
			}
			else {

				lstCuotas = r.message;

				drawData(r.message);
			}

		}
	});
}
function calcularPago() {
	let suma = 0.0;
	lstCutasapagar = [];
	
	lstCuotas.forEach((row) => {
		let val1 = $('#' + row.name).val();
		
		 
		if (val1) {
			valor_ = parseFloat(val1);
			let valsaldo = $('#' + row.name +"_saldo").html();	
			console.log('valsaldo',valsaldo);
			valsaldo= valsaldo.replace("$","").replace(",","").trim() ;	 
			console.log('valsaldo 2:',valsaldo);
			let realsaldo = parseFloat(valsaldo);
			console.log('realsaldo',realsaldo);
			console.log('valor_',valor_);

			if(valor_ >realsaldo  ){
				$('#' + row.name).val(realsaldo)
				frappe.msgprint("La cantidad máxima que puede abonar a la couta es:" + realsaldo);
				valor_ = realsaldo;
			}

			if (valor_ != 0) {
				suma += valor_;
				let fila = { "name": row.name, "valor": valor_  , "saldo": realsaldo }
				lstCutasapagar.push(fila);
			}
		}

	});
	let smg = formatMoney(suma, ".", ",")
	$('#lbl_apagar').html(smg)

}
function drawDataSaldo(datos) {

	let html = `<div class="alert alert-warning">
	  Cliente: <b> ${datos.cliente.nombres} </b>  CI/RUC: <b> ${datos.cliente.ruc} </b>
	 Saldo total:$ ${formatMoney(datos.saldo, ".", ",")} 
	
	</div> `;
	me.page.main.find(".divResultadosaldo").html(html);

}
function drawData(datos) {
	let html = `<table id='tbl_cuotas' class="GeneratedTable">
		<thead>
		  <tr>
		    
			<th style="width:160px" > Ver Documento </th>
			<th style="width:160px" > # Solicitud </th>
			<th style="width:160px" > Fecha Emsión </th>
			<th style="width:70px">#Cuota</th>
			<th style="width:100px" >Fecha</th>
			<th  style="width:120px" >Valor</th>
			<th style="width:120px" >A Pagar</th>	
			<th style="width:70px" >Atrasado</th>	
			<th style="width:70px" >Dias Atrazo</th>	
			<th style="width:50px" > Abonar</th>	
			<th style="width:40px" >  </th>		 
		  </tr>
		</thead>
		<tbody>
		  
	 
	   `;
	let mostar = false;
	let sumac = 0;
	let sumaatrasado = 0;

	datos.forEach((row) => {
		mostar = true;
		sumac += row.saldo;
		var color = '';
		if (row.atrasado == 'SI') {
			color = 'style="background-color:#F58B8B" ';
			sumaatrasado += row.saldo;
		}
		let nsolicitud="";
		if( row.numero_solicitud != 'null' && row.numero_solicitud != undefined)
		{
			nsolicitud = row.numero_solicitud;
		}
		html += `<tr ${color}  >

		
		
		<td> <div  onclick="iradoc('${row.solicitud}')"  style="cursor:pointer"> ${row.solicitud} </div>  </td>
		<td style="text-align: center" >${nsolicitud} </td>	
		<td style="text-align: center" >${row.fechaemision} </td>	
		<td style="text-align: center" >${row.cuota_numero} </td>
			<td>${row.fecha} </td>
			<td style="text-align:right;" ><font style="color:#616A6B"> $ ${formatMoney(row.cuota_valor, ".", ",")} </font> </td>
			<td style="text-align:right; color:#FAFCFF;" ><b style="font-size:18px; color:#616A6B" id="${row.name}_saldo" >$ ${formatMoney(row.saldo)} </b></td>
			<td style="text-align: center; "  >${row.atrasado} </td>	
			<td style="text-align: center" >${row.dias_atrazo} </td>		
			<td> <input onchange="calcularPago()" style="width:100px" type="number" id="${row.name}" name="${row.name}"   placeholder="0.00"
			 > </td>	
			 <td> <img onclick="copiarvalor('${row.name}', '${row.saldo}')"  style="cursor:pointer"  src="/assets/next/copy_icon.png" /> </td> 
		  </tr> `;

	});
	html += `	<tfoot>  <tr>
	<td colspan="5"  style="text-align:right;"> <p>Total:</p>
	<p>Total atrasado:</p>
	</td>
	<td  style="text-align:left;" > <p> <b>$ ${formatMoney(sumac)} </b> </p> 
	<p> <b> $ ${formatMoney(sumaatrasado)} </b> </p>
	</td>
  </tr>
  </tfoot></tbody>
	</table> `;

	if (mostar)
		me.page.main.find(".divResultado").html(html);
	else {
		if(datos.saldo==0){
		
		me.page.main.find(".divResultado").html('<div class="alert alert-warning">No hay datos</div>');
	}
		else{
			me.page.main.find(".divResultado").html(`<div class="alert alert-warning"> 
			Para registrar los pagos de CXC Iniciales, se debe ir a  <a href="/app/registro-de-cobros/new-registro-de-cobros-1" class="btn btn-sm btn-light"> Registros de cobros  </a> </div>
			`);
		}
	}
}
function iradoc(doc) {
	window.open('solicitud/' + doc, '_blank');
}
function copiarvalor(control, valor) {
	$('#' + control).val(parseFloat(valor));
	calcularPago();
}
function abonar() {
	if (entidad.get_value().trim().length == 0) {
		frappe.msgprint("Seleccione un cliente");
		return;
	}

	if ( $('#txtPagar').val( ) <= 0) {
		frappe.msgprint("El valor de pago debe ser mayor a cero");
		return;
	}

	

	let d = new frappe.ui.Dialog({
		title: 'Ingrese los datos',
		fields: [
			{
				label: 'VALOR EFECTIVO',
				fieldname: 'valor',
				fieldtype: 'Currency',
				reqd: 1
			},
			{
				label: '# Recivo',
				fieldname: 'num_recivo',
				fieldtype: 'Data',
				default: '',
				reqd: 1
			}
		],
		primary_action_label: 'Aceptar',
		primary_action(values) {

			if (values.valor <= 0) {
				frappe.msgprint("El valor debe ser mayor a cero");
				return;
			}
			abonarsw(values);

			d.hide();
		}
	});

	d.show();


}

function abonar_2() {
	if (entidad.get_value().trim().length == 0) {
		frappe.msgprint("Seleccione un cliente");
		return;
	}

	if ( $('#txtPagar').val( ) <= 0) {
		frappe.msgprint("El valor de pago debe ser mayor a cero");
		return;
	}

 
	let msg = ` <b> A pagar </b> `;
	lstCutasapagar.forEach((row) => {

		msg += '<div>' + row.valor + '</div>';
	});

	console.log(lstCutasapagar);
	let d = new frappe.ui.Dialog({
		title: 'Datos del Pago',
		fields: [
			{
				fieldname: 'mensaje',
				fieldtype: 'HTML'

			},
			{
				label: '# Recivo',
				fieldname: 'num_recivo',
				fieldtype: 'Data',
				default: '',
				reqd: 1
			}
		],
		primary_action_label: 'Aceptar',
		primary_action(values) {

			if (values.valor <= 0) {
				frappe.msgprint("El valor debe ser mayor a cero");
				return;
			}
			abonarsw(values,lstCutasapagar);

			d.hide();
		}
	});
	d.fields_dict.mensaje.$wrapper.append(msg);
	d.show();

}
function abonarsw(values,lstCutasapagar) {


	let nrecivo = '';
	if (values.num_recivo != undefined) {
		nrecivo = values.num_recivo;
	}
 
 
	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.abonar",
		args: {
			cliente: entidad.get_value(),
			valores: lstCutasapagar,
			num_recivo: nrecivo
		},
		freeze: true,
		callback: function (r) {
			show_entidad_info(entidad.get_value());
			getsaldo(entidad.get_value());


			var w1 = window.open(
				frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?" +
					"doctype=" + encodeURIComponent('Registro de Cobros') +
					"&name=" + encodeURIComponent(r.message) +
					"&format=" + encodeURIComponent('f_recivo') +
					"&no_letterhead=0"));

			if (!w1) {
				frappe.msgprint(__("Please enable pop-ups"));
				return;
			}

		}
	});

}
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
	try {
		decimalCount = Math.abs(decimalCount);
		decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

		const negativeSign = amount < 0 ? "-" : "";

		let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
		let j = (i.length > 3) ? i.length % 3 : 0;

		return negativeSign +
			(j ? i.substr(0, j) + thousands : '') +
			i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
			(decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
		console.log(e)
	}
};

function calcularAsigancionCuotas(){

	let valor_asig = $('#txtPagar').val( );
	lstCutasapagar = []
	lstCuotas.forEach((row) => {
		  $('#' + row.name).val(); 
	 
			let valsaldo = $('#' + row.name +"_saldo").html();	
			valsaldo= valsaldo.replace("$","").trim() ;	 
			let realsaldo = parseFloat(valsaldo);
			let fila ={};
			if ( valor_asig> 0)
			{
						if(valor_asig > realsaldo){
							$('#' + row.name).val(realsaldo);
							fila = { "name": row.name, "valor": realsaldo  , "saldo": realsaldo };
							lstCutasapagar.push(fila);
							valor_asig = valor_asig - realsaldo;
							
						}
						else{
							$('#' + row.name).val(valor_asig);
						
							fila = { "name": row.name, "valor": valor_asig  , "saldo": realsaldo };
							lstCutasapagar.push(fila);
							valor_asig = 0; 
						} 
		}
			 
		 

	});
/*let smg = formatMoney(suma, ".", ",")
	$('#lbl_apagar').html(smg)*/
}