var me;
var entidad;
let lstCuotas;
let lstCutasapagar;
frappe.pages['saldo_clientes2'].on_page_load = function (wrapper) {
	me = this;
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Saldo Clientes',
		single_column: true
	});

	page.main.html(frappe.render_template("saldo_clientes", {}));

	entidad = frappe.ui.form.make_control({
		parent: page.main.find(".entidad"),
		df: {
			fieldtype: "Link",
			options: "Clientes",
			fieldname: "clientes",
			reqd: 1,
			change: function () {

				if (entidad.get_value()) {
					show_entidad_info(entidad.get_value());
					getsaldo(entidad.get_value());
				}
			}
		},
		only_input: true,
	});
	entidad.refresh();
}
function getsaldo(inentidad) {

	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.getSaldoCLiente",
		args: {
			cliente: inentidad
		},
		freeze: true,
		callback: function (r) {

			if (r.message == undefined) {
				data = ''

			}
			else {

				drawDataSaldo(r.message);
			}

		}
	});
}
function show_entidad_info(inentidad) {
	lstCuotas = [];
	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.get_saldo_otros_egresos",
		args: {
			cliente: inentidad
		},
		freeze: true,
		callback: function (r) {

			if (r.message == undefined) {
				data = ''
				//drawError()
			}
			else {

				lstCuotas = r.message;

				drawData(r.message);
			}

		}
	});
}
function calcularPago() {
	let suma = 0.0;
	lstCutasapagar = [];
	lstCuotas.forEach((row) => {
		let val1 = $('#' + row.name).val();
		if (val1) {
			valor_ = parseFloat(val1);
			if (valor_ != 0) {
				suma += valor_;
				let fila = { "name": row.name, "valor": valor_ }
				lstCutasapagar.push(fila);
			}
		}

	});
	let smg = formatMoney(suma, ".", ",")
	$('#lbl_apagar').html(smg)

}
function drawDataSaldo(datos) {

	let html = `<div class="alert alert-warning">
	  Cliente: <b> ${datos.cliente.nombres} </b>  CI/RUC: <b> ${datos.cliente.ruc} </b>
	 Saldo total:$ ${formatMoney(datos.saldo, ".", ",")} 
	
	</div> `;
	me.page.main.find(".divResultadosaldo").html(html);

}
function drawData(datos) {
	let html = `<table id='tbl_cuotas' class="GeneratedTable">
		<thead>
		  <tr>
		    
			<th style="width:160px" > Cod. Interno </th>
			<th style="width:160px" > # Solicitud </th>
			<th style="width:70px">#Cuota</th>
			<th style="width:100px" >Fecha</th>
			<th  style="width:120px" >Valor</th>
			<th style="width:120px" >Saldo Cuota</th>	
			<th style="width:70px" >Atrasado</th>	
			<th style="width:50px" > Abonar</th>	
			<th style="width:40px" >  </th>		 
		  </tr>
		</thead>
		<tbody>
		  
	 
	   `;
	let mostar = false;
	let sumac = 0;
	let sumaatrasado = 0;

	datos.forEach((row) => {
		mostar = true;
		sumac += row.saldo;
		var color = '';
		if (row.atrasado == 'SI') {
			color = 'style="background-color:#E74C3C" ';
			sumaatrasado += row.saldo;
		}
		html += `<tr ${color}  >
		
		<td> <div  onclick="iradoc('${row.solicitud}')"  style="cursor:pointer"> ${row.solicitud} </div>  </td>
		<td style="text-align: center" >${row.numero_solicitud} </td>	
		<td style="text-align: center" >${row.cuota_numero} </td>
			<td>${row.fecha} </td>
			<td style="text-align:right;" >$ ${formatMoney(row.cuota_valor, ".", ",")} </td>
			<td style="text-align:right;" >$ ${formatMoney(row.saldo)} </td>
			<td style="text-align: center"  >${row.atrasado} </td>		
			<td> <input onchange="calcularPago()" style="width:100px" type="number" id="${row.name}" name="${row.name}"   placeholder="0.00"
			 > </td>	
			 <td> <img onclick="copiarvalor('${row.name}', '${row.saldo}')"  style="cursor:pointer"  src="/assets/next/copy_icon.png" /> </td> 
		  </tr> `;

	});
	html += `	<tfoot>  <tr>
	<td colspan="3"  style="text-align:right;"> <p>Total:</p>
	<p>Total atrasado:</p>
	</td>
	<td  style="text-align:left;" > <p> <b>$ ${formatMoney(sumac)} </b> </p> 
	<p> <b> $ ${formatMoney(sumaatrasado)} </b> </p>
	</td>
  </tr>
  </tfoot></tbody>
	</table> `;

	if (mostar)
		me.page.main.find(".divResultado").html(html);
	else {
		me.page.main.find(".divResultado").html('<div class="alert alert-warning">No hay datos</div>');
	}
}
function iradoc(doc) {
	window.open('solicitud/' + doc, '_blank');
}
function copiarvalor(control, valor) {
	$('#' + control).val(parseFloat(valor));
	calcularPago();
}
function abonar() {
	if (entidad.get_value().trim().length == 0) {
		frappe.msgprint("Seleccione un cliente");
		return;
	}


	let d = new frappe.ui.Dialog({
		title: 'Ingrese los datos',
		fields: [
			{
				label: 'VALOR EFECTIVO',
				fieldname: 'valor',
				fieldtype: 'Currency',
				reqd: 1
			},
			{
				label: '# Recivo',
				fieldname: 'num_recivo',
				fieldtype: 'Data',
				default: '',
				reqd: 1
			}
		],
		primary_action_label: 'Aceptar',
		primary_action(values) {

			if (values.valor <= 0) {
				frappe.msgprint("El valor debe ser mayor a cero");
				return;
			}
			abonarsw(values);

			d.hide();
		}
	});

	d.show();


}

function abonar_2() {

	console.log(lstCutasapagar);
	let msg = ` <b> A pagar </b> `;
	lstCutasapagar.forEach((row) => {

		msg += '<div>' + row.valor + '</div>';
	});
	let d = new frappe.ui.Dialog({
		title: 'Datos del Pago',
		fields: [
			{
				fieldname: 'mensaje',
				fieldtype: 'HTML'

			},
			{
				label: '# Recivo',
				fieldname: 'num_recivo',
				fieldtype: 'Data',
				default: '',
				reqd: 1
			}
		],
		primary_action_label: 'Aceptar',
		primary_action(values) {

			if (values.valor <= 0) {
				frappe.msgprint("El valor debe ser mayor a cero");
				return;
			}
			abonarsw(values,lstCutasapagar);

			d.hide();
		}
	});
	d.fields_dict.mensaje.$wrapper.append(msg);
	d.show();

}
function abonarsw(values,lstCutasapagar) {


	let nrecivo = '';
	if (values.num_recivo != undefined) {
		nrecivo = values.num_recivo;
	}
 
	frappe.call({
		"method": "next.mod_bancos.page.saldo_clientes.saldo_clientes.abonar",
		args: {
			cliente: entidad.get_value(),
			valores: lstCutasapagar,
			num_recivo: nrecivo
		},
		freeze: true,
		callback: function (r) {
			show_entidad_info(entidad.get_value());
			getsaldo(entidad.get_value());


			var w1 = window.open(
				frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?" +
					"doctype=" + encodeURIComponent('Registro de Cobros') +
					"&name=" + encodeURIComponent(r.message) +
					"&format=" + encodeURIComponent('f_recivo') +
					"&no_letterhead=0"));

			if (!w1) {
				frappe.msgprint(__("Please enable pop-ups"));
				return;
			}

		}
	});

}
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
	try {
		decimalCount = Math.abs(decimalCount);
		decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

		const negativeSign = amount < 0 ? "-" : "";

		let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
		let j = (i.length > 3) ? i.length % 3 : 0;

		return negativeSign +
			(j ? i.substr(0, j) + thousands : '') +
			i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
			(decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
		console.log(e)
	}
};

