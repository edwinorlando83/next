import frappe

@frappe.whitelist()
def get_cuotas(solicitud):
    sql = """ select d.solicitud,d.name, d.cuota_numero , d.fecha,d.cuota_valor ,
    d.saldo ,  case when (  d.saldo > 0) then 'PENDIENTE' else 'PAGADO' end estado
    ,case when (  d.saldo > 0 and CURRENT_DATE() > d.fecha  ) then   DATEDIFF(  CURRENT_DATE(),d.fecha   ) else 0 end as diasatrazo
    ,s.numero_solicitud 
    from tabcliente_deudas  d
    inner join tabSolicitud  s on ( s.name =  d.solicitud )
    where d.solicitud = '{0}'  """.format(solicitud)
    return frappe.db.sql(sql, as_dict=True)

@frappe.whitelist()
def get_solicitudes(cliente):
    sql = """     select s.name , c.ruc , c.nombres, s.tipo , s.numero_solicitud, s.importetotal , s.entrada , s.extra , s.saldo  from tabSolicitud s
    inner join tabClientes  c on (s.cliente= c.name )
    where c.name = '{0}'  """.format(cliente)
    return frappe.db.sql(sql, as_dict=True)