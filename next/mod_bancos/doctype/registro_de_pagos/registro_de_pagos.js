// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('Registro de Pagos', {
	refresh: function (frm) {

	},
	validate: function (frm) {
		validaciones();
		validarSaldos();
	},
	onload: function (frm) {


		frm.set_query('factura', 'factura_compras_gastos', function ( ) {
			return {
				query: "next.mod_bancos.doctype.registro_de_pagos.registro_de_pagos.getFacturasGastoPendientesdePago",
				filters: { "proveedores": frm.doc.proveedores }
			};
		});

		
		frm.set_query('factura', 'detallepago_fac_compras', function (doc) {
			return {
				query: "next.mod_bancos.doctype.registro_de_pagos.registro_de_pagos.getFacturasPendientesdePago",
				filters: { "proveedores": frm.doc.proveedores }
			};
		});

	}

});

function validaciones() {
	let suma = 0;
	$.each(cur_frm.doc.detalle_pagos, function (i, row) {

		suma += row.cantidad;
	});
	if (suma == 0) {
		frappe.throw("Debe ingresar al menos una cantidad");
	}
}
frappe.ui.form.on('detalle_pagos', {

	refresh: function (frm) {

	}, tipo: function (frm, cdt, cdn) {
		var fm = frm.fields_dict['detalle_pagos'].grid.fields_map;
		var doc = locals[cdt][cdn];
		if (doc.tipo == 'EFECTIVO') {

			fm.banco.reqd = 0;
			fm.cheque.reqd = 0;
			fm.cheque_fecha.reqd = 0;
			fm.banco.read_only = 1;
			fm.cheque.read_only = 1;
			//fm.dep_cuenta.read_only = 1;
		 
			fm.cheque_fecha.read_only = 1;
			fm.ret_numero.reqd = 0;
			fm.ret_iva.reqd = 0;
			fm.ret_renta.reqd = 0;
			fm.ret_fecha.reqd = 0;

			fm.banco.hidden = 1

			fm.ret_numero.read_only = 1;
			fm.ret_iva.read_only = 1;
			fm.ret_renta.read_only = 1;
			fm.ret_fecha.read_only = 1;
			fm.ret_observacion.read_only = 1;
			fm.voucher.reqd = 0;
			fm.voucher.read_only = 1;

		}
		if (doc.tipo == 'CHEQUE') {

			fm.banco.hidden = 0;
			fm.banco.reqd = 1;
			fm.cheque.reqd = 1;
			fm.cheque_fecha.reqd = 1;
			fm.banco.read_only = 0;
			fm.cheque.read_only = 0;
			fm.cheque_fecha.read_only = 0;
			fm.beneficiario.reqd = 1;
		}
		if (doc.tipo == 'TARJETA') {

			fm.voucher.reqd = 1;
			fm.voucher.read_only = 0;

			fm.banco.reqd = 0;
			fm.cheque.reqd = 0;
			fm.cheque_fecha.reqd = 0;
			fm.banco.read_only = 1;
			fm.cheque.read_only = 1;
			//	fm.dep_cuenta.read_only = 1;
		 
			fm.cheque_fecha.read_only = 1;
			fm.ret_numero.reqd = 0;
			fm.ret_iva.reqd = 0;
			fm.ret_renta.reqd = 0;
			fm.ret_fecha.reqd = 0;

			fm.ret_numero.read_only = 1;
			fm.ret_iva.read_only = 1;
			fm.ret_renta.read_only = 1;
			fm.ret_fecha.read_only = 1;
			fm.ret_observacion.read_only = 1;

		}


		if (doc.tipo == 'DEPOSITO BANCARIO' || doc.tipo == 'TRANSFERENCIA BANCARIA') {

			//	fm.dep_cuenta.read_only = 0;
		 
			//	fm.dep_cuenta.reqd = 1;
	 
			fm.banco.hidden = 0;
			fm.voucher.reqd = 0;
			fm.voucher.read_only = 1;
			fm.banco.reqd = 0;
			fm.cheque.reqd = 0;
			fm.cheque_fecha.reqd = 0;
			fm.banco.read_only = 1;
			fm.cheque.read_only = 1;
			fm.cheque_fecha.read_only = 1;
			fm.ret_numero.reqd = 0;
			fm.ret_iva.reqd = 0;
			fm.ret_renta.reqd = 0;
			fm.ret_fecha.reqd = 0;

			fm.ret_numero.read_only = 1;
			fm.ret_iva.read_only = 1;
			fm.ret_renta.read_only = 1;
			fm.ret_fecha.read_only = 1;
			fm.ret_observacion.read_only = 1;

		}

		if (doc.tipo == 'RETENCIONES') {


			fm.ret_numero.reqd = 1;
			fm.ret_iva.reqd = 1;
			fm.ret_renta.reqd = 1;
			fm.ret_fecha.reqd = 1;

			fm.ret_numero.read_only = 0;
			fm.ret_iva.read_only = 0;
			fm.ret_renta.read_only = 0;
			fm.ret_fecha.read_only = 0;
			fm.ret_observacion.read_only = 0;

			fm.voucher.reqd = 0;
			fm.voucher.read_only = 1;
			fm.banco.reqd = 0;
			fm.cheque.reqd = 0;
			fm.cheque_fecha.reqd = 0;
			fm.banco.read_only = 1;
			fm.cheque.read_only = 1;
			//	fm.dep_cuenta.read_only = 1;
		 
			fm.cheque_fecha.read_only = 1;
		}


		frm.refresh_fields();



	}
});

frappe.ui.form.on('detallepago_fac_gastos', {

	refresh: function (frm) {

	}

});

frappe.ui.form.on('detallepago_fac_compras', {

	factura: function (frm, cdt, cdn) {

	}
	,
	valor_apagar: function (frm, cdt, cdn) {
		calcula_total();
	}

});

function calcula_total() {
	let t_total = 0;
	$.each(cur_frm.doc.detallepago_fac_compras, function (i, row) {
		t_total += row.valor_apagar;
	});
	cur_frm.doc.detalle_pagos[0].cantidad = t_total;

	cur_frm.refresh_field('detalle_pagos');

}

function validarSaldos(){
	let t_total = 0;
	$.each(cur_frm.doc.detallepago_fac_compras, function (i, row) {
		t_total += row.valor_apagar;
	});
	let t_total1 = 0;
	$.each(cur_frm.doc.detalle_pagos, function (i, row) {
		t_total1 += row.cantidad;
	});

	if(t_total1 != t_total && cur_frm.doc.tipo=="COMPRAS"){
		frappe.throw("La cantidad especificada en formas de pago, no coincide con los valores a pagar en listado de facturas ")
	}

}
function getdatosFacturaGastos(frm, doc, cdt, cdn) {

	if (doc.factura !== undefined && doc.factura !== null) {
		frappe.call({
			doc: frm.doc,
			method: "getdatosFacturaGastos",
			args: { name: doc.factura },
			callback: r => {

				frappe.model.set_value(cdt, cdn, "totalfactura", r.message.importetotal);
				frappe.model.set_value(cdt, cdn, "saldo", r.message.saldo);
				//  frappe.model.set_value(cdt, cdn, "preciounitario", r.message[0]);
				//  frappe.model.set_value(cdt, cdn, "esservicio", r.message[2]);

			}
		});
	}
}


function getdatosFacturaInventarios(frm, doc, cdt, cdn) {

	if (doc.factura !== undefined && doc.factura !== null) {
		frappe.call({
			doc: frm.doc,
			method: "getdatosFacturaInventarios",
			args: { name: doc.factura },
			callback: r => {
				frm.add_child('detallepago_fac_compras', { totalfactura: r.message.importetotal, saldo: r.message.saldo, valor_apagar: r.message.saldo });
				// frm.refresh_field('gastos_act3');

			}
		});
	}
}


