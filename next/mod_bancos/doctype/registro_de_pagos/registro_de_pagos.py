# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from next.utilidades.utils import *


class RegistrodePagos(Document):
    def before_insert(self):
        setEmisor(self)

    def getdatosFacturaGastos(self, name):
        factura = frappe.get_doc('Factura Compras Gastos', name)
        return factura

    def getdatosFacturaInventarios(self, name):
        factura = frappe.get_doc('Factura Compras', name)
        return factura

    def on_update(self):
        if self.docstatus == 1:
            sqlup = """ update `tabFactura de Compras` c
			inner join tabdetallepago_fac_compras d on ( c.name= d.factura)
			set c.saldo = c.saldo - d.valor_apagar 
			where d.parent = '{0}' """.format(self.name)           
            frappe.db.sql(sqlup)
            for rw in self.detallepago_fac_compras:
                doc = frappe.get_doc("Factura de Compras", rw.factura )
                doc.reload()


@frappe.whitelist()
def getFacturasGastoPendientesdePago(doctype, txt, searchfield, start, page_len, filters):
    if filters.get('proveedores'):
        proveedores = filters.get('proveedores')
        sql = " select   name,nproveedor, CONCAT('Total:',format(importetotal,2),'   Saldo:',format(saldo,2)) AS resumen   FROM  `tabFactura Compras Gastos` where  proveedor ='{0}'   and saldo <> 0 ".format(
            proveedores)
        return frappe.db.sql(sql)


@frappe.whitelist()
def getFacturasPendientesdePago(doctype, txt, searchfield, start, page_len, filters):
    proveedores = filters.get('proveedores')
    sql = " select   name,nproveedor, CONCAT('Total:',format(importetotal,2),'   Saldo:',format(saldo,2)) AS resumen   FROM  `tabFactura de Compras` where  proveedor ='{0}'   and saldo <> 0 ".format(
        proveedores)
    return frappe.db.sql(sql)
