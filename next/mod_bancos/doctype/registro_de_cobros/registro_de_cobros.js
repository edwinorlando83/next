// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('Registro de Cobros', {
  refresh: function (frm) {
    borrarCobro();
    if (frm.doc.docstatus==1) {
      cur_frm.add_custom_button(__('VER RECIVO'), function () {
        window.open(
          frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
              + "doctype=" + encodeURIComponent("Registro de Cobros")
              + "&name=" + encodeURIComponent(cur_frm.doc.name)
              + "&format=" + encodeURIComponent('f_recivo')
              + "&no_letterhead=0"));
        }).css({'background-color':'#FCC60B','font-weight': 'bold' , 'color':'black'});

    } 
    filtros();
  },
  onload: function (frm) {
 
  },
  validate: function (frm) {
    validaciones();
    if (frm.doc.tipo == 'ANTICIPO') {
      frm.clear_table('detallepago_fac_ventas')
      frm.clear_table('detalle_cuotas_cobros')
    }
    if (frm.doc.tipo == 'FACTURA') {
      frm.clear_table('detalle_cuotas_cobros')
    }
    if (frm.doc.tipo == 'CXC INICIALES') {

      validarpagoCXCINI();
   
    }
   

  },
  clientes: function (frm) { }



})
frappe.ui.form.on('detallepago_fac_ventas', {
  factura: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      getdatosFactura(frm, doc, cdt, cdn)
    }
  },
  cantidad: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      verificarCantidad(frm, doc, cdt, cdn)
      calcula_total()
    }
  }

});


frappe.ui.form.on('detallepago_otros', {
  factura: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      getdatosOtros(frm, doc, cdt, cdn)
    }
  },
  cantidad: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      verificarCantidad(frm, doc, cdt, cdn)
      calcula_total()
    }
  }

});

function validaciones() {
  let suma = 0
  $.each(cur_frm.doc.detalle_pagos, function (i, row) {
    suma += row.cantidad
  })
  if (suma == 0) {
    frappe.throw('Debe ingresar al menos una cantidad')
  }
}

function verificarCantidad(frm, doc, cdt, cdn) {
  if (doc.cantidad > doc.saldo) {
    frappe.show_alert({ message: __('La cantidad a pagar es mayor que el saldo.'), indicator: 'red' })
    doc.cantidad = doc.saldo
  }
}
function getdatosFactura(frm, doc, cdt, cdn) {
  if (doc.factura !== undefined && doc.factura !== null) {
    frappe.call({
      doc: frm.doc,
      method: 'getdatosFactura',
      args: { name: doc.factura },
      callback: r => {

        frappe.model.set_value(cdt, cdn, 'totalfactura', r.message.importetotal)
        frappe.model.set_value(cdt, cdn, 'saldo', r.message.saldo)
        frappe.model.set_value(cdt, cdn, 'cantidad', r.message.saldo)
        //  frappe.model.set_value(cdt, cdn, "preciounitario", r.message[0])
        //  frappe.model.set_value(cdt, cdn, "esservicio", r.message[2])

      }
    })
  }
}

function getdatosOtros(frm, doc, cdt, cdn) {
  if (doc.factura !== undefined && doc.factura !== null) {
    frappe.call({
      doc: frm.doc,
      method: 'getdatosOtros',
      args: { name: doc.factura },
      callback: r => {

        frappe.model.set_value(cdt, cdn, 'totalfactura', r.message.importetotal)
        frappe.model.set_value(cdt, cdn, 'saldo', r.message.saldo)
        frappe.model.set_value(cdt, cdn, 'cantidad', r.message.saldo)
        //  frappe.model.set_value(cdt, cdn, "preciounitario", r.message[0])
        //  frappe.model.set_value(cdt, cdn, "esservicio", r.message[2])

      }
    })
  }
}


frappe.ui.form.on('detalle_pagos', {
  refresh: function (frm) { }, tipo: function (frm, cdt, cdn) { }
})

function calcula_total() {
  let t_total = 0
  $.each(cur_frm.doc.detallepago_fac_ventas, function (i, row) {
    t_total += row.cantidad
  })
  cur_frm.doc.detalle_pagos[0].cantidad = t_total

  cur_frm.refresh_field('detalle_pagos')
}
function borrarCobro() {
  if (cur_frm.doc.docstatus == 1) {

    cur_frm.page.add_menu_item('ANULAR/CANCELAR COBRO', function () {
      frappe.confirm('¿Esta seguro de ANULAR/CANCELAR  el cobro seleccionado?',
        () => {


          frappe.call({
            doc: cur_frm.doc,
            method: 'borrarcobro',
            callback: r => {
              frappe.msgprint(r.message);
              cur_frm.refresh_fields();

            }
          });


        }, () => {

        });
    });


  }
}

function validarpagoCXCINI(){
  let saldocxc=0;
  let saldodetpagos=0;

  $.each(cur_frm.doc.registro_cobros_cxc_iniciales, function (i, row) {
    saldocxc += row.valor;
  });
  $.each(cur_frm.doc.detalle_pagos, function (i, row) {
    saldodetpagos += row.cantidad;
  });
  if(saldodetpagos != saldocxc || saldocxc == 0 || saldodetpagos==0  ){
    frappe.throw('Los valores de CXC Inicial y Formas de Pago deben coincidir, y ser mayores que cero');
  }
}

function filtros(){
 
  cur_frm.set_query('factura', 'detallepago_fac_ventas', function () {
    return {
      query: 'next.mod_bancos.doctype.registro_de_cobros.registro_de_cobros.getFacturasPendientesdePago',
      filters: { 'cliente': cur_frm.doc.clientes }
    }
  })

  cur_frm.set_query('cliente_deudas', 'detalle_cuotas_cobros', function () {
    return {
      filters: [
        ['cliente', '=', cur_frm.doc.clientes],
        ['saldo', '>', 0]
      ]
    }
  })

  cur_frm.set_query('cuentasxcobrar', 'registro_cobros_cxc_iniciales', function () {
    return {
      filters: [
        ['cliente', '=', cur_frm.doc.clientes],
        ['saldo', '>', 0], ['docstatus', '=', 1]
      ]
    }
  })

  cur_frm.set_query('cheque_postfechado', 'detalle_pagos', function () {
    return {
      filters: [
        ['cliente', '=', cur_frm.doc.clientes],
        ['saldo', '>', 0],
        ['docstatus', '=', 1]
      ]
    }
  });

  cur_frm.set_query('factura', 'detallepago_otros', function () {
    return {
      query: 'next.mod_bancos.doctype.registro_de_cobros.registro_de_cobros.getOtrosDocumentosPendientesdePago',
      filters: { 'cliente': cur_frm.doc.clientes }
    }
  });
  

}