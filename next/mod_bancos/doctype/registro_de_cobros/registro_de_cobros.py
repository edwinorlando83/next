from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from next.utilidades.utils import *

class RegistrodeCobros(Document):
	def before_insert(self):
		setEmisor(self)
    		
	def validate (self):
		suma = 0
		suma_fac = 0
		for row in self.detalle_pagos:
			suma = suma + row.cantidad
		for rf in self.detallepago_fac_ventas:
			suma_fac = suma_fac + rf.cantidad

		self.totalcobro = suma
		self.total_fac = suma_fac
		if self.totalcobro != self.total_fac and self.detallepago_fac_ventas :
			frappe.throw("El total de los pagos no coincide con el total de las facturas")

		if self.tipo == "FACTURA": 	
			self.detallepago_otros = []
		
		if self.tipo=="OTROS":
			self.detallepago_fac_ventas = []		

		verificasaldoporAnticipos(self)

	def on_update(doc):
		if doc.docstatus == 1:
			if doc.tipo == "FACTURA": 			
				updateSaldoFactura(doc)
			if doc.tipo == "CUOTAS":
				updateSaldoCuotas(doc)
			if doc.tipo=="CXC INICIALES":
				updateSaldoCXC(doc)
			if doc.tipo=="OTROS":
				updateSaldoOtros(doc)
			
			updateSaldoCliente(doc)

	@frappe.whitelist()
	def getdatosFactura(self,name):
		factura=frappe.get_doc('Factura en Ventas',name)
		return factura

	@frappe.whitelist()
	def getdatosOtros(self,name):
		factura=frappe.get_doc('Solicitud',name)
		return factura

	@frappe.whitelist()
	def borrarcobro(self):
		sql =""" update `tabRegistro de Cobros`   set docstatus = 2  where name = '{0}' """.format( self.name)
		frappe.db.sql(sql)	 
		if self.tipo == "FACTURA": 			
			updateSaldoFactura_anular(self)
		if self.tipo == "CUOTAS":
			updateSaldoCuotas_anular(self)
		if self.tipo=="CXC INICIALES":
			updateSaldoCXC_anular(self)
		return "Anulado correctamente"

 
		



@frappe.whitelist()
def getdatosFactura(name):
	factura= frappe.get_doc('Factura en Ventas',name)	 
	return factura

@frappe.whitelist()
def getFacturasPendientesdePago(doctype, txt, searchfield, start, page_len, filters):
	cliente= filters.get('cliente')
	sql = "select   name, CONCAT('Total:',format(importetotal,2),'   Saldo:',format(saldo,2)) AS resumen  from  `tabFactura en Ventas` where cliente ='{0}' and docstatus =1 and saldo <> 0 ".format(cliente)
	return frappe.db.sql(sql)

def updateSaldoFactura(doc):   
		sqlup="UPDATE  `tabFactura en Ventas` T1  INNER JOIN   `tabdetallepago_fac_ventas` T2    ON T1.name = T2.factura SET T1.saldo= T1.saldo - T2.cantidad WHERE T2.parent =   '"+doc.name+"' "
		frappe.db.sql(sqlup)
		for rw in doc.detallepago_fac_ventas:
			doc = frappe.get_doc("Factura en Ventas", rw.factura )
			doc.reload()

def updateSaldoFactura_anular(doc):   
		sqlup="UPDATE  `tabFactura en Ventas` T1  INNER JOIN   `tabdetallepago_fac_ventas` T2    ON T1.name = T2.factura SET T1.saldo= T1.saldo + T2.cantidad WHERE T2.parent =   '"+doc.name+"' "
		frappe.db.sql(sqlup)
		for rw in doc.detallepago_fac_ventas:
			doc = frappe.get_doc("Factura en Ventas", rw.factura )
			doc.reload()

def updateSaldoCuotas(self):
	#obj = frappe.get_doc('Registro de Cobros',self.name)
	escxci=False
	for row in self.detalle_cuotas_cobros:
		cd = frappe.get_doc("cliente_deudas", row.cliente_deudas)
		cd.saldo = cd.saldo - row.valor_pagado
		cd.save()

		if cd.cuentasxcobrar:			 
			sql = """ update tabcuentasxcobrar set saldo = saldo - {0}  where name='{1}'  """.format( row.valor_pagado, cd.cuentasxcobrar)
			frappe.db.sql(sql)
			escxci = True
	
	if escxci==True:
		descripcion = "CXC   " + self.descripcion 
		sql_rc =""" update  `tabRegistro de Cobros` set descripcion='{0}' where name ='{1}' """.format(descripcion,self.name)
		frappe.db.sql(sql_rc)

	sql =""" select distinct solicitud from tabdetalle_cuotas_cobros tcc  where parent ='{0}' and solicitud is not null """.format(self.name)
	res = frappe.db.sql(sql, as_dict=True)
	for rw in res:
		if frappe.db.exists("Solicitud", rw.solicitud ):
			doc = frappe.get_doc("Solicitud", rw.solicitud )
			doc.reload()

def updateSaldoCuotas_anular(self):
	#obj = frappe.get_doc('Registro de Cobros',self.name)
	escxci=False
	for row in self.detalle_cuotas_cobros:
		cd = frappe.get_doc("cliente_deudas", row.cliente_deudas)
		cd.saldo = cd.saldo - row.valor_pagado

		cd.save()

		if cd.cuentasxcobrar:			 
			sql = """ update tabcuentasxcobrar set saldo = saldo + {0}  where name='{1}'  """.format( row.valor_pagado, cd.cuentasxcobrar)
			frappe.db.sql(sql)
			escxci = True
	
	if escxci==True:
		descripcion = "CXC   " + self.descripcion 
		sql_rc =""" update  `tabRegistro de Cobros` set descripcion='ANULADO' where name ='{1}' """.format(descripcion,self.name)
		frappe.db.sql(sql_rc)

	sql =""" select distinct solicitud from tabdetalle_cuotas_cobros tcc  where parent ='{0}' """.format(self.parent)
	res = frappe.db.sql(sql, as_dict=True)
	for rw in res:
		doc = frappe.get_doc("Solicitud", rw.solicitud )
		doc.reload()
		

def updateSaldoCXC(self):
	for row in self.registro_cobros_cxc_iniciales:
		sql = """ update tabcuentasxcobrar set saldo = saldo - {0}  where name='{1}'  """.format( row.valor, row.cuentasxcobrar)
		frappe.db.sql(sql)

def updateSaldoOtros(self):
	sqlup="UPDATE  `tabSolicitud` T1  INNER JOIN   `tabdetallepago_otros` T2    ON T1.name = T2.factura SET T1.saldo= T1.saldo + T2.cantidad WHERE T2.parent =   '"+self.name+"' "
	frappe.db.sql(sqlup)
	for rw in self.detallepago_fac_ventas:
		doc = frappe.get_doc("Solicitud", rw.factura )
		doc.reload()


def updateSaldoCXC_anular(self):
	for row in self.registro_cobros_cxc_iniciales:
		sql = """ update tabcuentasxcobrar set saldo = saldo + {0}  where name='{1}'  """.format( row.valor, row.cuentasxcobrar)
		frappe.db.sql(sql)

@frappe.whitelist()
def getOtrosDocumentosPendientesdePago(doctype, txt, searchfield, start, page_len, filters):
	cliente= filters.get('cliente')
	sql = "select   name, CONCAT('Total:',format(importetotal,2),'   Saldo:',format(saldo,2)) AS resumen  from  tabSolicitud where cliente ='{0}' and docstatus =1 and saldo <> 0 and tiene_cuotas=1 ".format(cliente)
	return frappe.db.sql(sql)

def updateSaldoCliente(self):
	cliente = frappe.get_doc("Clientes", self.clientes)
	saldo = cliente.saldo 
	if self.tipo == "ANTICIPO":
		saldo = saldo * -1
		
	saldoactual =  saldo - self.totalcobro 
	frappe.db.set_value("Clientes", self.clientes, "saldo", saldoactual )

def verificasaldoporAnticipos(self):
	esanticipo= False
	for row in self.detalle_pagos:
		if row.tipo == "ANTICIPO":
			esanticipo= True

	if esanticipo:
		cliente = frappe.get_doc("Clientes", self.clientes)
		if self.totalcobro > cliente.saldo:
			frappe.throw("No puede registrar este Pago, su saldo Actual es: {0}".format(cliente.saldo))