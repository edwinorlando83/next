

frappe.ui.form.CMultiSelectDialog = Class.extend({

	init: function (opts) {

		$.extend(this, opts);

		var me = this;
		if (this.doctype != "[Select]") {
			frappe.model.with_doctype(this.doctype, function (r) {
				me.make();
			});
		} else {
			this.make();
		}
	},
	make: function () {
		let me = this;

		this.page_length = 20;
		this.start = 0;
		this.objdata = []; 
 
		let fields = [
			{
				fieldtype: "Data",
				label: __("Buscar por"),
				fieldname: "search_term",
				hidden: 0
			},
			
			{
				fieldtype: "Column Break"
			}
		];
		let count = 0;
		if ($.isArray(this.setters)) {
			for (let df of this.setters) {
				if (df.hidden == 0)
					fields.push(df, { fieldtype: "Column Break" });
				else
					fields.push(df);
			}
		} else {
			Object.keys(this.setters).forEach(function (setter) {
				fields.push({
					fieldtype: me.target.fields_dict[setter].df.fieldtype,
					label: me.target.fields_dict[setter].df.label,
					fieldname: setter,
					options: me.target.fields_dict[setter].df.options,
					default: me.setters[setter]
				});
				if (count++ < Object.keys(me.setters).length) { 
					fields.push({ fieldtype: "Column Break" });
				}
			});
		}
		if (me.date_field != null) {
			fields = fields.concat([
				{
					"fieldname": "date_range",
					"label": __("Date Range"),
					"fieldtype": "DateRange",
					"hidden": 1
				},
				{ fieldtype: "Section Break" },
				{ fieldtype: "HTML", fieldname: "results_area" },
				{
					fieldtype: "Button", fieldname: "more_btn", label: __("More"),
					click: function () {
						me.start += 20;
						frappe.flags.auto_scroll = true;
						me.get_results();
					}
				}
			]);
		} else {
			fields = fields.concat([

				{ fieldtype: "Section Break" },
				{ fieldtype: "HTML", fieldname: "results_area" },
				{
					fieldtype: "Button", fieldname: "more_btn", label: __("More"),
					click: function () {
						me.start += 20;
						frappe.flags.auto_scroll = true;
						me.get_results();
					}
				}
			]);
		}

	 //cambiar posiscion de Servicios y Buscar por
		let aux = fields[0];
		fields[0]=fields[2];
		fields[2]=aux;
	 


		let doctype_plural = !this.doctype.endsWith('y') ? this.doctype + 's' : this.doctype.slice(0, -1) + 'ies';
		this.dialog = new frappe.ui.Dialog({
			title: __("Select {0}", [(this.doctype == '[Select]') ? __("value") : __(doctype_plural)]),
			fields: fields,
			primary_action_label: __("Procesar Seleccionados"),
			primary_action: function () {
				let ArraySelec = [];
				let seleccionados = me.get_checked_values();


				me.objdata.forEach(function (dato) {
					if (seleccionados.includes(dato.name)) {
						ArraySelec.push(dato);
					}
				});
		 
		 	me.action(ArraySelec, me.args);
			}
		});

		this.$parent = $(this.dialog.body);
		this.$wrapper = this.dialog.fields_dict.results_area.$wrapper.append(`<div class="results"
			style="border: 1px solid #d1d8dd; border-radius: 3px; height: 350px; overflow: auto;"></div>`);

		this.$results = this.$wrapper.find('.results');

		this.$results.append(this.make_list_row());

		this.args = {};

		this.bind_events();
		this.get_results();

		this.dialog.show();
		hideFindBar(me);
		this.dialog.$wrapper.find('.modal-dialog').css("width", "800px");
	},

	bind_events: function () {
		let me = this;

		this.$results.on('click', '.list-item-container', function (e) {
			 
			if (!$(e.target).is(':checkbox') && !$(e.target).is(':input') && !$(e.target).is(':div') ) {
				alert('sss');
				$(this).find(':checkbox').trigger('click');
			}
		});
		this.$results.on('click', '.list-item--head :checkbox', (e) => {
			
			this.$results.find('.list-item-container .list-row-check')
				.prop("checked", ($(e.target).is(':checked')));
		});

		this.$parent.find('.input-with-feedback').on('change', (e) => {
			frappe.flags.auto_scroll = false;
			this.get_results();
		});

		this.$parent.find('.input-with-feedback').on('blur', (e) => {
			frappe.flags.auto_scroll = false;
			this.get_results();
		});

		if (this.date_field = null) {
			this.$parent.find('[data-fieldname="date_range"]').on('blur', (e) => {
				frappe.flags.auto_scroll = false;
				this.get_results();
			});
		}
		
		this.$parent.find('[data-fieldname="search_term"]').on('input', (e) => {
			var $this = $(this);
			clearTimeout($this.data('timeout'));
			$this.data('timeout', setTimeout(function () {
				frappe.flags.auto_scroll = false;
				me.empty_list();
				me.get_results();
			}, 300));
		});
	},

	get_checked_values: function () {
	

	   let arrayCambios=[];
    	this.$results.find("input[type=text]").each(
			function(index){  
				var input = $(this);	
				let valor_real = input.attr('valor-real') ;
				let valor_actual = input.val();
				let namec = input.attr('name');
				let name = input.attr('data-item-name');	 
			 
				if(namec.includes("cantidad") ){
					if(valor_real!=valor_actual ){
						arrayCambios.push({
							'name':name,							 
							'columna':"cantidad",
							'valor':valor_actual
						});
					}
				}

				if(namec.includes("valor") ){
					if(valor_real!=valor_actual ){
						arrayCambios.push({
							'name':name,
							'columna':"valor",
							'valor':valor_actual
						});
					}
				}		 
				
 			}
		); 
 
		this.objdata.forEach(function (dato) {	 
		  arrayCambios.forEach(function (datoc) {
			 if( datoc.name == dato.name  ){				 
				 if(datoc.columna == 'cantidad'  ) dato.cantidad = datoc.valor;
				 if(datoc.columna == 'valor'  ) dato.valor = datoc.valor;
			 }
		  }); 
	   });

		return this.$results.find('.list-item-container').map(function () {
			if ($(this).find('.list-row-check:checkbox:checked').length > 0) {
				let codigo = $(this).attr('data-item-name');				 
				return codigo;
			}
		}).get();
	},

	make_list_row: function (result = {}) {

		var me = this;
		// Make a head row by default (if result not passed)
		let head = Object.keys(result).length === 0;

		let contents = ``;
		let columns = [];
		let flex_size = {};
		//let columns = ["name"];
		if ($.isArray(this.setters)) {
			for (let df of this.setters) {
				if (!df.hidden_query || df.hidden_query==0)
				{
					columns.push(df.fieldname);
				}
				if (df.flex_size && df.flex_size>0)
				{
					flex_size[df.fieldname]=df.flex_size;
				}
			}
		} else {
			columns = columns.concat(Object.keys(this.setters));
		}
		// console.log(flex_size);
		//columns.push("Date");

		columns.forEach(function (column) {
			let contentenido = '';
			let prefix;
			if(column == 'cantidad')
				prefix='cantidad_';
				if(column == 'valor')
				prefix='valor_';

			if (column == 'cantidad' || column == 'valor') {

				let idrow = prefix + result["name"];
				contentenido = `<div class="list-item__content ellipsis" style="flex: 0 0 125px;">
				${head ? `<span class="ellipsis">${__(frappe.model.unscrub(column))}</span>`
						: (column !== "name" ? `<input data-item-name="${__(result["name"])}" type="text" name="${__(idrow)}" class="input-with-feedback form-control"  valor-real="${__(result[column])}" value="${__(result[column])}"></input>`
							: `<a href="${"#Form/" + me.doctype + "/" + result[column]}" class="list-id ellipsis">
							${__(result[column])}</a>`)
					}
			</div>`;
				 
			}
			else {
				let extraFlex="";
				if (flex_size[column])
				{
					extraFlex='style="flex: 0 0 125px;"';
				}
				if (me.empty_message!=undefined && me.empty_message[column]!=undefined)
				{
					let res = result[column];
					if (res==null || res==undefined)
					{
						res = me.empty_message[column];
					}
					contentenido = `<div class="list-item__content ellipsis" ${extraFlex}>
					${head ? `<span class="ellipsis">${__(frappe.model.unscrub(column))}</span>`
	
							: (column !== "name" ? `<span class="ellipsis">${__(res)}</span>`
								: `<a href="${"#Form/" + me.doctype + "/" + res}" class="list-id ellipsis">
								${__(res)}</a>`)
						}
					</div>`;
				}
				else
				{
					contentenido = `<div class="list-item__content ellipsis" ${extraFlex}>
					${head ? `<span class="ellipsis">${__(frappe.model.unscrub(column))}</span>`
	
							: (column !== "name" ? `<span class="ellipsis" style="font-size:9px!important;display:block;word-wrap:break-word;white-space: normal;">${__(result[column])}</span>`
								: `<a href="${"#Form/" + me.doctype + "/" + result[column]}" class="list-id ellipsis">
								${__(result[column])}</a>`)
						}
					</div>`;
				}
			}


			contents += contentenido;


		})
		let $row = $(`<div style="z-index:auto" class="list-item">
			<div class="list-item__content" style="flex: 0 0 10px;">
				<input type="checkbox" class="list-row-check" data-item-name="${result.name}" ${result.checked ? 'checked' : ''}>
			</div>
			${contents}
		</div>`);


		head ? $row.addClass('list-item--head') : $row = $(`<div class="list-item-container" data-item-name="${result.name}" style="z-index:auto"></div>`).append($row);
		return $row;
	},
	setdata: function (data) {
		this.objdata = data;

	},
	render_result_list: function (results, more = 0) {
		var me = this;
		var more_btn = me.dialog.fields_dict.more_btn.$wrapper;

		// Make empty result set if filter is set
		if (!frappe.flags.auto_scroll) {
			this.empty_list();
		}
		more_btn.hide();

		if (results.length === 0) return;
		if (more) more_btn.show();

		results.forEach((result) => {
			me.$results.append(me.make_list_row(result));
		});

		if (frappe.flags.auto_scroll) {
			this.$results.animate({ scrollTop: me.$results.prop('scrollHeight') }, 900);
		}
	},

	empty_list: function () {
		this.$results.find('.list-item-container').remove();
	},

	get_results: function () {
		let me = this;
		let filters = this.get_query ? this.get_query().filters : {};		
		let filter_fields;
		if (me.date_field == null)
			filter_fields = [];
		else
			filter_fields = [me.date_field];
		// console.log(filters);
		if ($.isArray(this.setters)) {
			for (let df of this.setters) {
				filters[df.fieldname] = me.dialog.fields_dict[df.fieldname].get_value() || undefined;
				me.args[df.fieldname] = filters[df.fieldname];
				filter_fields.push(df.fieldname);
			}
		} else {
			Object.keys(this.setters).forEach(function (setter) {
				filters[setter] = me.dialog.fields_dict[setter].get_value() || undefined;
				me.args[setter] = filters[setter];
				filter_fields.push(setter);
			});
		}

		if (this.date_field = null) {
			let date_val = this.dialog.fields_dict["date_range"].get_value();
			if (date_val) {
				filters[this.date_field] = ['between', date_val];
			}
		}
		// console.log(filters);
		if (me.change_value_if_null){
			if (filters[me.change_value_if_null["field_null"]]==undefined)
			{
				filters[me.change_value_if_null["field_change"]]=me.change_value_if_null["field_value"];
			}
		}
		// console.log(filters);
		let args = {
			doctype: me.doctype,
			txt: me.dialog.fields_dict["search_term"].get_value(),
			filters: filters,
			filter_fields: filter_fields,
			start: this.start,
			page_length: this.page_length + 1,
			query: this.get_query ? this.get_query().query : '',
			as_dict: 1
		}
		
		frappe.call({
			type: "GET",
			method: 'frappe.desk.search.search_widget',
			no_spinner: false,
			args: args,
			callback: function (r) {
				if(r.values.length<=0 && me.use_empty_query!=undefined && me.use_empty_query==1)
				{
					me.get_results_empty();
				}
				else{
					let results = [], more = 0;
					me.setdata(r.values);
					if (r.values.length) {
						if (r.values.length > me.page_length) {
							r.values.pop();
							more = 1;
						}
						r.values.forEach(function (result) {
							if (me.date_field in result) {
								result["Date"] = result[me.date_field]
							}
							result.checked = 0;
							result.parsed_date = Date.parse(result["Date"]);
							results.push(result);
						});
						results.map((result) => {
							result["Date"] = frappe.format(result["Date"], { "fieldtype": "Date" });
						})
	
						results.sort((a, b) => {
							return a.parsed_date - b.parsed_date;
						});
	
						// Preselect oldest entry
						if (me.start < 1 && r.values.length === 1) {
							results[0].checked = 1;
						}
					}
					me.render_result_list(results, more);
				}
			}
		});
	},

	get_results_empty: function () {
		let me = this;
		let filters = this.get_query ? this.get_empty_query().filters : {};
		let filters_values = this.get_query ? this.get_empty_query().filters : {};
		let filter_fields;
		if (me.date_field == null)
			filter_fields = [];
		else
			filter_fields = [me.date_field];

		if ($.isArray(this.setters)) {
			for (let df of this.setters) {
				if (!filters_values.hasOwnProperty(df.fieldname))
				{
					filters[df.fieldname] = me.dialog.fields_dict[df.fieldname].get_value() || undefined;
				}
				else{
					filters[df.fieldname] = filters_values[df.fieldname] || undefined;
				}
				me.args[df.fieldname] = filters[df.fieldname];
				filter_fields.push(df.fieldname);
			}
		} else {
			Object.keys(this.setters).forEach(function (setter) {
				filters[setter] = me.dialog.fields_dict[setter].get_value() || undefined;
				me.args[setter] = filters[setter];
				filter_fields.push(setter);
			});
		}
		
		if (this.date_field = null) {
			let date_val = this.dialog.fields_dict["date_range"].get_value();
			if (date_val) {
				filters[this.date_field] = ['between', date_val];
			}
		}
		// console.log(filters);
		if (me.change_value_if_null){
			if (filters[me.change_value_if_null["field_null"]]==undefined)
			{
				filters[me.change_value_if_null["field_change"]]=me.change_value_if_null["field_value"];
			}
		}
		// console.log(filters);
		let args = {
			doctype: me.doctype,
			txt: me.dialog.fields_dict["search_term"].get_value(),
			filters: filters,
			filter_fields: filter_fields,
			start: this.start,
			page_length: this.page_length + 1,
			query: this.get_query ? this.get_empty_query().query : '',
			as_dict: 1
		}
		
		frappe.call({
			type: "GET",
			method: 'frappe.desk.search.search_widget',
			no_spinner: false,
			args: args,
			callback: function (r) {
				let results = [], more = 0;
				me.setdata(r.values);
				if (r.values.length) {
					if (r.values.length > me.page_length) {
						r.values.pop();
						more = 1;
					}
					r.values.forEach(function (result) {
						if (me.date_field in result) {
							result["Date"] = result[me.date_field]
						}
						result.checked = 0;
						result.parsed_date = Date.parse(result["Date"]);
						results.push(result);
					});
					results.map((result) => {
						result["Date"] = frappe.format(result["Date"], { "fieldtype": "Date" });
					})

					results.sort((a, b) => {
						return a.parsed_date - b.parsed_date;
					});

					// Preselect oldest entry
					if (me.start < 1 && r.values.length === 1) {
						results[0].checked = 1;
					}
				}
				me.render_result_list(results, more);
			}
		});
	},
});


function hideFindBar(me)
{
	if(me.hide_find_bar)
	{
		let hh = me.$parent.find(".form-section:first-child");
		hh.toggle();
		hh.hide();
	}
}