function validarCedula(cad) {
  var total = 0;
  var longitud = cad.length;

  if (cad !== "" && (longitud === 10 || longitud === 13)) {
    for (var i = 0; i < 9; i++) {
      if (i % 2 === 0) {
        var aux = cad.charAt(i) * 2;
        if (aux > 9) aux -= 9;
        total += aux;
      } else {
        total += parseInt(cad.charAt(i));
      }
    }
    total = total % 10 ? 10 - (total % 10) : 0;
    if (cad.charAt(9) == total) {
      return 1;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

function validarNumeroFactura(numfac) {
  var mensaje = "El número de la factura debe ser, por ejemplo:  001-011-000000001";
  if (numfac.length != 17)
    return mensaje;
    else{
 
         if( numfac.charAt(3)!='-' && numfac.charAt(7) =='-' )
         return mensaje;
         else
         {
           return 'OK';
         }
    }
}


function imprimirformatosimpresion( doctype_name, formato_name ){
  var w = window.open(
    frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
    +"doctype="+encodeURIComponent(doctype_name)
    +"&name="+encodeURIComponent(data)
    +"&format="+encodeURIComponent(formato_name)
    +"&no_letterhead=0"));
}