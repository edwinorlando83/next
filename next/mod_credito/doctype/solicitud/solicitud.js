// Copyright (c) 2019, Ing. Orlando Cholota and contributors
// For license information, please see license.txt
var frmVentas
frappe.ui.form.on('Solicitud', {
  refresh: function (frm) {
    frmVentas = frm;
    if (frm.doc.docstatus==1) {

      cur_frm.add_custom_button(__('Generar Factura'), function () {

        if (cur_frm.doc.docstatus == 0) {
          frappe.msgprint(__("Primero valide el documento")); return;

        }
        else
          generarfactura();

      });
    }
  },
  onload: function (frm) {
    frm.set_query('producto', 'detalle_solicitudcred', function (doc, cdt, cdn) {
      // const row = locals[cdt][cdn]
      return {
        query: 'next.mod_ventas.doctype.factura_en_ventas.factura_en_ventas.get_productos',
        filters: {
          listaprecio: frm.doc.listaprecios,
          almacen: frm.doc.estab
        }
      }
    })
  },
  entrada: function (frm) {
    saldo()
  },
  extra: function (frm) {
    saldo()
  },
  validate: function (frm) {
    if (cur_frm.doc.saldo < 0) {
      frappe.throw('El saldo no puede ser negativo')
    }
  },
  btngenerar: function (frm) {
    frappe.prompt([
      {
        fieldname: 'fecha_inicio',
        label: __('Fecha Inicio'),
        fieldtype: 'Date',
        reqd: 1
      },

      {
        fieldname: 'numeroc',
        label: __('# Cuotas'),
        fieldtype: 'Int',
        reqd: 1,
        default: 12
      }

    ], (data) => {

      generarCuotas(data.fecha_inicio, data.numeroc)
    })
  },
  estab: function (frm) {
    getPuntosEmision(frm)
  }

})
function generarfactura() {
  frappe.confirm('¿Estas seguro que deseas continuar?',
    () => {
      frappe.call({
        method: 'generarfactura',
        doc: cur_frm.doc,
        freeze: true,
        callback: (r) => {

          frappe.set_route('Form', 'Factura en Ventas', r.message)
        }
      })
    }, () => {
      // action to perform if No is selected
    })
}
function saldo() {
  cur_frm.doc.saldo = cur_frm.doc.importetotal - cur_frm.doc.extra - cur_frm.doc.entrada
  cur_frm.refresh_field('saldo')
}
function generarCuotas(fecha_inicio, cuotas) {
  let suma = 0
  cur_frm.clear_table('solicitud_coutas');
  let cuota = roundNumber(cur_frm.doc.saldo / cuotas, 2)
  var fec1 = fecha_inicio
  for (var i = 1; i < cuotas; i++) {
    cur_frm.add_child('solicitud_coutas', { numero: i, fecha: fec1, valor: cuota })
    fec1 = frappe.datetime.add_months(fec1, 1)
    suma += cuota
  }
  if (suma < cur_frm.doc.saldo)
    cuota = roundNumber(cur_frm.doc.saldo, 2) - roundNumber(suma, 2)
  else
    cuota = roundNumber(suma, 2) - roundNumber(cur_frm.doc.saldo, 2)

  // fec1 =  frappe.datetime.add_months(fec1, 1) 
  cur_frm.add_child('solicitud_coutas', { numero: i, fecha: fec1, valor: cuota })
  cur_frm.refresh_field('solicitud_coutas')
}

function add_months(dt, n) {
  return new Date(dt.setMonth(dt.getMonth() + n))
}

function getPuntosEmision(frm) {
  frappe.call({
    method: 'next.mod_ventas.doctype.factura_en_ventas.factura_en_ventas.getPuntoEmision',
    args: { almacen: frm.get_field('estab').value },
    callback: r => {
      console.log(r.message)
      frm.doc.ptoemi = r.message
      frm.refresh_fields()
    }
  })
}

frappe.ui.form.on('detalle_solicitudcred', {
  cantidad: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },
  preciounitario: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  desporcentaje: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  producto: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      getPrecioEiva(frm, doc, cdt, cdn)
      // stock_por_producto(frm, doc, cdt, cdn)
    }
  },
  detalle_solicitudcred_remove: (frm) => {

    calcular_valores()
  }

});


function calcular_valores() {
  var totalDescuentos = 0;
  var subtotal_iva12 = 0;
  var subtotal_iva0 = 0;
  var subtotal_nosujiva = 0;

  var iva_total = 0;

  $.each(cur_frm.doc.detalle_solicitudcred, function (i, row) {
    // calcula descuento
    let valordescuento = 0;

    if (row.desporcentaje > 0)
      valordescuento = (row.preciounitario * row.desporcentaje) / 100;

    row.descuento = valordescuento

    let precio_calc = roundNumber((row.preciounitario - valordescuento) * row.cantidad, 2);

    if (row.sri_iva == "6") //No Objeto de Impuesto
    {
      subtotal_nosujiva += precio_calc;
      row.iva_valor = 0;
    }
    else
      if (row.sri_iva == "0") //0%
      {
        subtotal_iva0 += precio_calc;
        row.iva_valor = 0;
      }
      else // 12% / 14%
      {
        let iva_valor = (precio_calc * row.tarifa_iva) / 100;
        row.iva_valor = roundNumber(iva_valor, 2);
        subtotal_iva12 += precio_calc;

      }

    row.preciototalsinimpuesto = precio_calc;
    row.totallinea = roundNumber(precio_calc + row.iva_valor, 2);
    totalDescuentos += row.descuento;
    iva_total += row.iva_valor;
  })

  cur_frm.doc.totaldescuento = totalDescuentos;
  cur_frm.doc.totalsinimpuestos = subtotal_iva12;
  cur_frm.doc.subtotal0 = subtotal_iva0;
  cur_frm.doc.noobjetoiva = subtotal_nosujiva;
  cur_frm.doc.iva_total = iva_total;
  cur_frm.doc.importetotal = roundNumber(subtotal_iva12 + subtotal_iva0 + subtotal_nosujiva, 2);

  cur_frm.doc.saldo = cur_frm.doc.importetotal;

  cur_frm.refresh_fields();
}
function getPrecioEiva(frm, doc, cdt, cdn) {
  if (cur_frm.doc.listaprecios === undefined) {
    frappe.msgprint('Seleccione una lista de precios')
    return
  }
  if (doc.producto !== undefined && doc.producto !== null) {
    frappe.call({
      method: 'next.mod_ventas.doctype.factura_en_ventas.factura_en_ventas.getPrecioEiva',
      freeze: true,
      args: {
        inproducto: doc.producto,
        listaPrecio: cur_frm.doc.listaprecios,
        inalmacen: cur_frm.doc.estab
      },
      callback: r => {
        console.log(r.message)
        frappe.model.set_value(cdt, cdn, 'preciounitario', r.message[0])
        frappe.model.set_value(cdt, cdn, 'tarifa_iva', r.message[1])
        frappe.model.set_value(cdt, cdn, 'sri_iva', r.message[3])
        cur_frm.refresh_fields()
        calcular_valores()
      }
    })
  }
}



