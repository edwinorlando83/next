# -*- coding: utf-8 -*-
# Copyright (c) 2019, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime
from next.utilidades.utils import *

class Solicitud(Document):
	def on_update(self):
		insert_cliente_deudas(self)
		if self.docstatus==1:                      
			updateStock(self)
			RegistarSalidaProductos(self)
			actulizarSaldoCliente(self)
			registarCobro(self)
			

	def validate(self):
		if len(self.solicitud_coutas) > 0 :
			self.tiene_cuotas = 1
		else:
			self.tiene_cuotas = 0
			
		cnfg_inv_negativo = frappe.db.get_single_value("configuraciones_generales","cnfg_inv_negativo")
		if cnfg_inv_negativo == 0:
			ComprobarStock(self)
	
	@frappe.whitelist()
	def generarfactura(self):
		return generar_factura(self)
        
  
def registarCobro(doc):
	if doc.entrada >0 or doc.extra >0:
		rc = frappe.new_doc("Registro de Cobros")
		rc.tipo = "OTROS"
		rc.fecha = datetime.datetime.now()
		rc.descripcion ="Entrada:{0}  Extra:{1}  ".format(doc.entrada,doc.extra)
		rc.clientes = doc.cliente
		rc.solicitud = doc.name 
		total =  doc.entrada + doc.extra 
		rc.append("detalle_pagos", {"tipo":"EFECTIVO" ,"cantidad":total  })      
		rc.insert()
		rc.docstatus = 1
		rc.save()
		doc.saldo = 0

def ComprobarStock(doc):
    msg=""
    for pro in doc.detalle_solicitudcred:
        prodc = frappe.get_doc("Productos", pro.producto)
        existe = False
        for pa in prodc.producto_almacen:
            if pa.almacen == doc.estab:
                existe = True
                if pa.cantidad < pro.cantidad:
                    msg = "El stock del producto: " + pro.producto + " es insuficiente, " + "Existen: " + str(pa.cantidad) + " Solicitados: " + str(pro.cantidad)
                    frappe.throw(msg)
        if not existe and pro.esservicio == 0:
            frappe.throw("El producto: " + pro.producto + " no tiene un stock en el almacen: " + doc.estab)

def RegistarSalidaProductos(doc):
    if doc.bajarstock == 1:
        for pro in doc.detalle_solicitudcred:
            if pro.esservicio == 0: 
                insert_kardex( "SOLICITUD DE CREDITO", doc.estab,pro.producto,pro.preciounitario, pro.cantidad,doc.name )     


def updateStock(doc):
    if doc.bajarstock == 1:
        sqlup=" update tabproducto_almacen p inner join  tabdetalle_solicitudcred t on (  t.parent = '"+doc.name+"' and t.producto = p.parent  ) inner join `tabFactura en Ventas` fac on ( fac.name = t.parent  and fac.estab = p.almacen )  set p.cantidad = p.cantidad -  t.cantidad    "
        frappe.db.sql(sqlup)

def insert_cliente_deudas(self):
		if self.docstatus == 1:
			for dt in self.solicitud_coutas:    			
				cliente_deudas = frappe.new_doc("cliente_deudas")
				cliente_deudas.cliente = self.cliente
				cliente_deudas.cuota_numero = dt.numero
				cliente_deudas.cuota_valor = dt.valor
				cliente_deudas.fecha = dt.fecha
				cliente_deudas.solicitud = self.name
				cliente_deudas.saldo = dt.valor
				cliente_deudas.insert()
		


		 
def generar_factura(doc):
	existe = frappe.db.exists( "Factura en Ventas", {"solicitud":doc.name})
	if existe:
		return existe

	fac = frappe.new_doc("Factura en Ventas")
	fac.estab = doc.estab
	fac.eslectronica = usa_facelec()
	fac.cliente = doc.cliente
	fac.listaprecios =  doc.listaprecios
	fac.bajarstock = 0
	fac.solicitud = doc.name
	fac.saldo = 0
	fac.importetotal =  doc.importetotal
	fac.iva_total  =  doc.iva_total
	fac.totalsinimpuestos  =  doc.totalsinimpuestos
	fac.totaldescuento  =  doc.totaldescuento
	
	for pro in doc.detalle_solicitudcred:
		producto = frappe.get_doc("Productos", pro.bodp_producto)
		fac.append('detalles_facventas', {
                    "producto":  pro.producto,
					 "nproducto":  producto.nombre,
                    "codigoprincipal":pro.codigoprincipal ,
                    "codigoauxiliar":pro.codigoauxiliar ,
                    "descripcion": pro.descripcion,
                    "cantidad":pro.cantidad ,
                    "preciounitario":pro.preciounitario ,
                    "descuento":pro.descuento ,
                    "preciototalsinimpuesto": pro.preciototalsinimpuesto,
                    "tarifa_iva": pro.tarifa_iva,
                    "iva_valor": pro.iva_valor,
                    "totallinea":pro.totallinea
                })
	cli = frappe.get_doc("Clientes",doc.cliente)
	correo = cli.correo
	if not correo:
		correo = 'sin@correo.com'
    		
	fac.append('infoadicional', {"nombre":"correo" , "valor": correo})
	
	fac.insert()
	return fac.name

def actulizarSaldoCliente(self):
    cliente = frappe.get_doc("Clientes", self.cliente)
    saldo = cliente.saldo 		
    saldoactual =  saldo  +  self.importetotal 
    frappe.db.set_value("Clientes", self.clientes, "saldo", saldoactual )