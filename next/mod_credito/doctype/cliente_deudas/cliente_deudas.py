# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class cliente_deudas(Document):
	def before_insert(self):
		if self.solicitud :
			self.codigo = self.solicitud + "-" + str(self.cuota_numero)
		else:
			if self.cuentasxcobrar :
				self.codigo = self.cuentasxcobrar + "-" + str(self.cuota_numero)
