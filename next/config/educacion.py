from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
          {
            "label": _("Academia"),
            "items": [
                {
                    "type": "doctype",
                     "name": "Periodo"
                }
            ]
        },
        {
            "label": _("Estudiantes"),
            "items": [
                {
                    "type": "doctype",
                     "name": "Estudiantes"
                }
            ]
        },{
            "label": _("Docentes"),
            "items": [
                {
                    "type": "doctype",
                     "name": "Docentes"
                }
            ]
        }
        ]
