from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
       
             {
            "label": _("Registros"),
            "items": [
             
                {
                    "type": "doctype",
                     "name": "Periodo Contable"
                },
                {
                    "type": "doctype",
                     "name": "Plan de Cuentas"
                }
                ,
                {
                    "type": "doctype",
                     "name": "Asientos por defecto"
                }
                ,
                {
                    "type": "doctype",
                     "name": "LibroDiario",
                     "label": "Libro Diario"
                }
            ]
        } 
        
       
        ]
