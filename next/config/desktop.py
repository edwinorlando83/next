# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [  
		{
			"module_name": "Next",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": "Facturación"
		},
        {
			"module_name": "Contabilidad",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": "Contabilidad"
		} 
        ,
        {
			"module_name": "jaap",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": "Administración de Agua"
		} 
		 ,
        {
			"module_name": "movil",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": "Pedidos Móviles"
		} 
	]
