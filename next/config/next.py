from __future__ import unicode_literals
from frappe import _


def get_data():
    return [
        
        {
            "label": _("Inventarios"),
            "items": [
                {
                    "type": "doctype",
                            "name": "Almacenes"
                },
                {
                    "type": "doctype",
                            "name": "Grupo Productos"
                },
                {
                    "type": "doctype",
                            "name": "Unidad de Medida"
                },
                {
                    "type": "doctype",
                            "name": "Productos",
                                    "label": "Productos y Servicios"
                },
                {
                    "type": "doctype",
                            "name": "Lista de Precios"
                },
                {
                    "type": "doctype",
                            "name": "Inventario Inicial"
                }, 
                {
                      "type": "doctype",
                     "name":"Movimiento de Inventarios"
                },
                {
                      "type": "doctype",
                     "name":"Ingreso de Productos"
                },
                {
                    "type": "doctype",
                    "name": "Guia de Remision",
                    "label": "Guías de Remisión"
                }
            ]
        },
        {
            
            "label": _("Cuentas por Cobrar"),
            "items": [

                {
                    "type": "doctype",
                            "app_icon": "file-alt",
                    "name": "Factura en Ventas"
                },
                {
                    "type": "doctype",
                            "name": "Registro de Cobros",
                            "label": "Registro de Cobros  (Caja General)"
                },


                {
                    "type": "doctype",
                            "name": "Categoria",
                                    "label": "Categoría de Cliente"
                },
                {
                    "type": "doctype",
                            "name": "Clientes"
                },

                {
                    "type": "doctype",
                            "name": "notasCredito",
                                    "label": "Notas de Credito  (Ventas)"
                },

                {
                    "type": "doctype",
                            "name": "Saldos Iniciales CXC",
                                    "label": "Saldos Iniciales de Cuentas por Cobrar"
                }
            ]
        } , {
            "label": _("Cuentas por Pagar"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Proveedores"
                },
                {
                    "type": "doctype",
                    "name": "Factura de Compras",
                    "label": "Factura de Compras de Inventarios"
                }	,

                {
                    "type": "doctype",
                    "name":  "Factura Compras Gastos",
					"label": "Factura Compras de Gastos"
                },
                {
                    "type": "doctype",
                    "name": "Registro de Pagos"
                },
                {
                    "type": "doctype",
                    "name": "Saldos Iniciales CXP",
                    "label": "Saldos Iniciales de Cuentas por Pagar"
                }
,
                {
                    "type": "doctype",
                    "name": "Retenciones",
                    "label": "Comprobantes de Retención"
                }

            ]
        }, {
            "label": _("Talento Humano"),
            "items": [
                {
                    "type": "doctype",
                    "name": "CargoRRHH",
                            "label": "Cargos de Empleados"
                },
                {
                    "type": "doctype",
                    "name": "Empleados",
                            "label": "Empleados"
                }
,
                {
                    "type": "doctype",
                    "name": "Transportista"
                            
                }

            ]
        }, {
            "label": _("Transacciones"),
            "items": [
                {
                    "type": "doctype",
                    "name": "EgresosCaja",
                            "label": "Egresos de Caja Chica"
                },
                {
                    "type": "doctype",
                    "name": "Cierre Caja",
                            "label": "Cierre de Caja Chica"
                },

                {
                    "type": "doctype",
                    "name": "Cierre Caja General",
                            "label": "Cierre de Caja General"
                }

            ]
        }, {
            "label": _("SRI"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Emisor"
                },
                {
                    "type": "doctype",
                    "name": "Impuestos Venta"
                }
            ]
        }, {
            "label": _("Bancos"),
            "items": [
                {
                    "type": "doctype",
                            "name": "BANCOS",
                                    "label": "Bancos"
                } ,
                {
                    "type": "doctype",
                    "name": "Cuentas Bancarias",
                    "label": "Cuentas Bancarias"
                } ,{
               
                    "type": "doctype",
                    "name": "Movimientos Bancarios",
                    "label": "Movimientos Bancarios"
                } 

            ]
        }, 
        {
            "label": _("Otros"),
            "items": [
                
                {
                    "type": "doctype",
                            "name": "Cargos",
                                    "label": "Cargos"
                },
                {
                    "type": "doctype",
                            "name": "Zonas",
                                    "label": "Zonas"
                }
            ]
        }, 
        {
            "label": _("Standard Reports"),
            "items": [
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Reporte de Kardex",
                    "label": "Reporte de Kardex"
                },
                 {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Cheques Emitidos",
                    "label": "Cheques Emitidos"
                }

            ]
        }, {
            "label": _("Créditos"),
            "items": [
                {
                    "type": "doctype",
                            "name": "Solicitud",
                                    "label": "Solicitud de crédito"
                }
            ]
        }

    ]
