# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Clientes(Document):
	def validate(self):   

		if self.tipo_identificacion == "CEDULA-05" and len(self.ruc) != 10 :
			frappe.throw("La cédula debe tener 10 digitos")
		if self.tipo_identificacion == "RUC-04" and len(self.ruc) != 13 :
			frappe.throw("El ruc debe tener 13 digitos")

		if not self.correo:
			self.correo = 'sin@correo.com'
