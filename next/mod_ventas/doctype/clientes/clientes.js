
frappe.ui.form.on('Clientes', {
	refresh: function (frm) {
		//getCantones_frm();
		//getParroquias();
	},
	cliente_sucursales_on_form_rendered: function (frm) {

	},
	validate: function (frm) {
		 
		if (  frm.doc.tipo_identificacion == 'RUC-04' && frm.doc.ruc.length != 13 )
			frappe.throw(__("El # de  ruc es incorrecto:" + frm.doc.ruc));

       if (    frm.doc.tipo_identificacion == 'CEDULA-05' && frm.doc.ruc.length != 10 )
			frappe.throw(__("El # de cedula   es incorrecto:" + frm.doc.ruc));


	},
	cli_canton: function (frm) {
		getParroquias();
	}
	,
	cli_provincia: function (frm) {
		getCantones_frm();
	}
});

frappe.ui.form.on('cliente_sucursales', {  
    clsu_provincia(frm, cdt, cdn) {  
    	getCantones(frm, cdt, cdn);
    },
	clsu_canton(frm, cdt, cdn) {  
       getParroquias(frm, cdt, cdn);
    }
});

function getCantones(frm, cdt, cdn){
	var fm = cur_frm.fields_dict["cliente_sucursales"].grid.fields_map;
	const doc = frappe.get_doc(cdt, cdn);
	if (doc.clsu_provincia != null)
	{		
		frappe.call({
			method: "next.next.doctype.dpa.dpa.get_cantones",
			freeze: true,
			freeze_message: "Buscar...",
			args: {
				provincia:	doc.clsu_provincia
			},	 
			callback: (r) => {		
				cur_frm.fields_dict.cliente_sucursales.grid.update_docfield_property('clsu_canton', 'options',  r.message);		 
			},
		});
	}
	
}
function getParroquias( ){
	var fm = cur_frm.fields_dict["cliente_sucursales"].grid.fields_map;
	const doc = frappe.get_doc(cdt, cdn);
	if (doc.clsu_canton != null)
	{	
	frappe.call({
		method: "next.next.doctype.dpa.dpa.get_parroquia",
		freeze: true,
		freeze_message: "Buscar...",
		args: {
			provincia:	doc.clsu_provincia,
			canton:	doc.clsu_canton
		},	 
		callback: (r) => {		
			cur_frm.fields_dict.cliente_sucursales.grid.update_docfield_property('clsu_parroquia', 'options',  r.message);		 
		},
	});
}
}


function getCantones_frm( ){
 
	frappe.call({
		method: "next.next.doctype.dpa.dpa.get_cantones",
		freeze: true,
		freeze_message: "Buscar...",
		args: {
			provincia:	cur_frm.doc.cli_provincia
		},	 
		callback: (r) => {		
			cur_frm.set_df_property("cli_canton", "options", r.message);	 
			cur_frm.refresh_field("cli_canton");
			cur_frm.set_df_property("cli_parroquia", "options", []);	 
			cur_frm.refresh_field("cli_parroquia");	 
		},
	});
}

function getParroquias( ){
	 
	frappe.call({
		method: "next.next.doctype.dpa.dpa.get_parroquia",
		freeze: true,
		freeze_message: "Buscar...",
		args: {
			provincia:	cur_frm.doc.cli_provincia,
			canton:	cur_frm.doc.cli_canton
		},	 
		callback: (r) => {		
			cur_frm.set_df_property("cli_parroquia", "options", r.message);	 
			cur_frm.refresh_field("cli_parroquia");	 
		},
	});
}
