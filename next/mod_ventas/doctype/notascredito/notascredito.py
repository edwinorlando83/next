# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from nis import cat
import frappe
from frappe.model.document import Document
import next.next.ecutils as ec
from next.utilidades.utils import *
class notasCredito(Document):
    def before_insert (doc):
        setEmisor(doc)
    
    def validate(self):
         self.iva_actual = getIvaActual()
        
    def on_update(self):
        if self.docstatus==1:
            if self.esdescuento == 0:
                for item in self.detalles_notascredito:
                    cant = item.cantidad * -1
                    insert_kardex( "DEVOLUCIÓN - NC", self.estab,item.producto,item.preciounitario, cant,item.name )               
                
                #sql = """ update `tabFactura en Ventas` set  docstatus = 2    where name='{0}' """.format(self.facturaventa)
                #frappe.db.sql(sql) 
            
            ec.genenarXmlNC(self.clavedeacceso,self)
             
            registarCobroNC(self,self.facturaventa)

    @frappe.whitelist()
    def regenerar_xml(self):
        return ec.genenarXmlNC(self.clavedeacceso,self)


def registarCobroNC(notacredito,factura):      
    doc = frappe.get_doc("Factura en Ventas",factura)
    rc = frappe.new_doc("Registro de Cobros")
    rc.tipo = "FACTURA"
    rc.fecha = datetime.datetime.now()
    rc.descripcion ="Pago con NOTA DE CRÉDITO : " + notacredito.num_nota
    rc.clientes = notacredito.cliente        
    rc.append("detalle_pagos", {"tipo":"NOTA DE CREDITO" ,"notacredito":notacredito.name ,"cantidad":notacredito.importetotal  })
    rc.append("detallepago_fac_ventas", {"factura":factura ,
        "totalfactura":doc.importetotal ,
        "saldo":doc.saldo,
        "cantidad":notacredito.importetotal })
    
    rc.insert()
    rc.docstatus = 1
    rc.save()
   

@frappe.whitelist()     
def get_facturas( doctype, txt, searchfield, start, page_len, filters):
        cond = ''
        if filters and filters.get('cliente'):
            
            cond = "and nombre   LIKE '%s'" % filters.get('nombre')
        #return frappe.db.sql(""" select  p.name, nombre, codigo, unidad , pa.cantidad, pa.almacen, lp.precio, lp.incluyeiva, lp.ivaporcentaje
        return frappe.db.sql("""  select name from `tabFactura en Ventas`
			where estado = 'AUTORIZADO' and 
            cliente=%s and
			notasCredito is null 
            """,  filters.get('cliente')  )

@frappe.whitelist()     
def facturaventa( doctype, txt, searchfield, start, page_len, filters):
        cond = ''
        if filters and filters.get('cliente'):
            
            cond = "and nombre   LIKE '%s'" % filters.get('nombre')
        #return frappe.db.sql(""" select  p.name, nombre, codigo, unidad , pa.cantidad, pa.almacen, lp.precio, lp.incluyeiva, lp.ivaporcentaje
        return frappe.db.sql("""  select name from `tabFactura en Ventas`
			where estado = 'AUTORIZADO' 
			and cliente=%s and
			notasCredito is null 
            """,  filters.get('cliente')  )

            
@frappe.whitelist()     
def get_productos( doctype, txt, searchfield, start, page_len, filters):
         if filters and filters.get('factura'):  
             return frappe.db.sql(""" select producto,preciounitario,cantidad from tabdetalles_facventas  where parent =%s """,  filters.get('factura')  )


def getIvaActual( ):
      iva_actual = frappe.db.get_single_value("configuraciones_generales","iva_actual") 
      if iva_actual:
          dociva = frappe.get_doc("Impuestos Venta",iva_actual)
          return  str(dociva.tarifa)
      else:
          return "12" 
