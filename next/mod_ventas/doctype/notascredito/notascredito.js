// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on("notasCredito", {
  refresh: function (frm) { 
    cur_frm.add_custom_button(__('VER RIDE'), function () {
      window.open(
        frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
            + "doctype=" + encodeURIComponent("notasCredito")
            + "&name=" + encodeURIComponent(cur_frm.doc.name)
            + "&format=" + encodeURIComponent('Ride_NotaCredito')
            + "&no_letterhead=0"));
		  }).css({'background-color':'#FCC60B','font-weight': 'bold' , 'color':'black'});


  },
  btnregenerar: function (frm) {  
    frappe.call({
      doc: frm.doc,
      method: "regenerar_xml",
      callback: r => {
        console.log(r.message)
        frappe.msgprint(r.message);
      }
    });

  },
  onload: function (frm) {
    /*frm.set_query("facturaventa", function(doc, cdt, cdn) {
      return {
        query: "next.next.doctype.notascredito.notascredito.get_facturas",
        filters: {
          cliente: frm.doc.cliente
        }
      };
    }); */

    frm.set_query("producto", "detalles_notascredito", function (doc, cdt, cdn) {
      if (frm.doc.tipo === "Devoluciones") {
        console.log(frm.doc.tipo);
        return {
          query: "next.next.doctype.notascredito.notascredito.get_productos",
          filters: {
            factura: frm.doc.facturaventa
          }
        };
      } else {
        console.log(frm.doc.tipo);
        return {
          filters: {
            esservicio: 1
          }
        };
      }
    });
  },

  cliente: function (frm) { }
});
