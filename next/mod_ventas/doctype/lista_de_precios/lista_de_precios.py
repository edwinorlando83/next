# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ListadePrecios(Document):

		@frappe.whitelist()
		def getIvaPorcentaje(self, inproducto):
			obj_tarifa = frappe.db.sql(""" SELECT tarifa FROM  `tabImpuestos Venta` where nombre = (SELECT impuesto FROM  tabProductos where name =%s) """,inproducto)
			return obj_tarifa[0][0]
