frappe.listview_settings["cuentasxcobrar_tmp"] = {
    refresh: function(listview) {
        //    listview.refresh()
    },
    onload: function(listview) {
        listview.page.add_menu_item(__("Sincronizar"), function() {
                frappe.call({
                    freeze: true,
                    freeze_message: "Procesando...",					 
                    method: "next.mod_ventas.doctype.cuentasxcobrar_tmp.cuentasxcobrar_tmp.sincronizar",
                    callback: (r) => {
                        alert(r.message);
                        
                    }
                });
            }) 
    }
};

 