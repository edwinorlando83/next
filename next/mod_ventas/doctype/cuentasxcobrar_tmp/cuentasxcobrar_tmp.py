# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class cuentasxcobrar_tmp(Document):
	pass
	
@frappe.whitelist()
def sincronizar():    		
	lstcc = frappe.get_all('cuentasxcobrar_tmp',fields=["name","fecha","numero_doc","tipo_doc","cliente","ruc","valor","observaciones"] )
	tt = len(lstcc)
	contador = 0
	for item in lstcc:
		existe = frappe.db.exists("Clientes", { "ruc":item.ruc})
		if not existe:
			existe = frappe.db.exists("Clientes", { "nombres":item.ruclientec})
		
		if existe:
			cc = frappe.new_doc("cuentasxcobrar")
			cc.fecha = item.fecha
			cc.numero_doc = item.numero_doc
			cc.tipo_doc = item.tipo_doc
			cc.cliente = existe
			cc.valor = item.valor
			cc.observaciones = item.observaciones
			cc.saldo = cc.valor
			cc.insert()
			cc.docstatus = 1
			cc.save()
			
			cct = frappe.get_doc("cuentasxcobrar_tmp",item.name)
			cct.cuentasxcobrar = cc.name
			cct.save()
			contador = contador + 1


	return "Se han ingresado " + str(contador) + " de "+ str(tt)