# Copyright (c) 2023, orlando and contributors
# For license information, please see license.txt

# import frappe
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import next.next.ecutils as ec
import random
import datetime
from next.utilidades.utils import *
class guia_remision(Document):
    def before_insert (doc):
        if not doc.ptoemi  :
                doc.ptoemi = getPuntoEmision(doc.estab)
        setEmisor(doc) 

    def on_update(self):        
        if self.docstatus==1:  
                clavedeacceso =  generarnumfac (self)
                ec.generarXMLGuiaRemision(self,clavedeacceso)  

@frappe.whitelist()
def getPuntoEmision( almacen):
        puntoemi =  frappe.db.sql(""" SELECT puntoemision FROM  tabAlmacenes where name=%s""",almacen)[0][0]
        return puntoemi

def generarnumfac(self):
        random.seed()
        obj_ambiente = frappe.db.sql("""select tipo_ambiente from tabEmisor """)
        if not obj_ambiente:
            frappe.throw("EL Ambiente no se ha establecido en el Emisor")

        sri_ambiente = obj_ambiente[0][0]

        obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)

        if not obj_ruc:
            frappe.throw("EL ruc no se ha establecido en el Emisor")
        sri_ruc = obj_ruc[0][0]

        if len(sri_ruc ) < 13 :
            frappe.throw("El ruc para la firma electrónica no tiene 13 dígitos, Por favor diríjase al módulo de facturación,en la sección SRI y emisor para configurarlo correctamente")


        obj_estab = frappe.db.sql("""SELECT establecimiento FROM  tabAlmacenes where name = %s """,self.estab)

        if not obj_estab:
            frappe.throw("EL establecimiento no se ha establecido en el Almacen")
        sri_establecimiento = obj_estab[0][0]


        if str( sri_ambiente ).strip() == 'Pruebas':
            sri_ambiente = "1"
        else:
            sri_ambiente = "2"

        tipoComprobante='06' 
  
        numerofacr = getSecuencial_FAC(sri_ambiente,sri_ruc)  
        
    

        #doc.secuencial= numerofac
        numerofac = str(numerofacr).zfill(9)
      
        numfac_completo =  sri_establecimiento + self.ptoemi +'-'+numerofac
        serie =  sri_establecimiento + self.ptoemi
      
        #doc.num_fac = numfac_completo
        
         
        #doc.sri_ambiente = sri_ambiente

        
        claveacceso = ec.generarClaveAcesso( self.fecha,tipoComprobante,sri_ruc,sri_ambiente,serie,numerofac)
        #doc.barcode_ca = ec.get_svg_from_text_code128(claveacceso)
        #doc.clavedeacceso=claveacceso

        sqlu = """ update  `tabguia_remision`  set  clavedeacceso = '{0}' , barcode_ca='{1}' ,
        sri_ambiente='{2}',sri_establecimiento='{3}' , secuencial ={4} , num_fac='{5}' , estado='GENERADO'
         where name = '{6}' """.format(claveacceso, ec.get_svg_from_text_code128(claveacceso), 
        sri_ambiente,sri_establecimiento,numerofac,numfac_completo,self.name     )    

        frappe.db.sql(sqlu)
        #logger.info(f"{user} accessed counter_app.update with value={value}")
        actualizar_secuencia_fac(sri_ruc,sri_ambiente)
        return  claveacceso

        #ruta = os.path.dirname(__file__)
        #ruta = frappe.local.site
        # ruta = os.path.abspath(frappe.get_site_path("public", "files"))
        #frappe.throw(ruta)

def actualizar_secuencia_fac(ruc,ambiente):
    emisor = frappe.get_doc("Emisor", ruc)
    if ambiente == '1':
        emisor.iniguia_pruebas = emisor.iniguia_pruebas + 1
    else:
        emisor.iniguia = emisor.inifac+ 1
    emisor.save()


def getSecuencial_FAC(ambiente,ruc):
    emisor = frappe.get_doc("Emisor", ruc)
    sec  = 0
    if ambiente == '1':
        sec = emisor.iniguia_pruebas
    else:
        sec = emisor.iniguia
    if not sec:
        sec=0

    if sec== 0 or sec== None:
        sec = 1
    return sec