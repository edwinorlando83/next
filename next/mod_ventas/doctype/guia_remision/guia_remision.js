// Copyright (c) 2023, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('guia_remision', {
	  refresh: function(frm) {
		cur_frm.add_custom_button(__('VER RIDE'), function () {
			window.open(
			  frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
				+ "doctype=" + encodeURIComponent("guia_remision")
				+ "&name=" + encodeURIComponent(cur_frm.doc.name)
				+ "&format=" + encodeURIComponent('RIDE_REMISION')
				+ "&no_letterhead=0"));
		  }).css({ 'background-color': '#FCC60B', 'font-weight': 'bold', 'color': 'black' });
	  
	 },
	onload: function (frm) {
		 
	},
	/* transportistas:function(frm){
		if (frm.doc.transportistas != null && frm.doc.transportistas != ''){
			frappe.call({
				method: "frappe.client.get",
				args: {
					doctype: "transportistas",
					name: frm.doc.transportistas,
				},
				callback(r) {
					
					if(r.message) {
						var transportista = r.message;
						if (transportista.correo != undefined && transportista.correo !=""){
							updateGridData(transportista.correo);
						}else{
							console.log("Transportista no tiene Email")
						}
					}
				}
			});
		}
		
	}, */
	cliente:function(frm){
		if (frm.doc.cliente != null && frm.doc.cliente != ''){
			frappe.call({
				method: "frappe.client.get",
				args: {
					doctype: "Clientes",
					name: frm.doc.cliente,
				},
				callback(r) {
					
					if(r.message) {
						console.log(r)
						var client = r.message;
						frm.set_value("direccion_del_destinatario", client.direccion);
						frm.refresh_field("direccion_del_destinatario");
						if (client.correo != undefined && client.correo !=""){
							updateGridData(client.correo);
						}else{
							console.log("Cliente no tiene Email")
						}
					}
				}
			});
		}
		
	},
	factura:function(frm){
		if (frm.doc.factura != null && frm.doc.factura != ''){
			frappe.call({
				method: "frappe.client.get",
				args: {
					doctype: "Factura en Ventas",
					name: frm.doc.factura,
				},
				callback(r) {
					
					if(r.message) {
						console.log(r)
						var factura = r.message;
						var unido = factura.estab+''+factura.ptoemi+''+factura.secuencial
						console.log(unido)
						frm.set_value("numdocsustento", unido);
						frm.refresh_field("numdocsustento");

						updateGridDataProductos(factura);
						/* if (client.email_id != undefined && client.email_id !=""){
							updateGridDataProductos(client.email_id);
						}else{
							console.log("Cliente no tiene Email")
						} */
					}
				}
			});
		}
		
	}

	
});







function updateGridData(email) {
	cur_frm.clear_table("infoadicional");
	var new_email = cur_frm.add_child("infoadicional");
	frappe.model.set_value(new_email.doctype, new_email.name, "descripcion", "Email");
	frappe.model.set_value(new_email.doctype, new_email.name, "valor", email);
	cur_frm.refresh_field("infoadicional");
}

function updateGridDataProductos(factura) {
	cur_frm.clear_table("detalle_remision");
	$.each(factura.items, function (i, row) {
		console.log( row)
		var new_email = cur_frm.add_child("detalle_remision");
		frappe.model.set_value(new_email.doctype, new_email.name, "producto", row.item_code);
		frappe.model.set_value(new_email.doctype, new_email.name, "nproducto", row.item_name);
		frappe.model.set_value(new_email.doctype, new_email.name, "cantidad", row.qty);
		cur_frm.refresh_field("detalle_remision");
	})

	
	
}
