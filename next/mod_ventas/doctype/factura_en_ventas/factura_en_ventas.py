# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import next.next.ecutils as ec
import random
import datetime
from next.utilidades.utils import *
from next.sequencias import getSecuencia

logger = frappe.logger("patch", allow_site=True, file_count=50)
 

class FacturaenVentas(Document): 
    
    @frappe.whitelist()
    def getIvaTarifa(self, inproducto):
        if inproducto:
         getIvaTarifa(inproducto)
    
    @frappe.whitelist()
    def getIvaActual(self):
        return getIvaActual() 
    
    @frappe.whitelist()
    def stock_por_producto(self, producto):
        sql = """ select almacen , cantidad  from tabproducto_almacen  where  parent ='{0}'   """.format(producto)
        return frappe.db.sql(sql,as_dict=True)

    @frappe.whitelist()
    def buscar_por_codigobarras(self ,codigo):         
          return buscarporcodigobarras(self,codigo)

    @frappe.whitelist()
    def generarxml(self ):         
          ec.genenarXmlFacturas(self.clavedeacceso)
    
    @frappe.whitelist()
    def generarGuiaRemision(self,transportista):
        return generar_guia(self,transportista)

    @frappe.whitelist()
    def generarNotaCredito(self, tipo,valor):
        docfac = frappe.get_doc("Factura en Ventas",self.name)
        existe = frappe.db.exists("notasCredito",{"facturaventa":self.name})
        if existe:
            return existe
        else:
            notac = frappe.new_doc("notasCredito")
            notac.fechaemision = docfac.fechaemision
            notac.tipo = tipo
            notac.ptoemi = docfac.ptoemi
            notac.cliente = docfac.cliente
            notac.facturaventa = docfac.name
            notac.estab = docfac.estab      
            notac.razonmodificacion = tipo
            
            notac.sri_establecimiento = docfac.sri_establecimiento
            generarnumNC(notac)      
            notac.totaldescuento =0.0
          
            notac.numdocmodificado = docfac.sri_establecimiento + "-"+ docfac.ptoemi+"-"+ str(docfac.secuencial).zfill(9)
            femix = (docfac.clavedeacceso[0:-47] + '/' + docfac.clavedeacceso[2:-45] + '/' + docfac.clavedeacceso[4:-41])
            notac.fechaemisiondocsustento = femix
            #notac.detalles_notascredito= docfac.
            #notac.infoadicional   
            #notac.estado
            for inf in docfac.infoadicional:
                notac.append('infoadicional',{
                "nombre":inf.nombre,
                "valor": inf.valor
                })

            if "Descuentos en Ventas" == tipo:
                pro_des= frappe.db.get_single_value("configuraciones_generales","producto_desc")
                if not pro_des:                
                    frappe.throw("Por favor seleccione  un Producto como servcio  para 'Descuento en ventas' ")
                else:
                    notac.esdescuento = 1
                    notac.totalsinimpuestos = docfac.totalsinimpuestos
                    notac.iva_total= 0
                    notac.importetotal= valor
                    notac.noobjetoiva= 0
                    notac.subtotal0= valor
                    notac.saldo=  valor  
                    prod_descuento = frappe.get_doc("Productos",pro_des)
                    ivac = frappe.get_doc("Impuestos Venta", prod_descuento.impuesto)
                    notac.append('detalles_notascredito', {
                        "producto": prod_descuento.name  ,
                    "nproducto": prod_descuento.nombre  ,
                    "codigoprincipal": prod_descuento.codigo  ,
                    "cantidad": 1  ,
                    "preciounitario":valor ,
                    "desporcentaje":0  ,
                    "descuento": 0 ,
                    "preciototalsinimpuesto": valor,
                    "totallinea": valor  ,
                    "impuesto_venta": prod_descuento.impuesto  ,
                    "sri_iva": ivac.sri_iva  ,
                    "iva_valor": 0  ,
                    "tarifa_iva": ivac.tarifa  ,
                    "esservicio": prod_descuento.esservicio  
                    })
            else:
                notac.esdescuento = 0
                notac.totalsinimpuestos = docfac.totalsinimpuestos
                notac.iva_total= docfac.iva_total
                notac.importetotal= docfac.importetotal
                notac.noobjetoiva= docfac.noobjetoiva
                notac.subtotal0= docfac.subtotal0
                notac.saldo=  docfac.saldo    

                for pro in docfac.detalles_facventas:
                    notac.append('detalles_notascredito', {
                        "producto": pro.producto  ,
                    "nproducto": pro.nproducto  ,
                    "codigoprincipal": pro.codigoprincipal  ,
                    "cantidad": pro.cantidad  ,
                    "preciounitario": pro.preciounitario  ,
                    "desporcentaje": pro.desporcentaje  ,
                    "descuento": pro.descuento  ,
                    "preciototalsinimpuesto": pro.preciototalsinimpuesto  ,
                    "totallinea": pro.totallinea  ,
                    "impuesto_venta": pro.impuesto_venta  ,
                    "sri_iva": pro.sri_iva  ,
                    "iva_valor": pro.iva_valor  ,
                    "tarifa_iva": pro.tarifa_iva  ,
                    "esservicio": pro.esservicio  ,
                    })
    

            notac.insert()
        
            return notac.name
          


    
    @frappe.whitelist()
    def getPuntoEmision(self,almacen):
        return getPuntoEmision(almacen)

    @frappe.whitelist()
    def getDatosCliente(self,name):        
        if name:           
            return frappe.get_doc("Clientes",name)

    def validate(self):
        self.iva_actual = getIvaActual()
        cnfg_inv_negativo = frappe.db.get_single_value("configuraciones_generales","cnfg_inv_negativo")
        if cnfg_inv_negativo == 0:
            ComprobarStock(self)
        
        #comprobar si es artesano
        emisor = frappe.get_doc("Emisor",self.emisor)
        if emisor.extra:
            contiene=False
            for inf in self.infoadicional:
                if inf.nombre == 'Calif. Art':
                    contiene = True
            if contiene == False:
                self.append("infoadicional",{"nombre":"Calif. Art", "valor":emisor.extra})


                 

    def on_update(self):        
        if self.docstatus==1:                      
            RegistarSalidaProductos(self)
            if self.eslectronica == 1:                 
                clavedeacceso =  generarnumfac (self)
                ec.genenarXmlFacturas(clavedeacceso)   
                
            
            registarCobro(self)
            actulizarSaldoCliente(self)
 

    def before_insert (doc):
        if doc.eslectronica == 1:
            if not doc.ptoemi  :
                doc.ptoemi = getPuntoEmision(doc.estab)  

        doc.saldo=doc.importetotal
        setEmisor(doc)


        
        
@frappe.whitelist()     
def get_productos( doctype, txt, searchfield, start, page_len, filters):
    cond = ''
    if  txt:
        cond = " and p.nombre   LIKE '%{0}%'".format(txt) 
        
           
    sql = """ 	select p.name,  nombre, prod_medidas  ,pa.cantidad 
            from tabProductos p 
            inner join tabproducto_almacen pa on  ( p.name = pa.parent  )
            LEFT join tabfrmproductos_precios lp on ( lp.productos = p.name  )
           where lp.lista_de_precios = '{0}'   and  pa.almacen = '{1}'   {2}        
			union all         
          select p.name, p.nombre, prod_medidas  , 0
            from tabProductos p        
            inner join tabfrmproductos_precios lp on ( lp.productos = p.name  )
           WHERE  lp.lista_de_precios = '{0}'
            AND  p.esservicio = 1
                {2} 
            order by 1 """.format(filters.get('listaprecio'),filters.get('almacen'),cond)
    
    return frappe.db.sql(sql)

@frappe.whitelist()
def getIvaTarifa (inproducto):
    if inproducto:
        obj_tarifa = frappe.db.sql(""" SELECT tarifa FROM  `tabImpuestos Venta` where nombre = (SELECT impuesto FROM  tabProductos where name =%s) """,inproducto)
        return obj_tarifa[0][0]

@frappe.whitelist()
def getPuntoEmision( almacen):
        puntoemi =  frappe.db.sql(""" SELECT puntoemision FROM  tabAlmacenes where name=%s""",almacen)[0][0]
        return puntoemi



@frappe.whitelist()
def getPrecioEiva( inproducto,listaPrecio , inalmacen ):   
        cantidad =0.0
        sql=""" select precio_siniva  , ivaporcentaje  from tabfrmproductos_precios  p 
            where p.productos ='{0}' and p.lista_de_precios ='{1}'  """.format(inproducto,listaPrecio)
        obj_precio_lista = frappe.db.sql(sql,as_dict=1)

        prod = frappe.get_doc("Productos",inproducto)
        largo = prod.prod_largo or " "
        ancho  = prod.prod_ancho or " "
        espesor  = prod.prod_espesor or " "
        nombrepod = prod.nombre + " "+ espesor + " "+  ancho + " "+  largo
        nombrepod = prod.nombre
        if prod.esservicio == 0:
            cantidad = 99999
        else:
            sqlCant = """  select cantidad  from tabproducto_almacen pa 
            where pa.almacen ='{0}' and parent='{1}'  """.format(inalmacen,inproducto)
            obj_cantidad = frappe.db.sql(sqlCant,as_dict=1)
            if obj_cantidad:
                cantidad = obj_cantidad[0].cantidad
                

        if obj_precio_lista:
            precio_sin_iva = float(obj_precio_lista[0].precio_siniva)
            
        else:
            precio_sin_iva = 0
          
        
        impven = frappe.get_doc("Impuestos Venta",prod.impuesto)

        arr = [ precio_sin_iva , impven.tarifa, cantidad  ,impven.sri_iva  , nombrepod ]
        return arr


def generarnumfac(self):
        random.seed()
        obj_ambiente = frappe.db.sql("""select tipo_ambiente from tabEmisor """)
        if not obj_ambiente:
            frappe.throw("EL Ambiente no se ha establecido en el Emisor")

        sri_ambiente = obj_ambiente[0][0]

        obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)

        if not obj_ruc:
            frappe.throw("EL ruc no se ha establecido en el Emisor")
        sri_ruc = obj_ruc[0][0]

        if len(sri_ruc ) < 13 :
            frappe.throw("El ruc para la firma electrónica no tiene 13 dígitos, Por favor diríjase al módulo de facturación,en la sección SRI y emisor para configurarlo correctamente")


        obj_estab = frappe.db.sql("""SELECT establecimiento FROM  tabAlmacenes where name = %s """,self.estab)

        if not obj_estab:
            frappe.throw("EL establecimiento no se ha establecido en el Almacen")
        sri_establecimiento = obj_estab[0][0]


        if str( sri_ambiente ).strip() == 'Pruebas':
            sri_ambiente = "1"
        else:
            sri_ambiente = "2"

        tipoComprobante='01'  
     
        numerofac =  getSecuencia("FA",sri_establecimiento,self.ptoemi)
      
        numfac_completo =  sri_establecimiento + self.ptoemi +'-'+numerofac
        serie =  sri_establecimiento + self.ptoemi
      
        #doc.num_fac = numfac_completo
        
         
        #doc.sri_ambiente = sri_ambiente


        claveacceso = ec.generarClaveAcesso( self.fechaemision,tipoComprobante,sri_ruc,sri_ambiente,serie,numerofac)
        #doc.barcode_ca = ec.get_svg_from_text_code128(claveacceso)
        #doc.clavedeacceso=claveacceso

        sqlu = """ update  `tabFactura en Ventas`  set  clavedeacceso = '{0}' , barcode_ca='{1}' ,
        sri_ambiente='{2}',sri_establecimiento='{3}' , secuencial ={4} , num_fac='{5}' , estado='GENERADO'
         where name = '{6}' """.format(claveacceso, ec.get_svg_from_text_code128(claveacceso), 
        sri_ambiente,sri_establecimiento,numerofac,numfac_completo,self.name     )    

        frappe.db.sql(sqlu)
        #logger.info(f"{user} accessed counter_app.update with value={value}")
     
        return  claveacceso

        #ruta = os.path.dirname(__file__)
        #ruta = frappe.local.site
        # ruta = os.path.abspath(frappe.get_site_path("public", "files"))
        #frappe.throw(ruta)

def generarnumfacNoElectronica(doc):

        obj_estab = frappe.db.sql("""SELECT establecimiento FROM  tabAlmacenes where name = %s """,doc.estab)

        if not obj_estab:
            frappe.throw("EL establecimiento no se ha establecido en el Almacen")
        doc.sri_establecimiento = obj_estab[0][0]
        documento_inicial='O-'
        sqlnumfac="SELECT case when max(secuencial2) is null then 1 else max(secuencial2) + 1 end    FROM `tabFactura en Ventas` where  sri_establecimiento='{0}' and ptoemi='{1}' ".format( doc.sri_establecimiento,doc.ptoemi)
         
        objnumerofac =  frappe.db.sql(sqlnumfac)
        numerofac = str (int(objnumerofac[0][0]))     

        doc.secuencial2= numerofac
        numerofac = numerofac.zfill(9)
 
        numfac_completo = doc.sri_establecimiento + doc.ptoemi +'-'+numerofac
        serie = doc.sri_establecimiento + doc.ptoemi
      
        doc.num_fac =documento_inicial+numfac_completo
       
   


def registarCobro(indoc):
    
    if indoc.pagoefectivo == 1:
        doc = frappe.get_doc("Factura en Ventas",indoc.name)

        rc = frappe.new_doc("Registro de Cobros")
        rc.tipo = "FACTURA"
        rc.fecha = datetime.datetime.now()
        rc.descripcion ="Pago en EFECTIVO de la factura: " + doc.num_fac
        rc.clientes = doc.cliente        
        rc.append("detalle_pagos", {"tipo":"EFECTIVO" ,"cantidad":doc.importetotal  })
        rc.append("detallepago_fac_ventas", {"factura":doc.name ,
            "totalfactura":doc.importetotal ,
            "cantidad":doc.importetotal })
      
        rc.insert()
        rc.docstatus = 1
        rc.save()
        indoc.saldo = 0
       

       


def RegistarSalidaProductos(doc):
    if doc.bajarstock == 1:
        for pro in doc.detalles_facventas:
            if pro.esservicio == 0:
                insert_kardex( "VENTA", doc.estab,pro.producto,pro.preciounitario, pro.cantidad,doc.name )
 

def ComprobarStock(doc):
    msg=""
    
    for pro in doc.detalles_facventas:
        prodc = frappe.get_doc("Productos", pro.producto)        
        if  prodc.esservicio == 0:
            existe = False
            for pa in prodc.producto_almacen:
                if pa.almacen == doc.estab:
                    existe = True
                    if pa.cantidad < pro.cantidad:
                        msg = "El stock del producto: " + pro.producto + " es insuficiente, " + "Existen: " + str(pa.cantidad) + " Solicitados: " + str(pro.cantidad)
                        frappe.throw(msg)
        
            if not existe  :
                frappe.throw("El producto: " + pro.producto + " no tiene un stock en el almacen: " + doc.estab)

    
 

def generarnumNC(doc):
        random.seed()
        obj_ambiente = frappe.db.sql("""select tipo_ambiente from tabEmisor """)
        if not obj_ambiente:
            frappe.throw("EL Ambiente no se ha establecido en el Emisor")

        sri_ambiente = obj_ambiente[0][0]

        obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)

        if not obj_ruc:
            frappe.throw("EL ruc no se ha establecido en el Emisor")
        sri_ruc = obj_ruc[0][0]

        obj_estab = frappe.db.sql("""SELECT establecimiento FROM  tabAlmacenes where name = %s """,doc.estab)

        if not obj_estab:
            frappe.throw("EL establecimiento no se ha establecido en el Almacen")
        doc.sri_establecimiento = obj_estab[0][0]


        if str( sri_ambiente ).strip() == 'Pruebas':
            sri_ambiente = "1"
        else:
            sri_ambiente = "2"
        tipoComprobante='04'  
        numerofac = getSecuencia("NC",doc.sri_establecimiento,doc.ptoemi)  

        doc.secuencial= int(numerofac)
    
        numfac_completo = doc.sri_establecimiento +doc.ptoemi+'-'+numerofac
        serie = doc.sri_establecimiento + doc.ptoemi
      
        doc.num_nota = numfac_completo
         
        doc.sri_establecimiento = doc.sri_establecimiento
        doc.sri_ambiente = sri_ambiente


        claveacceso = ec.generarClaveAcesso( str(doc.fechaemision),tipoComprobante,sri_ruc,sri_ambiente,serie,numerofac)
        doc.clavedeacceso=claveacceso
        doc.barcode_ca = ec.get_svg_from_text_code128(claveacceso)
        
      
        #ruta = os.path.dirname(__file__)
        #ruta = frappe.local.site
        # ruta = os.path.abspath(frappe.get_site_path("public", "files"))
        #frappe.throw(ruta)


 



def buscarporcodigobarras (self,codigo):
    existe = frappe.db.exists("Productos", {"codigo":codigo})
    if not existe:
        existe = frappe.db.exists("Productos", {"codbarra":codigo})
        if not existe:
            return {"estado":"error", "mensaje":"No existe un producto con el codigo de barras:" +codigo }
        
        else:
            producto = frappe.get_doc("Productos", {"codbarra":codigo} )
    else:
        producto = frappe.get_doc("Productos", {"codigo":codigo} )
    
    
    inproducto = producto.name
    listaPrecio = self.listaprecios 
    inalmacen = self.estab
    datosprod = getPrecioEiva( inproducto,listaPrecio , inalmacen )

    return {"estado":"ok", "mensaje": {"producto" : producto, "otros": datosprod }  }

def generar_guia(self,transportista):
    docfac = frappe.get_doc("Factura en Ventas",self.name)
    existe = frappe.db.exists("guia_remision",{"factura":self.name})
    if existe:
        return existe
    else:
        cliente = frappe.get_doc("Clientes",self.cliente)

        obj_ruc = frappe.db.sql("""select ruc from tabEmisor """)

        if not obj_ruc:
            frappe.throw("EL ruc no se ha establecido en el Emisor")
        sri_ruc = obj_ruc[0][0]

        emisor =  frappe.get_doc("Emisor",{"ruc":sri_ruc}) 
        guia = frappe.new_doc("guia_remision")

        guia = frappe.new_doc("guia_remision")
        
        objtrans = frappe.get_doc("transportistas",transportista)
        guia.transportistas = transportista 
        guia.placa =   "_"
        guia.cliente = self.cliente
        guia.estab =  self.estab
        guia.fecha =   datetime.datetime.now()
        guia.fechainitransporte=  datetime.datetime.now()
        guia.dirpartida= emisor.direccion_establecimiento 
        guia.motivotraslado= "TRANSPORTE DE MERCADERIA"
        guia.ruta= emisor.direccion_establecimiento  +  "   a " +  cliente.direccion 
        guia.fechafintransporte =    datetime.datetime.now()
        guia.direccion_del_destinatario = cliente.direccion 
        guia.cod_estab_destinoopcional =   ""
        guia.factura =  self.name
        estemi = self.num_fac.split("-")[0]

        guia.numdocsustento =  estemi[0:3]+"-"+ estemi[0:3] +"-" +self.num_fac.split("-")[1]
        guia.numero_de_autorizacion =   self.clavedeacceso
        guia.fecha_de_emision =   self.fechaemision
        guia.documento_aduanero =   ""

        for row in docfac.detalles_facventas:
            prod = frappe.get_doc("Productos",row.producto)
           
            guia.append("detalle_remision", {"producto":row.producto, "cantidad":row.cantidad , "nproducto":prod.nombre} ) 
        
        for row in docfac.infoadicional:
            guia.append("infoadicional", {"nombre":row.nombre, "valor":row.valor } ) 
      

        guia.insert()
        return guia.name 

 
def getIvaActual( ):
      iva_actual = frappe.db.get_single_value("configuraciones_generales","iva_actual") 
      if iva_actual:
          dociva = frappe.get_doc("Impuestos Venta",iva_actual)
          return  str(dociva.tarifa)
      else:
          return "12" 

def actulizarSaldoCliente(self):
    cliente = frappe.get_doc("Clientes", self.cliente)
    saldo = cliente.saldo 		
    
    saldoactual =  saldo  +  self.importetotal 
    frappe.db.set_value("Clientes", self.cliente, "saldo", saldoactual )