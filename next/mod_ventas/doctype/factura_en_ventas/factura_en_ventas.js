// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt
var frmVentas;
var esfisica=false;
frappe.ui.form.on('Factura en Ventas', {
  bcdb: function (frm) {

    buscar_por_codigobarras();
  },
  onload: function (frm) {
    usa_facelec();
   

    /* frm.set_query("producto", "detalles_facventas", function (doc, cdt, cdn) {
       return {
         filters: {
           deshabilitado: 0
         }
       }
     }); */

    frm.set_query('producto', 'detalles_facventas', function (doc, cdt, cdn) {
      return {
        query: 'next.mod_ventas.doctype.factura_en_ventas.factura_en_ventas.get_productos',
        filters: {
          listaprecio: frm.doc.listaprecios,
          almacen: frm.doc.estab
        }
      }
    })
  },
  detalles_facventas_on_form_rendered: function (frm, doc, cdt, cdn) {
    //	var grid_row1 = cur_frm.fields_dict['detalles_facventas'].grid.get_row(0)

    // $('div[data-fieldname="sublimite_dentro"]').css("color", "red")
    // $('div[data-fieldname="sublimite_dentro"]').css("cursor", "pointer")

    //  $(frm.fields_dict['age_html'].wrapper).html(`${__('AGE')} : ${get_age(frm.doc.dob)}`)
    stock_por_producto(frm, doc, cdt, cdn)
  },

  refresh: function (frm) {
    generarNotaCredito();
    generarNotaCreditoDescuento();
    generarGuiaRemision();

    if (!frm.is_new()) {


   
    cur_frm.add_custom_button(__('VER RIDE'), function () {
      window.open(
        frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
          + "doctype=" + encodeURIComponent("Factura en Ventas")
          + "&name=" + encodeURIComponent(cur_frm.doc.name)
          + "&format=" + encodeURIComponent('RIDE-FACTURA')
          + "&no_letterhead=0"));
    }).css({ 'background-color': '#FCC60B', 'font-weight': 'bold', 'color': 'black' });

    cur_frm.add_custom_button(__('VER RECIVO'), function () {
      window.open(
        frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
          + "doctype=" + encodeURIComponent("Factura en Ventas")
          + "&name=" + encodeURIComponent(cur_frm.doc.name)
          + "&format=" + encodeURIComponent('FACTURA-RECIVO')
          + "&no_letterhead=0"));
    }).css({ 'background-color': '#FCC60B', 'font-weight': 'bold', 'color': 'black' });
  }

  },
  validate(frm) {
    frm.barcode_ca = frm.clavedeacceso
  },
  estab: function (frm) {
    getPuntosEmision(frm)
  },
  cliente(frm) {
    getDatosCliente(frm)
  },

  ride: function (frm) {
    if (frm.doc.estado == 'AUTORIZADO') {
      openInNewTab(frm.doc.clavedeacceso)
    } else {
      frappe.throw('El documento no esta Autorizado')
    }
  },
  generarxml: function (frm) {
    if (frm.doc.estado != 'AUTORIZADO') {
      if (frm.doc.docstatus == 0) {
        frappe.throw(__('VALIDE LA FACTURA'))
      } else {
        frappe.call({
          doc: frm.doc,
          method: 'generarxml',
          freeze: true,
          args: { clavedeacceso: frm.doc.clavedeacceso },
          callback: r => {
            frappe.show_alert({
              indicator: 'green',
              message: __('XML Generado...')
            })
          }
        })
      }
    } else {
      frappe.throw(__('LA FACTURA NO CONTIENE ERRORES'))
    }
  }
})
function openInNewTab(url) {
  url = '/files/docelec/' + url + '.pdf'
  var win = window.open(url, '_blank')
  win.focus()
}
frappe.ui.form.on('detalles_facventas', {
  onload: function(frm, cdt, cdn) {
     alert('sss');
  },
  cantidad: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },
  preciounitario: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  desporcentaje: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)

    calcular_valores()
  },

  producto: function (frm, cdt, cdn) {
    const doc = frappe.get_doc(cdt, cdn)
    if (doc !== undefined && doc !== null) {
      getPrecioEiva(frm, doc, cdt, cdn)
      // stock_por_producto(frm, doc, cdt, cdn)
    }
  },
  detalles_facventas_remove: (frm) => {

    calcular_valores()
  }

})

function getPrecioEiva(frm, doc, cdt, cdn) {
  if (cur_frm.doc.listaprecios === undefined) {
    frappe.msgprint('Seleccione una lista de precios')
    return
  }
  if (doc.producto !== undefined && doc.producto !== null) {
    frappe.call({
      method: 'next.mod_ventas.doctype.factura_en_ventas.factura_en_ventas.getPrecioEiva',
      freeze: true,
      args: {
        inproducto: doc.producto,
        listaPrecio: cur_frm.doc.listaprecios,
        inalmacen: cur_frm.doc.estab
      },
      callback: r => {
     
        frappe.model.set_value(cdt, cdn, 'preciounitario', r.message[0]);
        frappe.model.set_value(cdt, cdn, 'tarifa_iva', r.message[1]);
        frappe.model.set_value(cdt, cdn, 'sri_iva', r.message[3]);
        frappe.model.set_value(cdt, cdn, 'nproducto', r.message[4]);
        cur_frm.refresh_fields();
        calcular_valores();
      }
    })
  }
}

function stock_por_producto(frm, doc, cdt, cdn) {
  if (cur_frm.doc.listaprecios === undefined) {
    frappe.msgprint('Seleccione una lista de precios')
    return
  }
  if (doc.producto !== undefined && doc.producto !== null) {
    frappe.call({
      method: 'stock_por_producto',
      doc: frm.doc,
      freeze: true,
      args: {
        producto: doc.producto
      },
      callback: r => {
        let html = `<table border=1 style = 'width:100%'>  <tr>   <td> ALMACEN </td>   <td>  CANTIDAD  </td>  </tr>     `
        $.each(r.message, function (i, row) {
          html += `  <tr>   <td>  ${row.almacen}  </td>   <td>  ${row.cantidad}  </td>  </tr>  `
        })
        html += `  </table>`
      
        $(frm.fields_dict.detalles_facventas.wrapper).find('[data-fieldname=html_stock]').html(html)
        cur_frm.refresh_fields()
      }
    })
  }
}

function calcular_valores() {
  var totalDescuentos = 0;
  var subtotal_iva12 = 0;
  var subtotal_iva0 = 0;
  var subtotal_nosujiva = 0;

  var iva_total = 0;

  $.each(cur_frm.doc.detalles_facventas, function (i, row) {
    // calcula descuento
    let valordescuento = 0;

    if (row.desporcentaje > 0)
      valordescuento = (row.preciounitario * row.desporcentaje) / 100;

    row.descuento = valordescuento

    // let precio_calc = roundNumber((row.preciounitario - valordescuento) * row.cantidad, 2);
    let precio_calc = (row.preciounitario - valordescuento) * row.cantidad;

    if (row.sri_iva == "6") //No Objeto de Impuesto
    {
      subtotal_nosujiva += precio_calc;
      row.iva_valor = 0;
    }
    else
      if (row.sri_iva == "0") //0%
      {
        subtotal_iva0 += precio_calc;
        row.iva_valor = 0;
      }
      else // 12%  14%  15%
      {

        let iva_valor = (precio_calc * row.tarifa_iva) / 100;



        row.iva_valor = iva_valor;

        subtotal_iva12 += precio_calc;

      }
    precision('iva_valor', row);
    precision('preciototalsinimpuesto', row);
    precision('totallinea', row);
    row.preciototalsinimpuesto = precio_calc;
    row.totallinea = roundNumber(precio_calc + row.iva_valor, 2);
    totalDescuentos += row.descuento;
    iva_total += row.iva_valor;
  })
  precision('totaldescuento', cur_frm.doc);
  precision('totalsinimpuestos', cur_frm.doc);
  precision('subtotal0', cur_frm.doc);
  precision('noobjetoiva', cur_frm.doc);
  precision('iva_total', cur_frm.doc);
  precision('importetotal', cur_frm.doc);
  precision('saldo', cur_frm.doc);
  cur_frm.doc.totaldescuento = totalDescuentos;
  cur_frm.doc.totalsinimpuestos = subtotal_iva12;
  cur_frm.doc.subtotal0 = subtotal_iva0;
  cur_frm.doc.noobjetoiva = subtotal_nosujiva;
  cur_frm.doc.iva_total = iva_total;
  // cur_frm.doc.importetotal = roundNumber(subtotal_iva12 + subtotal_iva0 + subtotal_nosujiva + iva_total, 2);
  cur_frm.doc.importetotal = subtotal_iva12 + subtotal_iva0 + subtotal_nosujiva + iva_total;
  cur_frm.doc.saldo = cur_frm.doc.importetotal;

  cur_frm.refresh_fields();
}

function getPuntosEmision(frm) {
  frappe.call({
    doc: frm.doc,
    method: 'getPuntoEmision',
    args: { almacen: frm.get_field('estab').value },
    callback: r => {
      frm.doc.ptoemi = r.message
      frm.refresh_fields()
    }
  })
}

function getDatosCliente(frm) {
  if (frm.doc.cliente != undefined)
    frappe.call({
      doc: frm.doc,
      method: 'getDatosCliente',
      args: { name: frm.doc.cliente },
      callback: r => {
     
        frm.clear_table('infoadicional')
        frm.add_child('infoadicional', { nombre: 'Correo', valor: r.message.correo })
      }
    })
}

function usa_facelec() {
  frappe.call({
    'method': 'next.utilidades.utils.usa_facelec',
    freeze: true,
    callback: function (r) {
      cur_frm.toggle_display(['formapago', 'infoadicional', 'sec_sri'], r.message == 1)
      // cur_frm.toggle_display(['num_fac'   ],r.message == 1)
      cur_frm.toggle_reqd('num_fac', r.message == 0);
      cur_frm.set_df_property('num_fac', 'read_only', r.message == 1);
      esfisica =  r.message == 0;
      cur_frm.doc.eslectronica = r.message;
      usa_genera_secuencial();
    }
  })
}

function usa_genera_secuencial() {
  frappe.call({
    'method': 'next.utilidades.utils.usa_genera_secuencial',
    freeze: true,
    callback: function (r) {
      console.log(r.message );
      if( r.message > 0 ){
        cur_frm.doc.num_fac = r.message;
        cur_frm.refresh_field('num_fac');
      }
    
 
    }
  })
}


function generarNotaCredito() {
  // if (cur_frm.doc.docstatus == 1 && cur_frm.doc.estado == 'AUTORIZADO') {
  cur_frm.page.add_menu_item('ANULAR FACTURA', function () {
    frappe.confirm('¿Esta seguro de ANULAR esta factura, se generar una nota de credito?',
      () => {
        frappe.call({
          doc: cur_frm.doc,
          method: 'generarNotaCredito',
          args: {
            //  valor:  data.valor,
            tipo: 'Devoluciones',
            valor: 0
          },
          callback: r => {
        
            frappe.show_alert({
              indicator: 'green',
              message: r.message
            })

            frappe.set_route('Form', 'notasCredito', r.message)
          }
        })
      }, () => {
      })
  })
  // }
}

function generarGuiaRemision() {
  // if (cur_frm.doc.docstatus == 1 && cur_frm.doc.estado == 'AUTORIZADO') {
  cur_frm.page.add_menu_item('GENERAR GUIA DE REMISIÓN', function () {



    let d = new frappe.ui.Dialog({
      title: '¿Esta seguro de generar la Guia de Remisión?',
      fields: [
          {
              label: 'Transportista',
              fieldname: 'transportista',
              fieldtype: 'Link',
              options: "transportistas",
              reqd: 1
          } 
      ],
      size: 'large', // small, large, extra-large 
      primary_action_label: 'Crear',
      primary_action(values) {
        console.log(values);
   
        frappe.call({
          doc: cur_frm.doc,
          method: 'generarGuiaRemision',
          args: {
            transportista: values.transportista
          },
          callback: r => {
        
            frappe.show_alert({
              indicator: 'green',
              message: r.message
            })

            frappe.set_route('Form', 'guia_remision', r.message)
          }
        })
          d.hide();
      }
  });
  
  d.show();



   
  })
  // }
}


function generarNotaCreditoDescuento() {
  // if (cur_frm.doc.docstatus == 1 && cur_frm.doc.estado == 'AUTORIZADO') {
  cur_frm.page.add_menu_item('GENERAR NC POR DESCUENTO', function () {
    frappe.confirm('¿Esta seguro generar una Nota de credito por descuento?',
      () => {

        frappe.prompt({
          label: 'Valor del Descuento',
          fieldname: 'valor',
          fieldtype: 'Currency',
          requiered: 1
        }, (values) => {
         

          if (values.valor > cur_frm.doc.saldo) {
            frappe.msgprint("EL valor de descuento debe ser menor al valor del saldo de la factura");
            return;
          }

          frappe.call({
            doc: cur_frm.doc,
            method: 'generarNotaCredito',
            args: {
              //  valor:  data.valor,
              tipo: 'Descuentos en Ventas',
              valor: values.valor
            },
            callback: r => {
        
              frappe.show_alert({
                indicator: 'green',
                message: r.message
              })

              frappe.set_route('Form', 'notasCredito', r.message)
            }
          })

        })




      }, () => {
      });

  })
  // }
}

function buscar_por_codigobarras() {

  if (!cur_frm.doc.bcdb) {

    return;
  }
  frappe.call({
    doc: cur_frm.doc,
    method: 'buscar_por_codigobarras',
    args: { codigo: cur_frm.doc.bcdb },

    callback: r => {
      
      let dato = r.message;
      if (dato.estado == "error") { 
        frappe.show_alert(dato.mensaje, 5);
        cur_frm.doc.bcdb = "";
        cur_frm.refresh_field("bcdb");

      }
      else {
        let producto = dato.mensaje.producto;
        let otrainfo = dato.mensaje.otros;

        let existe_en_listado = false;
        //buscar
        $.each(cur_frm.doc.detalles_facventas, function (i, row) {

          if (row.producto == producto.name) {
            existe_en_listado = true;
            row.cantidad = row.cantidad + 1;
          }
        });

        if (!existe_en_listado) {

          cur_frm.add_child('detalles_facventas', {
            producto: producto.name,
            nproducto: producto.nombre,
            codigoprincipal: producto.codigo,
            cantidad: 1,
            preciounitario: otrainfo[0],
            tarifa_iva: otrainfo[1],
            sri_iva: otrainfo[3],
            impuesto_venta: producto.impuesto
          });

        }

        cur_frm.refresh_field("detalles_facventas");
        calcular_valores();
        cur_frm.doc.bcdb = "";
        cur_frm.refresh_field("bcdb");
      }

    }
  })


}

function getIvaActual(){
	
  frappe.call({
		method: "getIvaActual",
		doc: frm.doc,
		freeze: true,
		freeze_message: "...",
		callback: (r) => {
		  frappe.msgprint(r.message);
	
		},
	  });

}