// Copyright (c) 2023, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('transportistas', {
	// refresh: function(frm) {

	// }
	validate: function (frm) {
        if (frm.doc.tipoidentificaciontransportista == "RUC-04" && (frm.doc.identificaciontransportista).length != 13 ){
            frappe.throw("Identificación no coicide con tipo de identificación");
        }
        if (frm.doc.tipoidentificaciontransportista == "CEDULA-05" && (frm.doc.identificaciontransportista).length != 10 ){
            frappe.throw("Identificación no coicide con tipo de identificación");
        }
    },
});
