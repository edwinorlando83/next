# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class cuentasxcobrar(Document):
	def on_update(self):
		if self.docstatus == 1:
				cliente_deudas = frappe.new_doc("cliente_deudas")
				cliente_deudas.cliente = self.cliente
				cliente_deudas.cuota_numero = 1
				cliente_deudas.cuota_valor =  self.valor
				cliente_deudas.fecha = self.fecha			
				cliente_deudas.saldo = self.valor
				cliente_deudas.cuentasxcobrar = self.name
				cliente_deudas.descripcion = "CXC Inicial: " + self.numero_doc + " " + str(self.fecha) 
				cliente_deudas.insert()
				actulizarSaldoCliente(self)


def actulizarSaldoCliente(self):
    cliente = frappe.get_doc("Clientes", self.cliente)
    saldo = cliente.saldo 		
    saldoactual =  saldo  +  self.saldo 
    frappe.db.set_value("Clientes", self.clientes, "saldo", saldoactual )