# -*- coding: utf-8 -*-
# Copyright (c) 2020, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Sanciones(Document):
		def before_save(self):
			listaprecio = frappe.db.get_single_value("Parametros Agua","listaprecio")
			precio = frappe.get_doc("listaprecios_productos",{'productos':self.multa,'parent':listaprecio})
			self.valor =  precio.precio
			medidor = frappe.get_doc("Asignar Medidor de Agua",{'usuario':self.usuario})
			self.medidor =  medidor.medidor
			
