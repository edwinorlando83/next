# -*- coding: utf-8 -*-
# Copyright (c) 2021, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import sqlite3
import datetime 
class RegistrodeConsumodeAgua(Document):
		def on_update(self):
			if self.lectura_a > 0:
				self.consumo = self.lectura_a - self.lectura
			else:
				self.consumo = self.lectura
			
			listaprecio = frappe.db.get_single_value("Parametros Agua","listaprecio")
			productoagua = frappe.db.get_single_value("Parametros Agua","producto")
			mantenimiento = frappe.db.get_single_value("Parametros Agua","producto2")
			aguaprecio = frappe.get_doc("listaprecios_productos",{'productos': productoagua,'parent':listaprecio})
			mantenimientoprecio = frappe.get_doc("listaprecios_productos",{'productos': mantenimiento,'parent':listaprecio})
			self.valorconsumo =  aguaprecio.precio * self.consumo
			self.mantenimiento =  mantenimientoprecio.precio
			multasobj = frappe.db.sql("""  select IFNULL(  sum(valor) , 0 ) from tabSanciones ts  where medidor = %s and docstatus = 1 and facturado = 0""",self.medidor)
			if multasobj :
				self.multas = multasobj[0][0]
		
		def before_insert(self):
			lectura_antobj = frappe.db.sql(""" select IFNULL(lectura,0) from  `tabRegistro de Consumo de Agua` where medidor = %s and fecha < %s""",(self.medidor, self.fecha))
			if lectura_antobj : 
				self.lectura_a = lectura_antobj[0][0]
		
@frappe.whitelist()
def generarLecturas(anio,mes):
			lstMedidores =  frappe.get_all('Asignar Medidor de Agua',  fields=["medidor","zona","usuario"] , filters = {"activo":1})
			v_mes = 0
			if mes == 'Enero':
				v_mes = 1
			if mes == 'Febrero':
				v_mes = 2
			if mes == 'Marzo':
				v_mes = 3
			if mes == 'Abril':
				v_mes = 4
			if mes == 'Mayo':
				v_mes = 5
			if mes == 'Junio':
				v_mes = 6
			if mes == 'Julio':
				v_mes = 7
			if mes == 'Agosto':
				v_mes = 8
			if mes == 'Septiembre':
				v_mes = 9
			if mes == 'Octubre':
				v_mes = 10
			if mes == 'Noviembre':
				v_mes = 11
			if mes == 'Diciembre':
				v_mes = 12
			
			any_date = datetime.datetime(int(anio), v_mes, 1)
			next_month = any_date.replace(day=28) + datetime.timedelta(days=4)
			last_day_of_month = next_month - datetime.timedelta(days=next_month.day)
			fecha_ultimo = datetime.datetime(int(anio), v_mes, last_day_of_month.day)
			aux =0
			for row in  lstMedidores:
				rc = frappe.new_doc('Registro de Consumo de Agua')
				rc.anio = anio
				rc.medidor = row.medidor
				rc.lectura =  0
				rc.zona =  row.zona
				rc.mes = mes
				rc.fecha = fecha_ultimo
				rc.usuario =  row.usuario
				rc.insert()
				aux = aux + 1
			return "Se crearon " + str(aux) + " registros"
			


