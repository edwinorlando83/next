// Copyright (c) 2021, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.listview_settings['Registro de Consumo de Agua'] = {
 
	onload: function(listview) {
		var method = "";
 
		listview.page.add_menu_item( "Generar Registros de lectura" , function() {

			var d = new frappe.ui.Dialog({
				title: 'Seleccione los datos',
				'fields': [
					 {'fieldname': 'anio','label':'Año', 'fieldtype': 'Select', 'options':['2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029'],'reqd': 1},
					 {'fieldname': 'mes','label':'Mes', 'fieldtype': 'Select', 'options':['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],'reqd': 1},
				],
				primary_action: function(){
					generarLecturas(listview,d.get_values().anio, d.get_values().mes);
					d.hide();
					//show_alert(d.get_values());
				}
			});
		 
			d.show();

		 
		});

		 

	}
} 
function generarLecturas(frm,inanio,inmes){
 
	frappe.call({
		method: "next.jaap.doctype.registro_de_consumo_de_agua.registro_de_consumo_de_agua.generarLecturas",
	 
		args: {    anio: inanio , mes: inmes   },
		callback: (r) => {
			show_alert(r.message);    
		}
	  });
}
