# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "Lexie"
app_title = "Lexie"
app_publisher = "Ing. Orlando Cholota"
app_description = "Sistema de Inventarios"
app_icon = "octicon octicon-file-directory"
app_color = "blue"
app_email = "edwin_orlando83@hotmail.com"
app_license = "Software Propietario"

error_report_email = "edwin_orlando83@hotmail.com"
doctype_js ={"Clientes" : "public/js/utils.js",
             "Factura de Compras" : "public/js/utils.js",
             "bom_order" : ["public/js/cmulti_select_dialog.js", "public/js/utils.js"],
             "Factura Compras Gastos" : "public/js/utils.js",
             "Proveedores" : "public/js/utils.js"}  

#app_include_css = "/assets/next/css/next.css"

website_context = {
"favicon": "/assets/next/favico.png",
"splash_image": "/assets/next/logo.png"
}
#web_include_css = ["/assets/next/css/nextweb.css"]
brand_html = ' <img src="/assets/next/logo2.png"> '
app_logo_url = "/assets/next/logo.png"
 
#setup_wizard_requires = "assets/next/js/setup_wizard.js"
#setup_wizard_stages = "next.install.get_setup_stages"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/next/css/next.css"
# app_include_js = "/assets/next/js/next.js"

# include js, css files in header of web template
# web_include_css = "/assets/next/css/next.css"
# web_include_js = "/assets/next/js/next.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------


# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "next.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "next.install.before_install"
after_install = "next.install.after_install"
#boot_session = "next.install.boot_session"
# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "next.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }

#permission_query_conditions = {
#	"Almacenes": "next.filtros.filtros.getpqc",	  
#}


# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = { 
 	"hourly": [ "next.tasks.hourly"
	]
 
  }

# scheduler_events = {
# 	"all": [
# 		"next.tasks.all"
# 	],
# 	"daily": [
# 		"next.tasks.daily"
# 	],
# 	"hourly": [
# 		"next.tasks.hourly"
# 	],
# 	"weekly": [
# 		"next.tasks.weekly"
# 	]
# 	"monthly": [
# 		"next.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "next.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "next.event.get_events"
# }

