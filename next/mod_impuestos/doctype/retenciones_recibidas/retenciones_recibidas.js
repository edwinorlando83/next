// Copyright (c) 2022, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('Retenciones_recibidas', {
	refresh: function(frm) {
		if (frm.is_new() != 1) {
          
        }else{
			var fechacampo = frm.fields_dict.fecha.datepicker;
			var dateNow = frappe.datetime.nowdate();
			fechacampo.update({
				maxDate: frappe.datetime.str_to_obj(dateNow)
			});
		}

		frm.set_query("factura", function() {
            return {
                filters: [
                    ['saldo', '>', 0]
                ]
            }
        })

		
	  },
	  base_imponible_iva:function(frm){
		calcula();
	  },
	  porcentaje_iva:function(frm){
		calcula();
	  }
	  ,
	  base_imponible_renta:function(frm){
		calcula();
	  }
	  ,
	  porcentaje_renta:function(frm){
		calcula();
	  }
});


function calcula(){
	cur_frm.doc.valor_renta = cur_frm.doc.base_imponible_renta * cur_frm.doc.porcentaje_renta /100 ;
 

	cur_frm.doc.valor_iva  = cur_frm.doc.base_imponible_iva * cur_frm.doc.porcentaje_iva /100;	
	

	let vrenta= 0;
	if(  cur_frm.doc.valor_renta) 
	  vrenta = cur_frm.doc.valor_renta ;

	let viva= 0;
	if(  cur_frm.doc.valor_iva) 
	   viva = cur_frm.doc.valor_iva ;


	cur_frm.doc.total = vrenta+ viva;
 
	cur_frm.refresh_fields();
}