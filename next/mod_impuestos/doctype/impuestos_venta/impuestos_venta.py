# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ImpuestosVenta(Document):
	
	@frappe.whitelist( )
	def actualizar_a_productos(self):
		sql = """ update tabProductos set impuesto = '{0}' """.format(self.name)
		frappe.db.sql(sql)
		return "Productos actualizados"

