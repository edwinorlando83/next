// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('Impuestos Venta', {
	refresh: function(frm) {

	},
	actulizar_productos: function(frm) {
		actulizar_productos(frm);
	}
});

function actulizar_productos(frm){

    frappe.call({
		method: "actualizar_a_productos",
		doc: frm.doc,
		freeze: true,
		freeze_message: "Actualizando...",
		callback: (r) => {
		  frappe.msgprint(r.message);
	
		},
	  });
}

