// Copyright (c) 2024, Ing. Orlando Cholota and contributors
// For license information, please see license.txt
/* eslint-disable */
 

frappe.query_reports["Existencias"] = {
	"filters": [
		{
			"fieldname": "lista_precio",
			"fieldtype": "Link",
			"label": "Lista de Precios",
			"options":"Lista de Precios",
			"width": "125",
			"reqd": 1
		 
		},
		{
			"fieldname": "almacen",
			"fieldtype": "Link",
			"label": __("Almacen"),
			"options":"Almacenes",
			"width": "125",
		 
		 
		},
		
		{
			"fieldname": "producto",
			"fieldtype": "Link",
			"label": __("Producto"),
			"options":"Productos",
			"width": "125"			 
		}
		,
		{
			"fieldname": "deshabilitado",
			"fieldtype": "Select",
			"label": "Producto Deshabilitado ",
			"options":"SI\nNO",
			"width": "80"			 
		}
	]
};
