import frappe
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}
	columns = get_columns()

	advances_list = get_advances(filters)
	
	if not advances_list:
		msgprint(_("No record found"))
		return columns, advances_list

	data = []
	for advance in advances_list:
		row = []
		for c in columns:
			row.append(advance[c['fieldname']])
		data.append(row)

	return columns, data

def get_columns():

	return[
		{
			"label": "ID Producto",
			"fieldname": "name",
			"fieldtype": "Link",
			"options":"Productos",			 
			"width": 120
		},{
			"label": "Codigo",
			"fieldname": "codigo",
			"fieldtype": "Data",			 
			"width": 100
		},
		{
			"fieldname": "nombre",
			"label": "Nombre",
			"fieldtype": "Data",
			"width":200
		},
		{
			"fieldname": "almacen",
			"label": "Almacen",
			"fieldtype": "Data",
			"width":200
		},
		{
			"fieldname": "lista_de_precios",
			"label": "Lista Precio",
			"fieldtype": "Data",
			"width":160
		},
		{
			"fieldname": "cantidad",
			"label": "Cantidad",
			"fieldtype": "float",
			"width":90
		}, 
		 
			
		{
			"fieldname": "precio_siniva",
			"label": "Precio sin IVA",
			"fieldtype": "Currency",
			"width":110
		} 
			,		
		{
			"fieldname": "total",
			"label": "Total",
			"fieldtype": "Currency",
			"width":120
		} 
	]

def get_conditions(filters):
	conditions = ""
	if filters.get("almacen"):
		conditions += """and pa.almacen = ''  """.format(filters.get("almacen")) 
	if filters.get("producto"):
		conditions +=   """ p.name = '{0}' """.format(filters.get("producto")) 	
	if filters.get("deshabilitado"):
		if filters.get("deshabilitado") == "NO":
			conditions += """  and p.deshabilitado  = 0 """ 
		else:
			conditions += """  and p.deshabilitado  = 1  """ 
 
	return conditions
 
def get_advances(filters):	 
	condiciones =  get_conditions(filters)
	sql = """ select p.name,p.codigo   , p.nombre , pa.almacen
 , pp.lista_de_precios, pa.cantidad  ,pp.precio_siniva  
, (pp.precio_siniva *  pa.cantidad ) total
from tabproducto_almacen pa
inner join tabProductos p on (pa.parent = p.name)
inner join  tabfrmproductos_precios   pp on (pa.parent = pp.productos )
where  pp.lista_de_precios ='{0}' {1}""".format(filters.get("lista_precio") ,condiciones )
 

	
	return frappe.db.sql(sql,  as_dict=1)

 
 