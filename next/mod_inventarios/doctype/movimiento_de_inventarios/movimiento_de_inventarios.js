// Copyright (c) 2020, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('Movimiento de Inventarios', {
	// refresh: function(frm) {

	// }
	validate: function (frm) {

		if (frm.doc.almacenes_o == frm.doc.almacenes_d) {

			frappe.throw("El almacen origen y destino no pueden ser iguales");
		}
	}

});
