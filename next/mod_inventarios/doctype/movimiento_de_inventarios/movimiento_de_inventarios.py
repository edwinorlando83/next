# -*- coding: utf-8 -*-
# Copyright (c) 2020, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
from next.utilidades.utils import *

class MovimientodeInventarios(Document):
	def on_update(self):
		if self.docstatus == 1:
			for prod in self.detmovimientoinventario:
				
				insert_kardex( "MOVIMIENTOS", self.almacenes_d,prod.producto,0, prod.cantidad,self.name )
				uaxcan = float(prod.cantidad) * -1				 
				insert_kardex( "MOVIMIENTOS", self.almacenes_o,prod.producto,0, uaxcan,self.name )

