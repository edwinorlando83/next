// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt
var frmInventario;
frappe.ui.form.on('Inventario Inicial', {
	refresh: function(frm) {
      frmInventario = frm;
	}
});

frappe.ui.form.on('ii_producto', {
	cantidad: function(frm, cdt, cdn) {		
	const doc = frappe.get_doc(cdt, cdn);
    calculaPrecioTotalxLinea(doc,cdt, cdn);
	} ,
	precio_valorado  : function(frm, cdt, cdn) {		
	const doc = frappe.get_doc(cdt, cdn);
    calculaPrecioTotalxLinea(doc,cdt, cdn);
	}   	
});

function calculaPrecioTotalxLinea(doc,cdt, cdn)
{
	var cantidad = doc.cantidad;
	var precio_valorado = doc.precio_valorado;
	var totallinea = cantidad *  precio_valorado ;
	 
 
	frappe.model.set_value(cdt, cdn, 'total', totallinea ); 
	 
}
 
 
