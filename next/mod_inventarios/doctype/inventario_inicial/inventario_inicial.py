# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime
from next.utilidades.utils import *

class InventarioInicial(Document):

    def validate(self):
        sumtotal = 0
        for pro in self.ii_producto:
            sumtotal += pro.precio_valorado * pro.cantidad
        self.costototal= sumtotal

    def on_update (doc): 
        if doc.docstatus == 1:
            for pro in doc.ii_producto:      
                insert_kardex( "Inventario Incial", doc.almacen,pro.producto,pro.precio_valorado, pro.cantidad,doc.name )
             





