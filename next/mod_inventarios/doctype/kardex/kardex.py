# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Kardex(Document):
	def before_insert(self):
		if not self.cantidad:
			frappe.throw("La cantidad no puede ser 0")
			
		if self.cantidad == 0:
			frappe.throw("La cantidad no puede ser 0")

	def on_update(self):
		if self.docstatus == 1:		  
			proalmacen =     frappe.db.exists("producto_almacen", {"parent":self.producto, "almacen": self.almacen  })			 
			if not proalmacen :
				producto = frappe.get_doc("Productos",self.producto) 
				producto.append("producto_almacen", {"almacen":self.almacen,"cantidad":self.cantidad})	
				producto.save() 			
			else:
				proalmacen = frappe.get_doc("producto_almacen", {"parent":self.producto, "almacen": self.almacen  })				 
			 
				proalmacen.cantidad = float(proalmacen.cantidad) + float(self.cantidad)					 
				proalmacen.save()			
			      
