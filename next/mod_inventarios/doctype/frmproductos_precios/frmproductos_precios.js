// Copyright (c) 2022, Ing. Orlando Cholota and contributors
// For license information, please see license.txt

frappe.ui.form.on('frmproductos_precios', {
	precio_coniva: function(frm) {
		//calculaIvaIncluido();
	 },
	 precio_siniva: function(frm) {
		calculaIva();
	},
});

function calculaIva(){

	if( cur_frm.doc.productos){
	
		let iva = ( cur_frm.doc.precio_siniva * cur_frm.doc.ivaporcentaje  /100);
		iva = roundNumber(iva, 2);
		let total = cur_frm.doc.precio_siniva + iva ;
        total = roundNumber(total, 2);
		cur_frm.doc.iva = iva;
		cur_frm.doc.precio_coniva = total;
		console.log(total);
		cur_frm.refresh_field("precio_coniva");
		cur_frm.refresh_field("iva");
	}
	else{
		frappe.throw("Seleccion un Producto");
		cur_frm.doc.precio_siniva = 0;
	}
}

function calculaIvaIncluido(){

	if( cur_frm.doc.productos){
	
		
		
		let iva = ( cur_frm.doc.precio_siniva * cur_frm.doc.ivaporcentaje  /100);
		iva = roundNumber(iva, 2);
		let total = cur_frm.doc.precio_siniva + iva ;
        total = roundNumber(total, 2);
		cur_frm.doc.iva = iva;
		cur_frm.doc.precio_coniva = total;
		console.log(total);
		cur_frm.refresh_field("precio_coniva");
		cur_frm.refresh_field("iva");
	}
	else{
		frappe.throw("Seleccion un Producto");
		cur_frm.doc.precio_coniva = 0;
	}
}
