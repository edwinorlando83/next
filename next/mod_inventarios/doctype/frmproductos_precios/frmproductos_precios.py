# Copyright (c) 2022, Ing. Orlando Cholota and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import  rounded
class frmproductos_precios(Document):
	def validate(self): 
		iva = (self.precio_siniva * self.ivaporcentaje  /100)
		iva = rounded(iva, 2)
		total = self.precio_siniva + iva 
		total = rounded(total, 2)
		self.iva = iva
		self.precio_coniva = total

	def on_update(self):
		prod = frappe.get_doc("Productos",self.productos)
		sql = """ update tabproductos_precios set precio = {0}, precio_siniva = {1}
						  where lista_precios ='{2}' and parent = '{3}' """.format(self.precio_coniva,self.precio_siniva,self.lista_de_precios, self.productos)

		frappe.db.sql (sql)
		prod.reload()

	@frappe.whitelist()
	def getIvaPorcentaje(self, inproducto):
		obj_tarifa = frappe.db.sql(""" SELECT tarifa FROM  `tabImpuestos Venta` where nombre = (SELECT impuesto FROM  tabProductos where name =%s) """,inproducto)
		return obj_tarifa[0][0]
