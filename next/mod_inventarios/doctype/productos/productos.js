// Copyright (c) 2019, orlando and contributors
// For license information, please see license.txt

frappe.ui.form.on('Productos', {
  refresh: function (frm) {
    crearvariante();
    frm.set_df_property('posee_variantes', 'read_only', frm.doc.variante_de != undefined)
  },
  validate(frm) {
    
  },
  posee_variantes: function(frm){
    if ( cur_frm.doc.posee_variantes == 1){
 
    cur_frm.clear_table('productos_precios');
  }
    else
    getListaPrecios(frm);

  },
  onload: function (frm) {
    getAlmacenes(frm);
    getListaPrecios(frm);
    if ( cur_frm.doc.posee_variantes == 1){
       
 

    }
  },
  productos_familia: function (frm) {
    if (frm.doc.productos_familia == 'SERVICIOS') {
      frm.set_value('esservicio', 1);
    }else {
      frm.set_value('esservicio', 0);
    }
  }
})

frappe.ui.form.on('productos_precios', {
  precio: function (frm, cdt, cdn) {
    const docpp = frappe.get_doc(cdt, cdn);
    frappe.db.get_doc('Impuestos Venta', frm.doc.impuesto)
      .then(doc => {
        console.log(doc );
        let total = docpp.precio  / ( 1 + (doc.tarifa / 100));
        console.log(docpp.precio );
        console.log(doc.tarifa );
        console.log(total);
       // total = roundNumber(total );
        precision('precio_siniva', docpp)
        docpp.precio_siniva  = total;
        cur_frm.refresh_field('productos_precios');
      })
  },
  imprimir(frm, cdt, cdn){
          const docpp = frappe.get_doc(cdt, cdn);
         imprimirTicket(docpp.lista_precios,cur_frm.doc.name);
  }

});

function imprimirTicket(lista_precio,producto){

  frappe.call({
    method: 'getidEtiquetas',
    doc: cur_frm.doc,
    args: { 'lista_precio': lista_precio, 'producto': producto},
    freeze: true,
    callback: (r) => {

      console.log(r.message );

      window.open(
        frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
          + "doctype=" + encodeURIComponent("frmproductos_precios")
          + "&name=" + encodeURIComponent(r.message.name)
          + "&format=" + encodeURIComponent('etiquetas2')
          + "&no_letterhead=0"));

    }
  })

/*
  frappe.db.get_doc("frmproductos_precios",{"lista_de_precios":lista_precio, "productos":producto})
      .then(doc => {
        console.log(doc );
 
        window.open(
          frappe.urllib.get_full_url("api/method/frappe.utils.print_format.download_pdf?"
            + "doctype=" + encodeURIComponent("frmproductos_precios")
            + "&name=" + encodeURIComponent(doc.name)
            + "&format=" + encodeURIComponent('etiquetas')
            + "&no_letterhead=0"));
      });
*/


} 

function getAlmacenes (frm) {
  if (frm.is_new()) {
    frappe.db.get_list('Almacenes',
      { fields: ['name'] }).then((res) => {
    
      res.forEach(function (item) {
        frm.add_child('producto_en_almacen', { almacen: item.name  });
      })

      frm.refresh_field('producto_en_almacen');
    })
  }
}

function getListaPrecios (frm) {
  if (frm.is_new()) {
    frappe.db.get_list('Lista de Precios',
      { fields: ['name'] }).then((res) => {
   
      res.forEach(function (item) {
        frm.add_child('productos_precios', { lista_precios: item.name,  precio: 0})
      })

      frm.refresh_field('productos_precios');
    })
  }
}
function crearvariante () {
  if (cur_frm.doc.posee_variantes == 1) {
    cur_frm.set_intro()
    cur_frm.set_intro('Este producto es una plantilla y no se puede utilizar en las transacciones.', true)

    cur_frm.add_custom_button(__('Show Variants'), function () {
      frappe.set_route('List', 'Productos', {'variante_de': cur_frm.doc.name})
    }, __('View'))
    cur_frm.add_custom_button('Crear Variantes', function () {
      let lstvariantes = ''
      cur_frm.doc.productos_lista_variantes.forEach(function (item) {
        lstvariantes += item.atributo_productos + '  '
      })

      frappe.confirm('Esta seguro de crear las variantes con los atributos de: ' + lstvariantes,
        () => {

          frappe.call({
            doc: cur_frm.doc,
            method: 'crearvariantes',

            callback: r => {
              frappe.show_alert({
                indicator: 'green',
                message: __(r.message)
              })
            }
          })
        }, () => {
          // action to perform if No is selected
        })
    })
  }
}
