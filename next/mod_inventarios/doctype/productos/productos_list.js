frappe.listview_settings['Productos'] = {
  add_fields: [   'posee_variantes', 'deshabilitado'],
  filters: [['deshabilitado', '=', '0']],

  get_indicator: function (doc) {
    if (doc.deshabilitado) {
      return [__('Disabled'), 'grey', 'deshabilitado,=,Yes']
    } else if (doc.posee_variantes) {
      return [__('Template'), 'orange', 'posee_variantes,=,Yes']
    }
    else if (doc.variante_de) {
      return [__('Variant'), 'green', 'variante_de,=,' + doc.variante_de]
    }
    
    else return [__('Enabled'), 'blue', 'deshabilitado,=,No']
  } 
}
