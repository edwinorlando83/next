# -*- coding: utf-8 -*-
# Copyright (c) 2019, orlando and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from next.utilidades.utils import *

class Productos(Document):
    def before_insert(self):
        setEmisor(self)
        #sql = """ select ifnull(max(sec_interno),0) + 1 from  tabProductos where emisor ='{0}' """.format(self.emisor)
        #intcodigo_pk = str(frappe.db.sql(sql)[0][0])
        #self.sec_interno = int(intcodigo_pk)
        #self.codigo_pk = 'PROD-'+ intcodigo_pk.zfill(6)
    
    def validate(self):
        largo = self.prod_largo or " "
        ancho  = self.prod_ancho or " "
        espesor  = self.prod_espesor or " "
        nombrepod =   espesor + " "+  ancho + " "+  largo
        self.prod_medidas = nombrepod  

    def on_update(self):
        prod = frappe.get_doc("Productos", self.name)         
        for item in prod.productos_precios:            
            if self.posee_variantes == False:
                if frappe.db.exists("frmproductos_precios", { "lista_de_precios":item.lista_precios,  "productos":self.name}):                
                    itemprod = frappe.get_doc("frmproductos_precios", { "lista_de_precios":item.lista_precios,  "productos":self.name})     
                    itemprod.precio_siniva = item.precio_siniva                
                    itemprod.save()                

                else:
                    itemprod = frappe.new_doc("frmproductos_precios")
                    itemprod.productos = self.name
                    itemprod.lista_de_precios = item.lista_precios
                    itemprod.precio_siniva  = item.precio_siniva        
                    itemprod.insert()  
            


    @frappe.whitelist()
    def crearvariantes( self):       
        cont = 0
        for  variante in self.productos_lista_variantes:
            atprod = frappe.get_doc("atributo_productos", variante.atributo_productos) 
            for  lst in atprod.atributo_valor:
                #newprod = frappe.new_doc("Productos")
                maestro = frappe.get_doc("Productos", self.name)                
                newprod = frappe.copy_doc(maestro)
                newprod.atributo_productos = []
                newprod.nombre = self.nombre+ " "+atprod.atpr_abreviatura + " "+lst.atri_valor
                newprod.variante_de   = self.name
                newprod.posee_variantes = False
                newprod.atributo_valor = lst.name
                newprod.productos_precios = []
                newprod.append("productos_precios",{"lista_precios":variante.lista_precios, "precio_siniva":variante.precio_siniva, "precio":variante.precio})
                newprod.save()
                cont += 1
        return  "Se ha generado " + str(cont) + " productos"


    @frappe.whitelist()
    def getidEtiquetas( self,lista_precio, producto):
        etiquetas = frappe.get_doc("frmproductos_precios",{"lista_de_precios":lista_precio, "productos":producto})
        return etiquetas
    



@frappe.whitelist()     
def movil_get_productos(  ):      
    sql = """ select p.name, d.productos , d.precio , d.iva , d.subtotal , p.imagen from  nextdb.tablistaprecios_productos d inner join nextdb.`tabLista de Precios` l on ( d.parent  = l.name) inner join nextdb.tabProductos p on ( d.productos  = p.name  ) where preciomovil = 1"""     
    return frappe.db.sql(sql)

