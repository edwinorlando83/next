let producto;
let cantidad;
let lstItems = [];
this.almacen;
let cliente;
let lista_precio; 
let  num_fac= sessionStorage.getItem("num_fac")== undefined?"None":sessionStorage.getItem("num_fac");
 

frappe.pages['factura_rapida'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'factura_rapida',
		single_column: true
	});

	page.main.html(frappe.render_template("factura_rapida", {}));
	create_field_almacen(page);
	create_field_lector(page);
	create_field_cliente(page);
	create_field_lista_precio(page);
	create_field_lista_esfacelec(page);
	create_field_cantidad(page);  

	//this.lstItems = JSON.parse(sessionStorage.getItem("row_prod"));
//	console.log(this.lstItems);
}

 
function agregar_linea( ) {
   
    frappe.call({       
        method: "next.mod_inventarios.page.factura_rapida.factura_rapida.agregarProductos",
        freeze:true,
        args: { num_fac : num_fac
            ,producto : this.producto.get_value()
            , cantidad : 1
            , preciounitario : 10
            ,cliente:  this.cliente.get_value()
            ,almacen  : this.almacen.get_value()
            ,listaprecios : this.lista_precio.get_value()
        },
        callback: r => {
          frappe.show_alert({
            indicator: 'green',
            message:  r.message
          });

          sessionStorage.setItem("num_fac", r.message )

        }
      });

    
}
function create_field_almacen(page){
	  this.almacen = frappe.ui.form.make_control({
        parent: page.main.find(".div_almacen"),
        df: {
            fieldtype: "Link",
            options: "Almacenes",
            fieldname: "almacen",
            only_select: true,            
            input_class: 'input-xs',          
        },   only_input: true,
    
    });
    this.almacen.refresh();
}
function create_field_lector(page){

	  this.producto = frappe.ui.form.make_control({
        parent: page.main.find(".div_lector"),
        df: {
           
			fieldtype: "Link",
            options: "Productos",
            fieldname: "producto",               
            input_class: 'input-xs',          
        } ,   only_input: true,    });
	
		this.producto.refresh();
}

function create_field_cantidad(page){

	 this.cantidad = frappe.ui.form.make_control({
        parent: page.main.find(".div_cantidad"),
        df: {
            fieldtype: "Data",         
            fieldname: "cantidad", 
            input_class: 'input-xs',          
        } ,   only_input: true, 
    });
   
 
    this.cantidad.set_focus();
	this.cantidad.set_value('1');
	this.cantidad.refresh();
}

function create_field_cliente(page){
	  this.cliente = frappe.ui.form.make_control({
        parent: page.main.find(".div_cliente"),
        df: {
            fieldtype: "Link",
            options: "Clientes",
            fieldname: "cliente",         
            placeholder: __('Cliente'),
            input_class: 'input-xs',          
        },

        only_input: true,
    });
    this.cliente.refresh();
}

function create_field_lista_precio(page){
	this.lista_precio = frappe.ui.form.make_control({
        parent: page.main.find(".div_listaPrecios"),
        df: {
            fieldtype: "Link",
            options: "Lista de Precios",
            fieldname: "lista_precio",
            only_select: true,
            placeholder: __('Lista de Precios'),
            input_class: 'input-xs',          
        },
        only_input: true,
    });
    this.lista_precio.refresh();
}

function create_field_lista_esfacelec(page){
	var factura_elec = frappe.ui.form.make_control({
        parent: page.main.find(".div_factura_elec"),
        df: {
            fieldtype: "Select",
            options: "SI\nNO",
            fieldname: "esfacelec",
            only_select: true,
			default: "NO",
         
            input_class: 'input-xs',          
        },
        only_input: true,
    });
    factura_elec.refresh();
	factura_elec.set_value('NO');
}

function eliminar_linea( ){
	frappe.confirm('Are you sure you want to proceed?',
    () => {
        // action to perform if Yes is selected
    }, () => {
        // action to perform if No is selected
    })
}
