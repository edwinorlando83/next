import frappe

@frappe.whitelist()
def agregarProductos(num_fac,producto, cantidad, preciounitario,cliente,almacen,listaprecios):
    if num_fac== "None":
        fac = frappe.new_doc("Factura en Ventas")
        fac.fechaemision = frappe.utils.nowdate()
        fac.cliente = cliente
        fac.estab = almacen 
        fac.listaprecios = listaprecios
        fac.append("detalles_facventas", {"producto": producto,"cantidad": float(cantidad),"preciounitario":float(preciounitario)   })
        fac.insert()
        return fac.name
     