import frappe

def getpqc(user):
    if not user: user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        emisor = frappe.defaults.get_user_default('emisor')      
        if emisor :
            return """(emisor= '{0}')""".format(emisor)
      